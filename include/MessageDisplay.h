#pragma once

#include <string.h>
#include <stdint.h>

namespace msp_osd {

// Character size limitations
#define MSG_BUFFER_SIZE 250

// Size of available characters, accounting for null terminator
// Note: the craft_name seems to think it has 15 chars. From testing,
// that seems incorrect
#define FULL_MSG_LENGTH 12
#define FULL_MSG_BUFFER 13

// Supported message types
enum MessageDisplayType {
    WARNING,
    FLIGHT_MODE,
    ARMING,
    STATUS,
    HEADING
};

// Display information
class MessageDisplay {
    // Working information
    char warning_msg[MSG_BUFFER_SIZE];
    char flight_mode_msg[MSG_BUFFER_SIZE];
    char arming_msg[MSG_BUFFER_SIZE];
    char heading_msg[MSG_BUFFER_SIZE];
    char status_msg[MSG_BUFFER_SIZE];  // Currently unused

    // The full message and the part we're currently displaying
    char full_message[MSG_BUFFER_SIZE];

    // Current index we're displaying
    uint16_t index;

    // Last update timestamp
    uint64_t last_update_;
    bool updated_;

    // Dwell duration update period (us)
    uint64_t period_;
    uint64_t dwell_;

public:
    MessageDisplay() :
        warning_msg{""}, flight_mode_msg{"???"}, arming_msg{"????"},
        heading_msg{"??"}, status_msg{""}, full_message{"INITIALIZING"},
        index(0), last_update_(0), updated_(false), 
        period_(125000), dwell_(500000) {}

    MessageDisplay(const uint64_t period, const uint64_t dwell) :
        warning_msg{""}, flight_mode_msg{"???"}, arming_msg{"????"},
        heading_msg{"??"}, status_msg{""}, full_message{"INITIALIZING"},
        index(0), last_update_(0), updated_(false),
        period_(period), dwell_(dwell) {}

    // Set the given string
    void set(const MessageDisplayType mode, const char *string);
    void get(char *string, const uint32_t current_time);

    // Update local parameters
    void set_period(const uint64_t period) { period_ = period; }
    void set_dwell(const uint64_t dwell) { dwell_ = dwell; }
};

} // namespace msp_osd
