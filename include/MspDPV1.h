#pragma once

#include <pthread.h> 
#include <cmath>
#include <memory>
#include <numeric>   
#include <chrono>
#include <map>

// MAVLink
#include <c_library_v2/common/mavlink.h>

// MPA
#include <modal_start_stop.h>
#include <modal_pipe_client.h>
#include <modal_pipe_common.h>
#include <modal_pipe_server.h>

// MSP
#include "msp_dp_defines.h"
#include "MessageDisplay.h"
#include "config_file_msp.h"
#include "msp_osd_symbols.h"

// Thread control
#include <atomic>
#include <mutex>

// Menu control
// #include "MenuManager.h"

// Misc (time)
#include "misc.h"

#define MSP_READ_BUFFER_SIZE 128
#define MSP_OUT_PIPE_CH 0
#define MSP_OUT_PIPE_NAME "msp_osd"
#define MSP_OUT_DATA_TYPE "msp_dp_cmd_t"
#define MSP_PROCESS_NAME "voxl-osd"
#define MSP_RECOMMENDED_PIPE_SIZE (4*1024)
#define PX4_LISTENER_BUFSIZE 128

typedef struct {
    int port_fd; // File descriptor for the UART port
    uint8_t inBuf[256]; // Buffer to store incoming data
    uint8_t checksum;   // MSP Checksum for the received data
    uint8_t offset;     // Tracks the current byte position in the buffer
    uint8_t dataSize;   // Expected size of the payload
    uint8_t packetState; // Keeps track of the parsing state
    uint8_t cmdMSP;     // Command ID of the MSP message
	uint8_t checksum1;
	uint8_t checksum2;
} mspPort_t;



class MspDPV1
{
public:
	MspDPV1(bool input_only);
	~MspDPV1();
	void process_data();
	bool Send(const uint8_t message_id, const void *payload, mspDirection_e direction = MSP_DIRECTION_REQUEST);
	void init_output_pipe_location(pipe_info_t* info, char* pipe_location, const char* process); 
	bool input_is_uart(){return m_input_fd>0;};
	void receive();
	void processMessage();
	void processIncomingByte(uint8_t byte);
	void reset_msp_port();
	bool set_osd_canvas(msp_dp_canvas_t osd_canvas);
	msp_dp_canvas_t get_osd_canvas_dims(){return m_osd_canvas;};
	void reset_clear_cmd(){clear_screen = false;};
	bool clear_screen_pending(){return clear_screen;};
	void reset_osd(){std::lock_guard<std::mutex> lock(map_mutex); displayDataMap.clear();};
	// bool skip_osd_element(int32_t col, int32_t row, int msg_size, int menu_col, int menu_row, int max_menu_msg_size, int menu_size){return m_menu_manger->menu_is_active() ? element_interferes_with_menu(col, row, msg_size, menu_col, menu_row, max_menu_msg_size, menu_size) : false;};

	// Betaflight Menu
	void bf_menu_active(bool status){betaflight_menu = status;};	

	// Function to stop receiving data
    void stopReceive();
	std::atomic<bool> receiveRunning;

	std::map<std::pair<uint8_t, uint8_t>, std::string> getDisplayDataMap(){return displayDataMap;};

	// Mavlink message
	int32_t m_vio_quality = -1;	// VIO/OV quality
	std::shared_ptr<mavlink_heartbeat_t> heartbeat_msg{std::make_shared<mavlink_heartbeat_t>()};
	std::shared_ptr<mavlink_ping_t> ping_msg{std::make_shared<mavlink_ping_t>()};
	std::shared_ptr<mavlink_attitude_t> attitude_msg{std::make_shared<mavlink_attitude_t>()};
	std::shared_ptr<mavlink_odometry_t> odometry_msg{std::make_shared<mavlink_odometry_t>()};
	std::shared_ptr<mavlink_rc_channels_t> rc_msg{std::make_shared<mavlink_rc_channels_t>()};
	std::shared_ptr<mavlink_battery_status_t> battery_msg{std::make_shared<mavlink_battery_status_t>()};
	std::shared_ptr<mavlink_gps_raw_int_t> global_pos_msg{std::make_shared<mavlink_gps_raw_int_t>()};
	std::shared_ptr<mavlink_home_position_t> home_position_msg{std::make_shared<mavlink_home_position_t>()};
	std::shared_ptr<mavlink_system_time_t> system_time_msg{std::make_shared<mavlink_system_time_t>()};
	std::shared_ptr<mavlink_statustext_t> status_text_msg{std::make_shared<mavlink_statustext_t>()};

private:
	mspPort_t mspPort{0};  
	msp_dp_rc_sticks_t	m_sticks{0};
	msp_dp_osd_params_t	m_parameters{0};
	uint8_t fontType{0};
	resolutionType_e resolution{HD_5320};
	msp_dp_canvas_t m_osd_canvas{19,52};
	static constexpr uint8_t row_max[4]{15,17,15,19};
	static constexpr uint8_t column_max[4]{29,49,29,52};

	std::map<std::pair<uint8_t, uint8_t>, std::string> displayDataMap;
	const std::unordered_map<uint8_t, std::string>& symbolMap{getSymbolMap()};

	// Menu control
	bool betaflight_menu{false};
	// MenuManager Disabled for camera server implementation (minimal processing done here in order to not add latency to video streams)
	// std::unique_ptr<MenuManager> m_menu_manger{nullptr};
	
	// Display message
	int m_param_osd_log_level{0};
	msp_osd::MessageDisplay m_display{};

	// UART/MPA control
	int m_input_fd{-1};
	int m_output_fd{-1};
	char MSP_PIPE_LOCATION[64];

	// Track screen refresh
	std::mutex map_mutex;
	int64_t last_clear{time_monotonic_ns()};
	bool clear_screen{false};

	// Internal functionality 
	int open_uart(char* device);
	bool setup_comms();
	int get_rssi_dbm();
	float get_bearing_to_next_waypoint(double lat_now, double lon_now, double lat_next, double lon_next);
	float get_distance_to_next_waypoint(double lat_now, double lon_now, double lat_next, double lon_next);
	int wrap(int x, int low, int high);
	void read_uart();
};