#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include "msp_dp_defines.h"

#define CONF_FILE "/etc/modalai/voxl-osd.conf"

enum protocol_type_e {
    PROTO_MAVLINK=1,
    PROTO_MSP
};

enum input_type_e {
    INPUT_MPA=1,
    INPUT_UART
};

enum output_type_e {
    OUTPUT_NONE,
    OUTPUT_MPA=1,
    OUTPUT_UART,
    OUTPUT_MPA_UART
};

enum shortcut_type_e {
    OPT_A=0,
    OPT_B,
    OPT_MAX
};

static const char* shortcut_type_strings[OPT_MAX] = {"OPT_A", "OPT_B"};

typedef struct {
	int32_t		rssi_col;
	int32_t		rssi_row;
	int32_t		current_draw_col;
	int32_t		current_draw_row;
	int32_t		battery_col;
	int32_t		battery_row;
	int32_t		cell_battery_col;
	int32_t		cell_battery_row;
	int32_t		disarmed_col;
	int32_t		disarmed_row;
	int32_t		status_col;
	int32_t		status_row;
	int32_t		flight_mode_col;
	int32_t		flight_mode_row;
	int32_t 	latitude_col;
	int32_t 	latitude_row;
	int32_t 	longitude_col;
	int32_t		longitude_row;
	int32_t		to_home_col;
	int32_t		to_home_row;
	int32_t		crosshair_col;
	int32_t		crosshair_row;
	int32_t		heading_col;
	int32_t		heading_row;
	int32_t		vio_col;
	int32_t		vio_row;
	int32_t		resolution;
	uint8_t 	font_type;
	uint8_t		input_protocol;						// MAVLink or MSP, default to MAVLink
	uint8_t		input_type;							// UART or MPA exclusive
	char		input_port[MAX_PORT_NAME_LEN];		// If input is UART, which UART port to read from 
	uint8_t		output_type;						// Can be any combo of UART and MPA
	char		output_port[MAX_PORT_NAME_LEN];		// If output is UART, which UART port to write to
	enum shortcut_type_e		shortcut;					// 0mW mode shortcut
} msp_dp_osd_params_t;

static const char* input_protocol_as_string(enum protocol_type_e protocol_type){
	switch(protocol_type){
		case PROTO_MAVLINK: return "MAVLink";
		case PROTO_MSP: 	return "MSP";
	}
	return "???";
}

static const char* input_type_as_string(enum input_type_e input_type){
    switch(input_type){
        case INPUT_MPA:  return "MPA";
        case INPUT_UART: return "UART";
    }
	return "???";
}

static const char* output_type_as_string(enum output_type_e output_type){
    switch(output_type){
		case OUTPUT_NONE: return "NONE";
        case OUTPUT_MPA:  return "MPA";
        case OUTPUT_UART: return "UART";
        case OUTPUT_MPA_UART: return "MPA & UART";
    }
	return "???";
}
static const char* shortcut_type_as_string(enum shortcut_type_e shortcut_type){
    switch(shortcut_type){
        case OPT_A:  return "OPT_A";
        case OPT_B: return "OPT_B";
        default: return "???";
    }
	return "???";
}

#ifdef __cplusplus
extern "C" int load_config_file(msp_dp_osd_params_t* params, bool print=false);
#else
int load_config_file(msp_dp_osd_params_t* params, bool print);
#endif

#ifdef __cplusplus
extern "C" int print_config_file(msp_dp_osd_params_t params);
#else
int print_config_file(msp_dp_osd_params_t params);
#endif

#endif // end #define CONFIG_FILE_H
