#pragma once

// basic types
#include <cmath>
#include <string>
#include <memory>
#include <chrono>

// MSP structs
#include "msp_dp_defines.h"
#include "msp_osd_symbols.h"
#include "MessageDisplay.h"
#include <c_library_v2/common/mavlink.h>

const int DEFAULT_ROLL_CH = 0;
const int DEFAULT_PITCH_CH = 1;
const int DEFAULT_YAW_CH = 3;
const int DEFAULT_THROTTLE_CH = 2;


namespace msp_dp_osd
{

// construct an MSP_NAME struct
//  note: this is actually how we display _all_ string information
msp_name_t construct_display_message(std::shared_ptr<mavlink_heartbeat_t> hrt,
				     std::shared_ptr<mavlink_attitude_t> attitude,
					 std::shared_ptr<mavlink_ping_t> ping,
					 std::shared_ptr<mavlink_statustext_t> status_text,
				     const int log_level,
				     msp_osd::MessageDisplay &display);

// Construct a vtx config struct
msp_dp_vtx_config_t construct_vtx_config(uint8_t band, uint8_t channel);

// Construct a status struct
msp_dp_status_t construct_status(std::shared_ptr<mavlink_heartbeat_t> hrt);

// Construct a RC struct
msp_rc_t construct_RC(std::shared_ptr<mavlink_rc_channels_t> rc, msp_dp_rc_sticks_t &sticks);

// Construct a canvas struct
msp_dp_canvas_t construct_OSD_canvas(uint8_t col, uint8_t row);

// Construct a heartbeat command
displayportMspCommand_e construct_OSD_heartbeat();

// Construct a release command
displayportMspCommand_e construct_OSD_release();

// Construct a clear command
displayportMspCommand_e construct_OSD_clear();

// Construct a write struct given a string
uint8_t construct_OSD_write(uint8_t col, uint8_t row, bool blink, const char *string, uint8_t *output, uint8_t len);

// Construct a draw command
displayportMspCommand_e construct_OSD_draw();

// Construct a config command
msp_dp_config_t construct_OSD_config(uint8_t resolution, uint8_t fontType);

// Construct Flight mode Message
const char* construct_flight_mode(std::shared_ptr<mavlink_heartbeat_t> hrt);

// Generate bearing symbol from direction
uint8_t get_symbol_from_bearing(float bearing);

// Convert warning message to upper case so OSD interprets the message as letters instead of symbols
void log_msg_to_upper(char* string);

} // namespace msp_dp_osd