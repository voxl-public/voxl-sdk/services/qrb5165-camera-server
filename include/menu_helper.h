#pragma once
#ifndef MENU_HELPER_H
#define MENU_HELPER_H

// basic types
#include <cmath>
#include <string>
#include <memory>
#include <chrono>

// Modal JSON
#include <modal_json.h>
#include <modal_journal.h>

// Config files
static constexpr const char* FC_CONFIG_PATH = "/etc/modalai/voxl-px4.conf";
static constexpr const char* VTX_CONFIG_PATH= "/etc/modalai/voxl-vtx.conf";
static constexpr const char* CAMERA_CONFIG_PATH = "/etc/modalai/voxl-camera-server.conf";

// DON'T CHANGE THESE UNLESS MORE ITEMS ARE ADDED OR REMOVED TO MENUS DEFINED BELOW
/* Menu constants */ 
static constexpr int FC_MENU_ROWS{3};
static constexpr int VTX_MENU_ROWS{8};
static constexpr int CAMERA_MENU_ROWS{5};
static constexpr int ERROR_MENU_ROWS{1};

enum menu_type_e {
    FC_MENU = 1,
    VTX_MENU,
    CAMERA_MENU,
    SHORTCUT_MENU,
    UNKNOWN_MENU = -1
};

// Utils
static bool is_armed(std::shared_ptr<mavlink_heartbeat_t> hrt){return hrt->base_mode & MAV_MODE_FLAG_SAFETY_ARMED;};
static constexpr int stick_is_high(uint16_t value){return value > 1750;};
static constexpr int stick_is_low (uint16_t value){return value < 1250;};
static constexpr int stick_is_middle(uint16_t value){return !stick_is_high(value) && !stick_is_low(value);};
static constexpr bool open_fc_menu(uint16_t roll, uint16_t pitch, uint16_t yaw, uint16_t throttle){return stick_is_low(yaw) && stick_is_middle(throttle) && stick_is_middle(roll) && stick_is_high(pitch);};
static constexpr bool open_vtx_menu(uint16_t roll, uint16_t pitch, uint16_t yaw, uint16_t throttle){return stick_is_high(yaw) && stick_is_low(throttle) && stick_is_low(roll) && stick_is_low(pitch);};
static bool open_camera_menu(uint16_t roll, uint16_t pitch, uint16_t yaw, uint16_t throttle){
    static std::chrono::steady_clock::time_point timer_start{std::chrono::steady_clock::time_point::min()};
    if(stick_is_high(yaw) && stick_is_middle(throttle) && stick_is_middle(roll) && stick_is_middle(pitch)){
        if(timer_start == std::chrono::steady_clock::time_point::min()){
            timer_start = std::chrono::steady_clock::now();
            return false;
        }
        if(std::chrono::steady_clock::now()-timer_start >= std::chrono::seconds(2)){
            timer_start = std::chrono::steady_clock::time_point::min();
            return true;
        }
    }  else {
        timer_start = std::chrono::steady_clock::time_point::min();
    }
    return false;
};
static constexpr bool element_interferes_with_menu(int32_t col, int32_t row, int msg_size, int menu_col, int menu_row, int largest_menu_string, int menu_size){
    return ((col+msg_size > menu_col) && col < (menu_col+largest_menu_string)) && (row < (menu_row+menu_size) && row > menu_row);
};
static bool stick_debounce(std::chrono::steady_clock::time_point last){return (std::chrono::steady_clock::now()-last)>std::chrono::milliseconds(300);};
static bool menu_debounce(std::chrono::steady_clock::time_point last_menu_input, std::chrono::steady_clock::time_point last_menu_request){
    return stick_debounce(last_menu_input) && stick_debounce(last_menu_request);
};
static constexpr bool enter_shortcut(uint16_t roll, uint16_t pitch, uint16_t yaw, uint16_t throttle, shortcut_type_e shortcut_type){
    switch(shortcut_type){
        case OPT_MAX: [[fallthrough]]
        case OPT_A: return stick_is_low(yaw) && stick_is_low(throttle) && stick_is_high(roll) && stick_is_low(pitch);
        case OPT_B: return stick_is_low(yaw) && stick_is_high(throttle) && stick_is_high(roll) && stick_is_high(pitch);
        default: return false; 
    }
    return false;
};
static constexpr bool exit_shortcut(uint16_t roll, uint16_t pitch, uint16_t yaw, uint16_t throttle, shortcut_type_e shortcut_type){
    switch(shortcut_type){
        case OPT_MAX: [[fallthrough]]
        case OPT_A: return stick_is_high(yaw) && stick_is_low(throttle) && stick_is_low(roll) && stick_is_low(pitch);
        case OPT_B: return stick_is_high(yaw) && stick_is_high(throttle) && stick_is_low(roll) && stick_is_high(pitch);
        default: return false; 
    }
    return false;
};
static bool exit_menu(msp_dp_rc_sticks_t m_sticks, int menu_row, menu_type_e menu_type){
    switch(menu_type){
        case FC_MENU: return stick_is_high(m_sticks.yaw) && (menu_row >= FC_MENU_ROWS-2);
        case VTX_MENU: return stick_is_high(m_sticks.yaw) && (menu_row >= VTX_MENU_ROWS-2);
        case CAMERA_MENU: return stick_is_high(m_sticks.yaw) && (menu_row >= CAMERA_MENU_ROWS-2);
        case UNKNOWN_MENU: break;
    }
    return false;
};
static bool save_changes(uint16_t yaw, int menu_row, menu_type_e menu_type){
    switch(menu_type){
        case FC_MENU: return stick_is_high(yaw) && (menu_row == FC_MENU_ROWS-1);
        case VTX_MENU: return stick_is_high(yaw) && (menu_row == VTX_MENU_ROWS-1);
        case CAMERA_MENU: return stick_is_high(yaw) && (menu_row == CAMERA_MENU_ROWS-1);
        case UNKNOWN_MENU: break;
    }
    return false;
};
static const char* menu_type_as_string(menu_type_e menu_type){
    switch(menu_type){
        case FC_MENU: return "FC";
        case VTX_MENU: return "VTX";
        case CAMERA_MENU: return "CAMERA";
        case SHORTCUT_MENU: return "SHORTCUT";
        case UNKNOWN_MENU: break;
    }
    return "???";
}

static cJSON* open_json(const char* json_path){
  int ret = json_make_empty_file_if_missing(json_path);
  if(ret < 0){ 
    M_ERROR("Error creating MSP DP OSD config file\n");
    return NULL;
  } else if(ret>0){
    M_ERROR("No MSP DP OSD configuration file found, created a new empty configuration file using defaults: %s", json_path);
  }
  return json_read_file(json_path);
}

// If you update these menus below, you must also update the constants for # of menu rows above!!
/* Flight Controller Menu Fields */
constexpr int FC_STICK_ASSIGNMENTS{0};
static const char* fc_menu[FC_MENU_ROWS] = {
    (char*)"STICKS",
    (char*)"EXIT",
    (char*)"SAVE AND EXIT",
};
constexpr int NUM_STICK_LAYOUTS{3};
static const char* stick_layouts[NUM_STICK_LAYOUTS] = {
    "DEFAULT",
    "PROFILE 1",
    "PROFILE 2"
};
static const char* FC_MENU_DEFAULTS[FC_MENU_ROWS-2] =
{
    "DEFAULT"    
};

/* VTX Menu Fields */
constexpr int VTX_FREQ{0};
constexpr int VTX_POWER{1};
constexpr int VTX_MCS{2};
constexpr int VTX_FEC{3};
constexpr int VTX_MODE{4};
constexpr int VTX_SHORTCUTS{5};
static const char* vtx_menu[VTX_MENU_ROWS] = {
    "FREQ",
    "POWER",
    "MCS",
    "FEC %",
    "MODE",
    "SHORTCUTS",
    "EXIT",
    "SAVE AND EXIT",
};
constexpr int NUM_FREQ{41};
static constexpr int DEFAULT_FREQ{5865};
static const char* frequencies[NUM_FREQ] = { 
    "2412",
    "2417",
    "2422",
    "2427",
    "2432",
    "2437",
    "2442",
    "2447",
    "2452",
    "2457",
    "2462",
    "2467",
    "2472",
    "5180",
    "5200",
    "5220",
    "5240",
    "5260",
    "5280",
    "5300",
    "5320",
    "5500",
    "5520",
    "5540",
    "5560",
    "5580",
    "5600",
    "5620",
    "5640",
    "5660",
    "5680",
    "5700",
    "5720",
    "5745",
    "5765",
    "5785",
    "5805",
    "5825",
    "5845",
    "5865",
    "5885",
};
constexpr int NUM_PWR_LEVEL{14};
static const char* power_level[NUM_PWR_LEVEL] = {
    "1",
    "5",
    "10",
    "15",
    "20",
    "25",
    "30",
    "35",
    "40",
    "45",
    "50",
    "55",
    "60",
    "63"
};
constexpr int NUM_MCS{8};
static const char* mcs_indices[NUM_MCS] = {
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7"
};
constexpr int NUM_FEC_PERCENT{19};
static const char* fec_percent[NUM_FEC_PERCENT] = {
    "0",
    "10",
    "20",
    "30",
    "40",
    "50",
    "60",
    "70",
    "80",
    "90",
    "100",
    "150",
    "200",
    "250",
    "300",
    "350",
    "400",
    "450",
    "500",
};
constexpr int NUM_MODES{3};
static const char* vtx_mode[NUM_MODES] = {
    "CUSTOM",
    "LOW LATENCY",
    "LONG RANGE"
};
constexpr int NUM_SHORTCUTS{2};
static const char* vtx_shortcut[NUM_SHORTCUTS] = {
    "OPT_A",
    "OPT_B"
};
static const char* VTX_MENU_DEFAULTS[VTX_MENU_ROWS-2] =
{
    "5745",    
    "3.0",    
    "3",    
    "20",    
    "CUSTOM",
    "OPT_A"   
};


/* Camera Menu Fields */ 
constexpr int CAMERA_RESOLUTION{0};
constexpr int CAMERA_BITRATE{1};
constexpr int CAMERA_FPS{2};
static const char* camera_menu[CAMERA_MENU_ROWS] = {
    "RESOLUTION",
    "BITRATE",
    "FPS",
    "EXIT",
    "SAVE AND EXIT",
};
constexpr int NUM_RESOLUTIONS{3};
static const char* resolutions[NUM_RESOLUTIONS] = {
    "640x480",
    "1024x768",
    "1280x720"
};
static const char* resolution_width[NUM_RESOLUTIONS] = {
    "640",
    "1024",
    "1280"
};
static const char* resolution_height[NUM_RESOLUTIONS] = {
    "480",
    "768",
    "720"
};
constexpr int NUM_BITRATE{40};
static const char* bitrates[NUM_BITRATE] = {
    "0.5",
    "1.0",
    "1.5",
    "2.0",
    "2.5",
    "3.0",
    "3.5",
    "4.0",
    "4.5",
    "5.0",
    "5.5",
    "6.0",
    "6.5",
    "7.0",
    "7.5",
    "8.0",
    "8.5",
    "9.0",
    "9.5",
    "10.0",
    "10.5",
    "11.0",
    "11.5",
    "12.0",
    "12.5",
    "13.0",
    "13.5",
    "14.0",
    "14.5",
    "15.0",
    "15.5",
    "16.0",
    "16.5",
    "17.0",
    "17.5",
    "18.0",
    "18.5",
    "19.0",
    "19.5",
    "20.0"
};
constexpr int NUM_FPS{8};
static const char* fps[NUM_FPS] = {
    "5",
    "10",
    "15",
    "20",
    "25",
    "30",
    "45",
    "60"
};
static const char* CAMERA_MENU_DEFAULTS[CAMERA_MENU_ROWS-2] =
{
    "1280x720",    
    "1.5",    
    "60"    
};

// ERROR MENU
static const char* error_menu[ERROR_MENU_ROWS] = {
    "ERROR - EXIT"
};

#endif // MENU_HELPER_H