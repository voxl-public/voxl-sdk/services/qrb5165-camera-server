/*******************************************************************************
 * Copyright 2025 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with hardware devices provided by
 *    ModalAI® Inc. Reverse engineering or copying this code for use on hardware 
 *    not manufactured by ModalAI is not permitted.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#pragma once

#include <stdint.h>
#include <string.h>

#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.h>
#include <CL/cl.hpp>

#include <vector>
#include <string>
#include <map>

#include <modal_journal.h>
#define ENCODER_USAGE  GRALLOC_USAGE_HW_VIDEO_ENCODER
#include "buffer_manager.h"

typedef struct __attribute__ ((packed))
{
    int src_offset_x;
    int src_offset_y;
    int src_width;
    int src_height;
    int src_line_stride;
    int src_plane_stride;
    int src_rggb;
    int dst_width;
    int dst_height;
    int dst_line_stride;    //pixels
    int dst_plane_stride;   //lines
    float gain_r;
    float gain_g;
    float gain_b;
    float gamma;
    float R00;
    float R01;
    float R10;
    float R11;
} MispTestParams;

typedef struct __attribute__ ((packed))
{
    float gain_r;
    float gain_g;
    float gain_b;
} MispGainParams;

typedef struct __attribute__ ((packed))
{
    float H[9];
} EisParams;

typedef struct _cl_box_size_qcom
{
    // Width of box filter on X direction.
    float x;

    // Height of box filter on Y direction.
    float y;
} cl_box_size_qcom;

typedef struct _cl_weight_desc_qcom
{
    // Coordinate of the "center" point of the weight image
    // based on the weight image's top-left corner as the origin.
    size_t       center_coord_x;
    size_t       center_coord_y;

    cl_bitfield  flags;

} cl_weight_desc_qcom;

typedef struct _cl_weight_image_desc_qcom
{
    cl_image_desc           image_desc;
    cl_weight_desc_qcom     weight_desc;
} cl_weight_image_desc_qcom;

typedef std::map< std::string, cl::Program > ProgramMap;

class Misp
{
public:
    Misp();
    ~Misp();

    int Init(uint32_t width, uint32_t height);

    int ConvertMipi10ToYuv(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_rggb,
                           void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride);

    int ConvertMonoMipi12ToYuv(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_rggb,
                               void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride);

    int ConvertMonoMipi12ToRaw8(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_rggb,
                             void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride);

    int ConvertMonoMipi10ToRaw8(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_rggb,
                             void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride);

    int AR0144MipiToRaw8LSC(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_bpp,
                            void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride);


    int NormalizeImage(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_rggb, uint32_t src_bpp,
                             void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride);

    int ConvertMipi10ToYuvRot(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_rggb,
                             void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride, float R[9]);

    int DrawPip(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride,
                 void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride);

    uint8_t * GetTempBuffer(uint32_t size);

    void * GetFullScratchBuffer() { return scratch_full_buffer->vaddress; }

    int SetAwbManualGains(float r, float g, float b);
    int SetGammaCorrection(float gamma_corr);
    int SetPIP(void * img, int width, int height, int stride, float scale);

    int ComputeAWB(void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride);

    std::string GetAWBMode() { return awb_mode; }

    int HandleControlCommand(std::string cmd);

    float roi_zoom = 1.0;
    float roi_zoom_target = 1.0;
    float roi_zoom_alpha  = 0.8;

    int SetRoiZoom(float val) { roi_zoom_target = val; return 0; }
    int ResetZoomFilter() { roi_zoom = roi_zoom_target; return 0; }
    int UpdateZoomFilter()
    {
        roi_zoom = roi_zoom * roi_zoom_alpha + roi_zoom_target * (1.0-roi_zoom_alpha);

        /*
        if      (roi_zoom < roi_zoom_target)
        {
            roi_zoom += 0.05;
            if (roi_zoom > roi_zoom_target)
                roi_zoom = roi_zoom_target;
        }

        else if (roi_zoom > roi_zoom_target)
        {
            roi_zoom -= 0.05;
            if (roi_zoom < roi_zoom_target)
                roi_zoom = roi_zoom_target;
        }
        */

        return 0;
    }


protected:
    static std::vector<cl::Platform> platforms;
    static std::vector<cl::Device> devices;
    static cl::Device  gpu_device;
    static cl::Context gpu_context;
    static ProgramMap cl_programs;
    static int gpu_initialized;

    static std::string device_name;
    static std::string driver_version;
    static std::string device_extensions;
/*
    std::vector<cl::Platform> platforms;
    std::vector<cl::Device> devices;
    cl::Device  gpu_device;
    cl::Context gpu_context;
    ProgramMap cl_programs;
    int gpu_initialized = 0;

    std::string device_name;
    std::string driver_version;
    std::string device_extensions;
*/
    cl::CommandQueue queue;

    cl::Kernel  kernel;
    cl::Kernel  kernel2;
    cl::Kernel  kernel3;
    cl::Kernel  kernel4;
    cl::Kernel  kernel5;

    cl::Kernel  blur_kernel;

    cl::Kernel  ar0144_8bit_lsc_kernel;
    cl::Kernel  ar0144_10bit_lsc_kernel;
    cl::Kernel  ar0144_12bit_lsc_kernel;

    cl::Kernel raw8_downscale_blur_to_raw8_kernel;
    cl::Kernel raw8_to_raw8_norm_kernel;
    cl::Kernel raw10_12_to_mono8_norm_kernel;

    cl::Kernel transform_kernel;
    

    BufferGroup  scratch1_buffer_group;
    BufferGroup  scratch2_buffer_group;
    BufferBlock* scratch_buffer;
    BufferBlock* scratch_buffer2;

    BufferBlock* scratch_full_buffer;

    std::vector<uint8_t> temp_buffer;

    std::string awb_mode   = "auto";
    float awb_gains[3]     = {1.0, 1.0, 1.0};
    uint32_t awb_counter   = 0;
    float gamma_correction = 1.0; //1.0 = no change, > 1.0 boost dark regions

    int pip_src_width = 0;
    int pip_src_height = 0;
    int pip_src_stride = 0;
    float pip_scale = 1.0;

    int LoadFileToString(std::string filename, std::string& s);
    int LoadKernelFromFile(std::string opencl_kernel_file, std::string kernel_name, cl::Kernel & kernel, std::string compiler_opts = std::string(""));
    int LoadKernelFromString(const std::string & kernel_string, std::string kernel_name, cl::Kernel & kernel, std::string compiler_opts = std::string(""));

    int MapIonBufferToGpu(cl::Context & cl_ctx, 
                      void * host_ptr, int32_t ion_fd, uint32_t size, 
                      cl::Memory * cl_mem_out_ptr,
                      cl_int * err_code);

    int MapIonBufferToGpuImage2dBayer8(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code);

    int MapIonBufferToGpuImage2dBayer10(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code);

    int MapIonBufferToGpuImage2dBayer12(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code);

    int MapIonBufferToGpuImage2dRgba8(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code);

    int MapIonBufferToGpuImage2dYuv(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr, cl::Memory * y_plane, cl::Memory * uv_plane,
                                            cl_int * err_code);

    int MapIonBufferToGpuImage2dY8(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code);

    int MapIonBufferToGpuImage2dY16(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code);

    int CreateGaussianKernelImage(cl::Context & cl_ctx, 
                                    cl::Memory * cl_mem_out_ptr,
                                    cl_int * err_code);

    int64_t GetTimeUs();
};
