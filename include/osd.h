#ifndef OSD_H
#define OSD_H

// 1280x720
constexpr int  ROW_MAX_720 = 684;
constexpr int  COL_MAX_720 = 1184;

// 640x480
constexpr int  ROW_MAX_480 = 452;
constexpr int  COL_MAX_480 = 636;

// Frame counter
constexpr int  CNTR_ROW_720 = 0;
constexpr int  CNTR_COL_720 = 580;
constexpr int  CNTR_ROW_480 = 0;
constexpr int  CNTR_COL_480 = 311;

// Battery  
constexpr int  BATT_ROW_720 = ROW_MAX_720-10;  
constexpr int  BATT_COL_720 = 0;
constexpr int  BATT_ROW_480 = ROW_MAX_480-10;  
constexpr int  BATT_COL_480 = 0;

// Current draw
constexpr int  AMPS_ROW_720 = ROW_MAX_720-10;
constexpr int  AMPS_COL_720 = 250;
constexpr int  AMPS_ROW_480 = ROW_MAX_480-10;  
constexpr int  AMPS_COL_480 = 90;

// VTX CPU Temp
constexpr int  VTX_CPU_T_ROW_720 = 54;
constexpr int  VTX_CPU_T_COL_720 = 0;
constexpr int  VTX_CPU_T_ROW_480 = 54;  
constexpr int  VTX_CPU_T_COL_480 = 0;

// VTX CPU Load
constexpr int  VTX_CPU_L_ROW_720 = 72;
constexpr int  VTX_CPU_L_COL_720 = 0;
constexpr int  VTX_CPU_L_ROW_480 = 72;  
constexpr int  VTX_CPU_L_COL_480 = 0;


// Roll
constexpr int  HRZN_ROW_720 = ROW_MAX_720/2;  
constexpr int  HRZN_COL_720 = ((COL_MAX_720/2) + 1);
constexpr int  HRZN_ROW_480 = ROW_MAX_480/2;
constexpr int  HRZN_COL_480 = ((COL_MAX_480/2) + 1);

// Pitch
#define PITCH  "-------"
constexpr int  PITCH_COL_720     = (HRZN_COL_720-1);
constexpr int  PITCH_TOP_ROW_720 =  HRZN_ROW_720/2;
constexpr int  PITCH_BOT_ROW_720 = HRZN_ROW_720+HRZN_ROW_720/2;
constexpr int  PITCH_COL_480     = (HRZN_COL_480-1);
constexpr int  PITCH_TOP_ROW_480 =  HRZN_ROW_480/2;
constexpr int  PITCH_BOT_ROW_480 = HRZN_ROW_480+HRZN_ROW_480/2;

// RC
constexpr int  RSSI_ROW = 0;
constexpr int  RSSI_COL = 0;

// Disarmed
constexpr int  DSRM_ROW_720 = 600;
constexpr int  DSRM_COL_720 = (COL_MAX_720/2);
constexpr int  DSRM_ROW_480 = 396;
constexpr int  DSRM_COL_480 = (COL_MAX_480/2);

// Display Message
constexpr int  DISP_ROW_720 = 650;
constexpr int  DISP_COL_720 = (COL_MAX_720/2);
constexpr int  DISP_ROW_480 = 430;
constexpr int  DISP_COL_480 = (COL_MAX_480/2);

struct osd_t {
    int row_max;
    int col_max;
    int cntr_row;
    int cntr_col;
    int batt_row;
    int batt_col;
    int amps_row;
    int amps_col;
    int cpu_t_row;
    int cpu_t_col;
    int cpu_l_row;
    int cpu_l_col;
    int hrzn_row;
    int hrzn_col;
    int pitch_top_row;
    int pitch_bott_row;
    int pitch_col;
    int rssi_row;
    int rssi_col;
    int dsrm_row;
    int dsrm_col;
    int disp_row;
    int disp_col;
};

static const osd_t OSDDefaults480 = 
    {
        ROW_MAX_480,        //< Max row available for writing for this resolution 
        COL_MAX_480,        //< Max column available for writing for this resolution 
        CNTR_ROW_480,       //< Frame counter row
        CNTR_COL_480,       //< Frame counter column
        BATT_ROW_480,       //< Battery voltage row
        BATT_COL_480,       //< Battery voltage column
        AMPS_ROW_480,       //< Current draw row
        AMPS_COL_480,       //< Current draw column
        VTX_CPU_T_ROW_480,  //< VTX CPU Temperature row
        VTX_CPU_T_COL_480,  //< VTX CPU Temperature column
        VTX_CPU_L_ROW_480,  //< VTX CPU Load row
        VTX_CPU_L_COL_480,  //< VTX CPU Load column
        HRZN_ROW_480,       //< Level Horizon bar row
        HRZN_COL_480,       //< Level Horizon bar column
        PITCH_TOP_ROW_480,  //< Pitch Top row
        PITCH_BOT_ROW_480,  //< Pitch Bottom row
        PITCH_COL_480,      //< Pitch column
        RSSI_ROW,           //< RC RSSI row
        RSSI_COL,           //< RC RSSI column
        DSRM_ROW_480,       //< Disarm message row
        DSRM_COL_480,       //< Disarm message column
        DISP_ROW_480,       //< Display Information row (flight mode, arm/disarm, heading)
        DISP_COL_480,       //< Display Information column (flight mode, arm/disarm, heading)
    };

static const osd_t OSDDefaults720 = 
    {
        ROW_MAX_720,        //< Max row available for writing for this resolution 
        COL_MAX_720,        //< Max column available for writing for this resolution 
        CNTR_ROW_720,       //< Frame counter row
        CNTR_COL_720,       //< Frame counter column
        BATT_ROW_720,       //< Battery voltage row
        BATT_COL_720,       //< Battery voltage column
        AMPS_ROW_720,       //< Current draw row
        AMPS_COL_720,       //< Current draw column
        VTX_CPU_T_ROW_720,  //< VTX CPU Temperature row
        VTX_CPU_T_COL_720,  //< VTX CPU Temperature column
        VTX_CPU_L_ROW_720,  //< VTX CPU Load row
        VTX_CPU_L_COL_720,  //< VTX CPU Load column
        HRZN_ROW_720,       //< Level Horizon bar row
        HRZN_COL_720,       //< Level Horizon bar column
        PITCH_TOP_ROW_720,  //< Pitch Top row
        PITCH_BOT_ROW_720,  //< Pitch Bottom row
        PITCH_COL_720,      //< Pitch column
        RSSI_ROW,           //< RC RSSI row
        RSSI_COL,           //< RC RSSI column
        DSRM_ROW_720,       //< Disarm message row
        DSRM_COL_720,       //< Disarm message column
        DISP_ROW_720,       //< Display Information row (flight mode, arm/disarm, heading)
        DISP_COL_720,       //< Display Information column (flight mode, arm/disarm, heading)
    };

#endif // OSD_H