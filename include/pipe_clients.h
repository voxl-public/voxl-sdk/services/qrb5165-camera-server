#pragma once



#include <modal_pipe_client.h>
#include "msp_dp_defines.h"
#include <map>

// Got these from voxl-osd: https://gitlab.com/voxl-public/voxl-sdk/services/voxl-osd/-/blob/master/include/msp_dp_defines.h?ref_type=heads#L970
#define FLIGHT_MODE_MANUAL 65536
#define FLIGHT_MODE_ALTCTL 131072
#define FLIGHT_MODE_ACRO 327680
#define FLIGHT_MODE_POSCTL 196608
#define FLIGHT_MODE_ORBIT 235143168
#define FLIGHT_MODE_AUTO_MISSION 67371008
#define FLIGHT_MODE_AUTO_LOITER 50593792
#define FLIGHT_MODE_AUTO_RTL 84148224
#define FLIGHT_MODE_AUTO_TAKEOFF 33816576
#define FLIGHT_MODE_AUTO_LAND 100925440
#define FLIGHT_MODE_AUTO_FOLLOW_TARGET 134479872
#define FLIGHT_MODE_AUTO_PRECLAND 151257088
#define FLIGHT_MODE_AUTO_VTOL_TAKEOFF 251920384
#define FLIGHT_MODE_OFFBOARD 393216
#define FLIGHT_MODE_STABELIZED 458752


typedef struct {
    double lat_deg;
    double lon_deg;
    double alt_msl_meters;
    double alt_agl_meters;
    
    double pitch_deg;
    double roll_deg;
    double yaw_deg;
} uav_state_t;

int pipe_clients_init(void);
uav_state_t grab_uav_state(void);
int grab_cpu_standby_active(void);

const char* grab_heading(void);
const char* grab_flight_mode(void);
bool grab_armed(void);
uint8_t grab_rssi(void);
double grab_battery_amps(void);
double grab_battery_voltage(void);
uint8_t grab_battery_voltage_percent(void);
float grab_cpu_temp(void);
float grab_cpu_load(void);
std::map<std::pair<uint8_t, uint8_t>, std::string> grab_msp_osd_map(void);
msp_dp_canvas_t grab_msp_osd_canvas_dims(void);
void bf_menu_active(bool status);

int pipe_clients_close(void);
