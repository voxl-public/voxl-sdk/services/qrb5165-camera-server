/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef APQ8096

#include <stdio.h>
#include <stdint.h>

#include <cci_direct.h>
#include "cci_direct_helpers.h"


// only tested with ar0144. I don't know if cci-direct or ar0144 reverses
// the bytes on 16-bit writes but this function undoes that.
static int write_16(int cam_id, uint16_t slave_addr, uint16_t reg, uint16_t val)
{
	uint8_t valarr[2];

	valarr[0] = (val&0x00ff);
	valarr[1] = (val&0xff00)>>8;
	//printf("writing 0x%04x 0x%02x%02x\n", reg, valarr[1], valarr[0]);
	return voxl_cci_write(cam_id, slave_addr, reg, CCI_16BIT, valarr, CCI_16BIT);
}


static int _ov7251_tweaks(int cam_id)
{
	printf("setting ov7251 BLC register for cam id %d\n", cam_id);
	int ret = 0;

	uint16_t slave_addr = voxl_cci_get_slave_addr(cam_id);
	if((int16_t)slave_addr == -1){
		fprintf(stderr, "failed to fetch slave address for cam_id #%d\n", cam_id);
		return -1;
	}

	uint8_t val = 0x02; // force BLC to run every frame, this prevents random brightness shifts
	ret |= voxl_cci_write(cam_id, slave_addr, 0x4005, CCI_16BIT, &val, CCI_8BIT);

	return ret;
}


// same registers for both!
static int _ov7251_ov9782_rotate(int cam_id, int en_rotate)
{
	int ret = 0;
	uint8_t val;

	uint16_t slave_addr = voxl_cci_get_slave_addr(cam_id);
	if((int16_t)slave_addr == -1){
		fprintf(stderr, "failed to fetch slave address for cam_id #%d\n", cam_id);
		return -1;
	}

	// normal, upright
	if(!en_rotate){
		val = 0;
		ret |= voxl_cci_write(cam_id, slave_addr, 0x3820, CCI_16BIT, &val, CCI_8BIT);
		ret |= voxl_cci_write(cam_id, slave_addr, 0x3821, CCI_16BIT, &val, CCI_8BIT);
	}
	else{
		val = 4;
		ret |= voxl_cci_write(cam_id, slave_addr, 0x3820, CCI_16BIT, &val, CCI_8BIT);
		ret |= voxl_cci_write(cam_id, slave_addr, 0x3821, CCI_16BIT, &val, CCI_8BIT);
	}

	return ret;
}


static int _ar0144_set_lsc(int cam_id)
{
	int ret = 0;

	uint16_t slave_addr = voxl_cci_get_slave_addr(cam_id);
	if((int16_t)slave_addr == -1){
		fprintf(stderr, "failed to fetch slave address for cam_id #%d\n", cam_id);
		return -1;
	}

	// enable, this is wonky so do again at the end
	write_16(cam_id, slave_addr, 0x3780, 0x8000);

	ret |= write_16(cam_id, slave_addr, 0x3600, 0x05B0);
	ret |= write_16(cam_id, slave_addr, 0x3602, 0x6A0A);
	ret |= write_16(cam_id, slave_addr, 0x3604, 0x1F51);
	ret |= write_16(cam_id, slave_addr, 0x3606, 0xB9D0);
	ret |= write_16(cam_id, slave_addr, 0x3608, 0x03B8);
	ret |= write_16(cam_id, slave_addr, 0x360A, 0x05B0);
	ret |= write_16(cam_id, slave_addr, 0x360C, 0x4F4A);
	ret |= write_16(cam_id, slave_addr, 0x360E, 0x1FB1);
	ret |= write_16(cam_id, slave_addr, 0x3610, 0xBDF0);
	ret |= write_16(cam_id, slave_addr, 0x3612, 0x0398);
	ret |= write_16(cam_id, slave_addr, 0x3614, 0x05B0);
	ret |= write_16(cam_id, slave_addr, 0x3616, 0x578A);
	ret |= write_16(cam_id, slave_addr, 0x3618, 0x1F51);
	ret |= write_16(cam_id, slave_addr, 0x361A, 0xB690);
	ret |= write_16(cam_id, slave_addr, 0x361C, 0x03B8);
	ret |= write_16(cam_id, slave_addr, 0x361E, 0x05B0);
	ret |= write_16(cam_id, slave_addr, 0x3620, 0x412A);
	ret |= write_16(cam_id, slave_addr, 0x3622, 0x2071);
	ret |= write_16(cam_id, slave_addr, 0x3624, 0xBB90);
	ret |= write_16(cam_id, slave_addr, 0x3626, 0x0398);
	ret |= write_16(cam_id, slave_addr, 0x3640, 0x006A);
	ret |= write_16(cam_id, slave_addr, 0x3642, 0xA091);
	ret |= write_16(cam_id, slave_addr, 0x3644, 0x9B93);
	ret |= write_16(cam_id, slave_addr, 0x3646, 0x4BD2);
	ret |= write_16(cam_id, slave_addr, 0x3648, 0x9ED6);
	ret |= write_16(cam_id, slave_addr, 0x364A, 0x7189);
	ret |= write_16(cam_id, slave_addr, 0x364C, 0x9FD1);
	ret |= write_16(cam_id, slave_addr, 0x364E, 0x9AB3);
	ret |= write_16(cam_id, slave_addr, 0x3650, 0x43F2);
	ret |= write_16(cam_id, slave_addr, 0x3652, 0xA156);
	ret |= write_16(cam_id, slave_addr, 0x3654, 0x79C9);
	ret |= write_16(cam_id, slave_addr, 0x3656, 0xA151);
	ret |= write_16(cam_id, slave_addr, 0x3658, 0x9AB3);
	ret |= write_16(cam_id, slave_addr, 0x365A, 0x54D2);
	ret |= write_16(cam_id, slave_addr, 0x365C, 0xA716);
	ret |= write_16(cam_id, slave_addr, 0x365E, 0x5E09);
	ret |= write_16(cam_id, slave_addr, 0x3660, 0x9FD1);
	ret |= write_16(cam_id, slave_addr, 0x3662, 0x97F3);
	ret |= write_16(cam_id, slave_addr, 0x3664, 0x43F2);
	ret |= write_16(cam_id, slave_addr, 0x3666, 0xAD76);
	ret |= write_16(cam_id, slave_addr, 0x3680, 0x6252);
	ret |= write_16(cam_id, slave_addr, 0x3682, 0x8753);
	ret |= write_16(cam_id, slave_addr, 0x3684, 0x8637);
	ret |= write_16(cam_id, slave_addr, 0x3686, 0x24F5);
	ret |= write_16(cam_id, slave_addr, 0x3688, 0x6EFC);
	ret |= write_16(cam_id, slave_addr, 0x368A, 0x6132);
	ret |= write_16(cam_id, slave_addr, 0x368C, 0x9073);
	ret |= write_16(cam_id, slave_addr, 0x368E, 0x8317);
	ret |= write_16(cam_id, slave_addr, 0x3690, 0x5EB5);
	ret |= write_16(cam_id, slave_addr, 0x3692, 0x6E7C);
	ret |= write_16(cam_id, slave_addr, 0x3694, 0x61F2);
	ret |= write_16(cam_id, slave_addr, 0x3696, 0x8293);
	ret |= write_16(cam_id, slave_addr, 0x3698, 0x84B7);
	ret |= write_16(cam_id, slave_addr, 0x369A, 0x1855);
	ret |= write_16(cam_id, slave_addr, 0x369C, 0x6E9C);
	ret |= write_16(cam_id, slave_addr, 0x369E, 0x61B2);
	ret |= write_16(cam_id, slave_addr, 0x36A0, 0x8D33);
	ret |= write_16(cam_id, slave_addr, 0x36A2, 0x8497);
	ret |= write_16(cam_id, slave_addr, 0x36A4, 0x5435);
	ret |= write_16(cam_id, slave_addr, 0x36A6, 0x6F5C);
	ret |= write_16(cam_id, slave_addr, 0x36C0, 0xB0B4);
	ret |= write_16(cam_id, slave_addr, 0x36C2, 0x0456);
	ret |= write_16(cam_id, slave_addr, 0x36C4, 0x1CB8);
	ret |= write_16(cam_id, slave_addr, 0x36C6, 0xAAF9);
	ret |= write_16(cam_id, slave_addr, 0x36C8, 0xF23C);
	ret |= write_16(cam_id, slave_addr, 0x36CA, 0xB054);
	ret |= write_16(cam_id, slave_addr, 0x36CC, 0x0256);
	ret |= write_16(cam_id, slave_addr, 0x36CE, 0x1978);
	ret |= write_16(cam_id, slave_addr, 0x36D0, 0xA559);
	ret |= write_16(cam_id, slave_addr, 0x36D2, 0xED7C);
	ret |= write_16(cam_id, slave_addr, 0x36D4, 0xB2D4);
	ret |= write_16(cam_id, slave_addr, 0x36D6, 0x02D6);
	ret |= write_16(cam_id, slave_addr, 0x36D8, 0x2FB8);
	ret |= write_16(cam_id, slave_addr, 0x36DA, 0xA6B9);
	ret |= write_16(cam_id, slave_addr, 0x36DC, 0x825D);
	ret |= write_16(cam_id, slave_addr, 0x36DE, 0xB1F4);
	ret |= write_16(cam_id, slave_addr, 0x36E0, 0x7FB5);
	ret |= write_16(cam_id, slave_addr, 0x36E2, 0x2838);
	ret |= write_16(cam_id, slave_addr, 0x36E4, 0x9CD9);
	ret |= write_16(cam_id, slave_addr, 0x36E6, 0xFB3C);
	ret |= write_16(cam_id, slave_addr, 0x3700, 0x41F7);
	ret |= write_16(cam_id, slave_addr, 0x3702, 0x6233);
	ret |= write_16(cam_id, slave_addr, 0x3704, 0xAB3C);
	ret |= write_16(cam_id, slave_addr, 0x3706, 0x49DC);
	ret |= write_16(cam_id, slave_addr, 0x3708, 0x7FFF);
	ret |= write_16(cam_id, slave_addr, 0x370A, 0x4277);
	ret |= write_16(cam_id, slave_addr, 0x370C, 0x2EF4);
	ret |= write_16(cam_id, slave_addr, 0x370E, 0xAB9C);
	ret |= write_16(cam_id, slave_addr, 0x3710, 0x415C);
	ret |= write_16(cam_id, slave_addr, 0x3712, 0x7FFF);
	ret |= write_16(cam_id, slave_addr, 0x3714, 0x42B7);
	ret |= write_16(cam_id, slave_addr, 0x3716, 0x25B2);
	ret |= write_16(cam_id, slave_addr, 0x3718, 0xAE9C);
	ret |= write_16(cam_id, slave_addr, 0x371A, 0x4C3C);
	ret |= write_16(cam_id, slave_addr, 0x371C, 0x7FFF);
	ret |= write_16(cam_id, slave_addr, 0x371E, 0x4237);
	ret |= write_16(cam_id, slave_addr, 0x3720, 0x07D4);
	ret |= write_16(cam_id, slave_addr, 0x3722, 0xAB1C);
	ret |= write_16(cam_id, slave_addr, 0x3724, 0x421C);
	ret |= write_16(cam_id, slave_addr, 0x3726, 0x7FFF);
	ret |= write_16(cam_id, slave_addr, 0x3782, 0x0292);
	ret |= write_16(cam_id, slave_addr, 0x3784, 0x0194);
	ret |= write_16(cam_id, slave_addr, 0x37C0, 0xC4B0);
	ret |= write_16(cam_id, slave_addr, 0x37C2, 0xC2B0);
	ret |= write_16(cam_id, slave_addr, 0x37C4, 0xC590);
	ret |= write_16(cam_id, slave_addr, 0x37C6, 0xC230);

	// enable, this is wonky so do again at the beginning
	write_16(cam_id, slave_addr, 0x3780, 0x8000);

	return ret;
}

static int _ar0144_rotate(int cam_id, int en_rotate)
{
	int ret = 0;
	uint8_t val8;

	uint16_t slave_addr = voxl_cci_get_slave_addr(cam_id);
	if((int16_t)slave_addr == -1){
		fprintf(stderr, "failed to fetch slave address for cam_id #%d\n", cam_id);
		return -1;
	}

	// normal, upright
	if(!en_rotate){
		val8 = 0;
		ret |= voxl_cci_write(cam_id, slave_addr, 0x301D, CCI_16BIT, &val8, CCI_8BIT);
	}
	else{
		val8 = 3;
		ret |= voxl_cci_write(cam_id, slave_addr, 0x301D, CCI_16BIT, &val8, CCI_8BIT);
	}

	return ret;
}


int cci_direct_set_rotation(int cam_id, sensor_t sensor, int en_rotate)
{
	voxl_cci_init();

	switch(sensor){

		case SENSOR_OV9782:
		case SENSOR_OV7251:
			return _ov7251_ov9782_rotate(cam_id, en_rotate);
			break;

		case SENSOR_AR0144:
		case SENSOR_AR0144_12BIT:
			return _ar0144_rotate(cam_id, en_rotate);
			break;

		default:
			return 0;
	}

	return 0;
}


static int _ov9782_set_wb_registers(int cam_id)
{
	int ret = 0;

	// for 9782 WITH IR filter
	// uint16_t reg_red = 0x3400;
	// uint16_t val_red = 0x0800; // doubled from default 0x0400
	// uint16_t reg_grn = 0x3402;
	// uint16_t val_grn = 0x0400; // OV default
	// uint16_t reg_blu = 0x3404;
	// uint16_t val_blu = 0x0800; // doubled from default 0x0400


	// for M0113 with no IR filter
	uint16_t reg_red = 0x3400;
	uint16_t val_red = 0x0580; // default 0x0400
	uint16_t reg_grn = 0x3402;
	uint16_t val_grn = 0x0400; // OV default
	uint16_t reg_blu = 0x3404;
	uint16_t val_blu = 0x0900; // default 0x0400

	// multiplier to crank up overall gain closer to the limit
	float mult = 1.5;
	val_red *= mult;
	val_grn *= mult;
	val_blu *= mult;

	uint16_t slave_addr = voxl_cci_get_slave_addr(cam_id);
	if((int16_t)slave_addr == -1){
		fprintf(stderr, "failed to fetch slave address for cam_id #%d\n", cam_id);
		return -1;
	}

	ret |= voxl_cci_write(cam_id, slave_addr, reg_red, CCI_16BIT, (uint8_t*)&val_red, CCI_16BIT);
	ret |= voxl_cci_write(cam_id, slave_addr, reg_grn, CCI_16BIT, (uint8_t*)&val_grn, CCI_16BIT);
	ret |= voxl_cci_write(cam_id, slave_addr, reg_blu, CCI_16BIT, (uint8_t*)&val_blu, CCI_16BIT);

	return ret;
}



int cci_direct_apply_register_tweaks(int cam_id, enum sensor_t sensor)
{
	voxl_cci_init();

	switch(sensor){

		case SENSOR_OV7251:
			return _ov7251_tweaks(cam_id);
			break;

		case SENSOR_AR0144:
			//return _ar0144_set_lsc(cam_id);
			return 0;

		default:
			return 0;
	}

	return 0;
}


int cci_direct_set_white_balance(int cam_id, enum sensor_t sensor)
{
	// currently this is the only Color CV camera we have
	if(sensor == SENSOR_OV9782){
		return _ov9782_set_wb_registers(cam_id);
	}

	return 0;
}


#endif // APQ8096
