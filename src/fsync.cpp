/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>      // for O_WRONLY & O_RDONLY
#include <unistd.h>
#include <sched.h>
#include <pthread.h>
#include <time.h>
#include <inttypes.h>

#include <modal_start_stop.h>

#include "misc.h"
#include "fsync.h"
#include "config_file.h"


#define EN_FILE		"/sys/module/voxl_fsync_mod/parameters/enabled"
#define GPIO_FILE	"/sys/module/voxl_fsync_mod/parameters/gpio_num"
#define PERIOD_FILE	"/sys/module/voxl_fsync_mod/parameters/sampling_period_ns"
#define WIDTH_FILE	"/sys/module/voxl_fsync_mod/parameters/pulse_width_us"
#define TS_FILE		"/dev/voxl-fsync"


#define READ_BUF_LEN 32
static int ts_fd;
static pthread_t ts_thread;
static pthread_mutex_t ts_mtx;


// ringbuf could probably only be 1 or 2 long but keep 4 since it's so lightweight
#define RINGBUF_LEN 4
#define MATCH_TOLERANCE_NS 10000000
static int64_t ts_buf[RINGBUF_LEN];
static int ts_buf_running = 0; // set to 1 after we read the first ts
static int ts_buf_pos = 0; // position in ringbuf


static void* _ts_reader(__attribute__((unused)) void* arg)
{
	char buf[READ_BUF_LEN];
	memset(buf, 0, READ_BUF_LEN*sizeof(char));

	// yes, run until the process stops, no need to start/stop this thread
	while(1){

		// this should be blocking
		int n = read(ts_fd, buf, READ_BUF_LEN-1);
		if(n<0){
			perror("error reading from ts_fd");
			return NULL;
		}
		if(n==0) continue;

		int64_t new_ts;
		n = sscanf(buf, "%" SCNd64, &new_ts);
		if(n<1){
			M_ERROR("failed to parse an integer from %s\n", buf);
			continue;
		}

		// page 13 of the AR0144 developer guide shoes time from trigger to flash
		// as 1.48ms in MIPI mode. Not sure why we need mipi to initialize before
		// staring the exposure but it is what it is. Correct that here.
		new_ts += 1480000;

		pthread_mutex_lock(&ts_mtx);
		ts_buf_pos++;
		if(ts_buf_pos>=RINGBUF_LEN) ts_buf_pos=0;
		ts_buf[ts_buf_pos] = new_ts;
		ts_buf_running = 1;
		//printf("new ts: %lld\n", new_ts);
		pthread_mutex_unlock(&ts_mtx);
	}

	return NULL;
}

// currently, fsync is used for VIO cameras so we can send a command to
// voxl-imu-server to instruct it to read the IMU fifo once we read camera data
// so by the time voxl-qvio-server or voxl-openvins-server get the image data
// there will be imu data waiting and ready
static void _send_imu_read_command(int64_t guess)
{
	static int fd = 0;
	static int64_t last_guess = 0;

	// multiple cameras will be reading the same fsync clock, if one has already
	// signaled the imu read, no need to do it again.
	if(guess == last_guess) return;

	// file is closed, try to open it
	if(fd<=0){
		fd = open("/run/mpa/imu_apps/control", O_WRONLY|O_NONBLOCK);
	}

	if(fd<=0){
		// perror("ERROR opening control pipe");
		return;
	}

	int ret = write(fd, "read", 5);
	if(ret!=5){
		// perror("ERROR writing to control pipe");
		close(fd);
		fd = 0;
	}
	last_guess = guess;
	return;
}


int fsync_fetch_ts(int64_t* guess, int fps)
{
	static int fail_ctr = 0;

	if(!ts_buf_running) return -1;

	int64_t diff;

	pthread_mutex_lock(&ts_mtx);

	// look through the buffer of values for a match. Yes we could start looking
	// at the most recent entry but there are only 4 items in the buffer and the
	// check is so fast that it doesn't matter, keep the code simple instead.
	for(int i=0; i<RINGBUF_LEN; i++){
		diff = *guess - ts_buf[i];

		//printf("%5.1fms ", diff/1000000.0);

		// HAL3 guess is usually within 0.5ms, if the match is within 10ms consider it good
		if(abs(diff)<MATCH_TOLERANCE_NS){
			*guess = ts_buf[i];
			_send_imu_read_command(*guess);
			fail_ctr=0;
			//printf("match\n");
			pthread_mutex_unlock(&ts_mtx);

			// just a debug print
			// printf("diff: %7.2fms  i: %d\n", (double)diff/1000000.0, i);
			// printf("latency: %7.2f\n", (double)(time_monotonic_ns()-ts_buf[i])/1000000.0);
			return 0;
		}
	}

	// in the case where we got the hal3 metadata callback before the timestamp
	// read thread woke up, lets see if we can predict ahead 1 frame. This might happen
	// with very short exposure tiems and high CPU load.

	int64_t period = 1e9 / fps;
	diff = *guess - (ts_buf[ts_buf_pos]+period);
	if(abs(diff)<MATCH_TOLERANCE_NS){
		*guess = ts_buf[ts_buf_pos]+period;
		_send_imu_read_command(*guess);
		pthread_mutex_unlock(&ts_mtx);
		//printf("used extrapolation to predict timestamp\n");
		fail_ctr = 0;
		return 0;
	}

	// okay, really couldn't find a match, log this and unlock mtx
	pthread_mutex_unlock(&ts_mtx);
	fail_ctr++;

	// warn if we aren't getting matches. First few are fine as the threads start up
	if(fail_ctr>4){
		M_WARN("no match found in fsync timestamp buffer\n");
	}
	return -1;
}




int fsync_init(void)
{
	int ret = 0;

	// not enabled, nothing to do
	if(!fsync_en) return 0;

	// open the enable file so we can turn it on or off
	int en_fd = open(EN_FILE, O_WRONLY);
	if(en_fd < 0){
		if(fsync_en){
			M_ERROR("Failed to find voxl-fsync kernel module, likely your system image is too old\n");
		}
		return -1;
	}

	// turn off if disabled
	if(!fsync_en){
		if(write(en_fd, "0", 2)<2){
			perror("error writing to fsync enable fd\n");
			ret = -1;
		}
		else M_DEBUG("Turning off fsync module\n");
		close(en_fd);
		return ret;
	}

	// must be enabled, set gpio and turn on
	int gpio_fd = open(GPIO_FILE, O_WRONLY);
	if(gpio_fd < 0){
		M_ERROR("Failed to open voxl-fsync module gpio file\n");
		close(en_fd);
		return -1;
	}

	char buf[8];
	int len = snprintf(buf, 8, "%d", fsync_gpio);
	len ++; // include null terminator
	if(write(gpio_fd, buf, len)<len){
		perror("error writing to fsync gpio fd\n");
		ret = -1;
	}
	close(gpio_fd);

	if(write(en_fd, "1", 2)<2){
		perror("error writing to fsync enable fd\n");
		ret = -1;
	}
	close(en_fd);

	if(ret){
		M_ERROR("not starting fsync timestamp reader\n");
		return ret;
	}


	// open for blocking reads
	ts_fd = open(TS_FILE, O_RDONLY);
	if(ts_fd < 0){
		M_ERROR("Failed to open fsync timestamp fd\n");
		return -1;
	}


	if(pipe_pthread_create(&ts_thread, _ts_reader, NULL, THREAD_PRIORITY_RT_HIGH)){
		M_ERROR("Failed to start ts reader thread\n");
		return -1;
	}

	return ret;
}
