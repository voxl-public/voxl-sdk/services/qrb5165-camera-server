/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#include <cstdint>
#include <hardware/gralloc.h>
#include <modal_pipe_interfaces.h>
#include <modal_pipe_server.h>
#include <sys/resource.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <iostream>
#include <vector>
#include <string.h>
#include <camera/CameraMetadata.h>
#include <hardware/camera_common.h>
#include <algorithm>
#include <royale/DepthData.hpp>
#include <tof_interface.hpp>
#include <voxl_cutils.h>
#include <modal_journal.h>
#include <modal_pipe.h>

#include "buffer_manager.h"
#include "common_defs.h"
#include "config_file.h"
#include "fsync.h"
#include "hal3_camera_manager.hpp"
#include "hal3_helpers.h"
#include "voxl_camera_server.h"
#include "voxl_cutils.h"
#include "jpeg_size.h"
#include "cv_routines.h"
#include "misc.h"
#include "pipe_clients.h"
#include "cci_direct_helpers.h"
#include "opencl_helpers.hpp"

// OSD
#include <math.h>   // For round() when creating horizon bar on OSD
#include "cCharacter.h"

#define EXPOSURE_CONTROL_COMMANDS "set_exp_gain,set_exp,set_gain,start_ae,stop_ae"

#define NUM_PREVIEW_BUFFERS  16 // used to be 32, really shouldnt need to be more than 7
#define NUM_SNAPSHOT_BUFFERS 16 // used to be 8, just making it consistent with the rest that are now 16
#define NUM_MISP_BUFFERS     16
#define SMALL_VID_ALLOWED_ITEMS_IN_OMX_QUEUE 1 // favor latency when dropping frames
#define LARGE_VID_ALLOWED_ITEMS_IN_OMX_QUEUE 2 // only drop frames when really getting behind

#define JPEG_DEFUALT_QUALITY        75

#define MAX_STEREO_DISCREPENCY_NS ((1000000000/configInfo.fps)*0.9)

// Platform Specific Flags
#ifdef APQ8096
    #define ROTATION_MODE  CAMERA3_STREAM_ROTATION_0
    #define OPERATION_MODE QCAMERA3_VENDOR_STREAM_CONFIGURATION_RAW_ONLY_MODE
    #define ENCODER_USAGE  GRALLOC_USAGE_HW_COMPOSER | GRALLOC_USAGE_HW_TEXTURE
    #define SNAPSHOT_DS    HAL_DATASPACE_JFIF
    #define NUM_STREAM_BUFFERS   16
    #define NUM_RECORD_BUFFERS   16
#elif QRB5165
    #define ROTATION_MODE  2
    #define OPERATION_MODE CAMERA3_STREAM_CONFIGURATION_NORMAL_MODE
    #define ENCODER_USAGE  GRALLOC_USAGE_HW_VIDEO_ENCODER
    #define SNAPSHOT_DS    HAL_DATASPACE_V0_JFIF
    #define NUM_STREAM_BUFFERS   16 // shouldn't need more than 10, if the buffer pool is empty then OMX should be dropping more frames
    #define NUM_RECORD_BUFFERS   16 // shouldn't need more than 10, if the buffer pool is empty then OMX should be dropping more frames
#else
    #error "No Platform defined"
#endif

// Libexif includes for QRB platform
#ifndef APQ8096
#include <libexif/exif-data.h>
#include <libexif/exif-entry.h>
#include <libexif/exif-utils.h>
/* raw EXIF header data */
static const unsigned char exif_header[] = {
  0xff, 0xe1
};
static const unsigned int exif_header_len = sizeof(exif_header);
#endif

// -----------------------------------------------------------------------------------------------------------------------------
// Constructor
// -----------------------------------------------------------------------------------------------------------------------------
PerCameraMgr::PerCameraMgr(PerCameraInfo pCameraInfo) :
    configInfo        (pCameraInfo),
    cameraId          (pCameraInfo.camId),
    en_preview        (pCameraInfo.en_preview),
    en_small_video         (pCameraInfo.en_small_video),
    en_large_video         (pCameraInfo.en_large_video),
    en_misp           (pCameraInfo.en_misp),
    en_snapshot       (pCameraInfo.en_snapshot),
    fps               (pCameraInfo.fps),
    focal_length      (pCameraInfo.focal_length),
    focal_len_35mm_format(pCameraInfo.focal_len_35mm_format),
    fnumber           (pCameraInfo.fnumber),
    pre_width         (pCameraInfo.pre_width),
    pre_height        (pCameraInfo.pre_height),
    pre_halfmt        (previewHalFmtFromInfo(pCameraInfo)),
    vid_halfmt                (HAL_PIXEL_FORMAT_YCbCr_420_888),
    small_video_width         (pCameraInfo.small_video_width),
    small_video_height        (pCameraInfo.small_video_height),
    large_video_width         (pCameraInfo.large_video_width),
    large_video_height        (pCameraInfo.large_video_height),
    misp_width        (pCameraInfo.misp_width),
    misp_height       (pCameraInfo.misp_height),
    snap_width        (pCameraInfo.snap_width),
    snap_height       (pCameraInfo.snap_height),
    snap_halfmt       (HAL_PIXEL_FORMAT_BLOB),
    ae_mode           (pCameraInfo.ae_mode),
    pCameraModule     (HAL3_get_camera_module()),
    pVideoEncoderSmall(NULL),
    pVideoEncoderLarge(NULL),
    pVideoEncoderMisp(NULL),
    expHistInterface  (pCameraInfo.ae_hist_info),
    expMSVInterface   (pCameraInfo.ae_msv_info)
{

    strcpy(name, pCameraInfo.name);

    cameraCallbacks.cameraCallbacks = {&CameraModuleCaptureResult, &CameraModuleNotify};
    cameraCallbacks.pPrivate        = this;

    if(pCameraModule == NULL ){
        M_ERROR("Failed to get HAL module!\n");
        throw -EINVAL;
    }

    // Check if the stream configuration is supported by the camera or not. If cameraid doesnt support the stream configuration
    // we just exit. The stream configuration is checked into the static metadata associated with every camera.
    if (en_preview && !HAL3_is_config_supported(cameraId, pre_width, pre_height, pre_halfmt))
    {
        M_ERROR("Camera %d failed to find supported preview config: %dx%d\n", cameraId, pre_width, pre_height);

        throw EBADSLT;
    }
    if (en_small_video && !HAL3_is_config_supported(cameraId, small_video_width, small_video_height, vid_halfmt))
    {
        M_ERROR("Camera %d failed to find supported stream config: %dx%d\n", cameraId, small_video_width, small_video_height);

        throw EBADSLT;
    }
    if (en_large_video && !HAL3_is_config_supported(cameraId, large_video_width, large_video_height, vid_halfmt))
    {
        M_ERROR("Camera %d failed to find supported record config: %dx%d\n", cameraId, large_video_width, large_video_height);

        throw EBADSLT;
    }
    if (en_snapshot && !HAL3_is_config_supported(cameraId, snap_width, snap_height, snap_halfmt))
    {
        M_ERROR("Camera %d failed to find supported snapshot config: %dx%d\n", cameraId, snap_width, snap_height);

        throw EBADSLT;
    }

    char cameraName[20];
    sprintf(cameraName, "%d", cameraId);

    if (pCameraModule->common.methods->open(&pCameraModule->common, cameraName, (hw_device_t**)(&pDevice)))
    {
        M_ERROR("Open camera %s failed!\n", name);

        throw -EINVAL;
    }

    if (pDevice->ops->initialize(pDevice, (camera3_callback_ops*)&cameraCallbacks))
    {
        M_ERROR("Initialize camera %s failed!\n", name);

        throw -EINVAL;
    }

    if (ConfigureStreams())
    {
        M_ERROR("Failed to configure streams for camera: %s\n", name);

        throw -EINVAL;
    }

    if (en_preview) {
        if (bufferAllocateBuffers(pre_bufferGroup,
                                  NUM_PREVIEW_BUFFERS,
                                  pre_stream.width,
                                  pre_stream.height,
                                  pre_stream.format,
                                  pre_stream.usage)) {
            M_ERROR("Failed to allocate preview buffers for camera: %s\n", name);

            throw -EINVAL;
        }
        M_DEBUG("Successfully set up pipeline for stream: PREVIEW\n");
    }

    if (en_small_video) {

        if (bufferAllocateBuffers(small_vid_bufferGroup,
                                  NUM_STREAM_BUFFERS,
                                  small_vid_stream.width,
                                  small_vid_stream.height,
                                  small_vid_stream.format,
                                  small_vid_stream.usage)) {
            M_ERROR("Failed to allocate encode buffers for camera: %s\n", name);
            throw -EINVAL;
        }

        try{
            VideoEncoderConfig enc_info;
            enc_info = {
                .width =             (uint32_t)small_video_width,
                .height =            (uint32_t)small_video_height,
                .format =            (uint32_t)vid_halfmt,
                .venc_config =       pCameraInfo.small_venc_config,
                .frameRate =         pCameraInfo.fps,
                .inputBuffers =      &small_vid_bufferGroup,
                .outputPipe =        &smallVideoPipeEncoded
            };
            pVideoEncoderSmall = new VideoEncoder(&enc_info);
            if(enc_info.venc_config.osd) setSmallOsdConfig(getDefaultOSDConfig(enc_info.width, enc_info.height));
        } catch(int) {
            M_ERROR("Failed to initialize encoder for camera: %s\n", name);
            throw -EINVAL;
        }
        M_DEBUG("Successfully set up pipeline for stream: STREAM_SMALL_VID\n");
    }

    if (en_large_video) {

        if (bufferAllocateBuffers(large_vid_bufferGroup,
                                  NUM_RECORD_BUFFERS,
                                  large_vid_stream.width,
                                  large_vid_stream.height,
                                  large_vid_stream.format,
                                  large_vid_stream.usage)) {
            M_ERROR("Failed to allocate encode buffers for camera: %s\n", name);
            throw -EINVAL;
        }

        try{
            VideoEncoderConfig enc_info;
            enc_info = {
                .width =             (uint32_t)large_video_width,
                .height =            (uint32_t)large_video_height,
                .format =            (uint32_t)vid_halfmt,
                .venc_config =       pCameraInfo.large_venc_config,
                .frameRate =         pCameraInfo.fps,
                .inputBuffers =      &large_vid_bufferGroup,
                .outputPipe =        &largeVideoPipeEncoded
            };

            pVideoEncoderLarge = new VideoEncoder(&enc_info);
            if(enc_info.venc_config.osd) setLargeOsdConfig(getDefaultOSDConfig(enc_info.width, enc_info.height));
        } catch(int) {
            M_ERROR("Failed to initialize encoder for camera: %s\n", name);
            throw -EINVAL;
        }
        M_DEBUG("Successfully set up pipeline for stream: STREAM_LARGE_VID\n");
    }

    if (en_misp) {
        if (!en_preview){
            M_ERROR("MISP enabled for camera %s, but preview stream is not enabled. Please enable preview stream\n",name);
            throw -EINVAL;
        }
        if (!configInfo.en_raw_preview){
            M_ERROR("MISP enabled for camera %s, but en_raw_preview is not enabled. Please set en_raw_preview to true\n",name);
            throw -EINVAL;
        }

        if (misp_width <= 0 || misp_height <=0){
            misp_width  = pre_width;
            misp_height = pre_height;
        }

        misp = new Misp();
        misp->Init(misp_width, misp_height);

        //| GRALLOC_USAGE_PRIVATE_UNCACHED
        if (bufferAllocateBuffers(misp_bufferGroup,
                                  NUM_MISP_BUFFERS,
                                  misp_width,
                                  misp_height,
                                   /*HAL_PIXEL_FORMAT_RGBA_8888, */
                                  HAL_PIXEL_FORMAT_YCbCr_420_888,
                                  ENCODER_USAGE | GRALLOC_USAGE_PRIVATE_UNCACHED
                                  )) {
            M_ERROR("Failed to allocate misp buffers for camera: %s\n", name);
            throw -EINVAL;
        }

        try{
            if (pCameraInfo.en_misp_encoder){
                VideoEncoderConfig enc_info;
                enc_info = {
                    .width =             (uint32_t)misp_width,
                    .height =            (uint32_t)misp_height,
                    .format =            (uint32_t)vid_halfmt,
                    .venc_config =       pCameraInfo.misp_venc_config,
                    .frameRate =         pCameraInfo.fps,
                    .inputBuffers =      &misp_bufferGroup,
                    .outputPipe =        &mispPipeEncoded
                };

                if(enc_info.venc_config.osd) setSmallOsdConfig(getDefaultOSDConfig(enc_info.width, enc_info.height));
                pVideoEncoderMisp = new VideoEncoder(&enc_info);
            }
        } catch(int) {
            M_ERROR("Failed to initialize MISP encoder for camera: %s\n", name);
            throw -EINVAL;
        }

        misp->SetAwbManualGains(pCameraInfo.misp_abw_gains[0], pCameraInfo.misp_abw_gains[1], pCameraInfo.misp_abw_gains[2]);
        misp->SetGammaCorrection(pCameraInfo.misp_gamma_correction);

        M_DEBUG("Successfully set up pipeline for stream: MISP\n");

    }

    if (en_snapshot) {

        camera_info halCameraInfo;
        pCameraModule->get_camera_info(cameraId, &halCameraInfo);
        camera_metadata_t* pStaticMetadata = (camera_metadata_t *)halCameraInfo.static_camera_characteristics;

        int blobWidth = estimateJpegBufferSize(pStaticMetadata, snap_width, snap_height);

        if(bufferAllocateBuffers(snap_bufferGroup,
                                 NUM_SNAPSHOT_BUFFERS,
                                 blobWidth,
                                 1,
                                 snap_stream.format,
                                 snap_stream.usage)) {
            M_ERROR("Failed to allocate snapshot buffers for camera: %s\n", name);

            throw -EINVAL;
        }
        M_DEBUG("Successfully set up pipeline for stream: SNAPSHOT\n");
    }

#ifndef APQ8096

    // apply any register tweaks through cci-direct, things that should go into
    // the camera driver in a future system image
    cci_direct_apply_register_tweaks(configInfo.camId, configInfo.type);

    // set rotation and white balance, only for certain cameras
    cci_direct_set_rotation(configInfo.camId, configInfo.type, configInfo.en_rotate);

    //do not set static white balance if misp is enabled, since it will do its own white balance
    if(en_preview && configInfo.en_raw_preview && !en_misp){
        cci_direct_set_white_balance(configInfo.camId, configInfo.type);
    }
#endif

    // set up slave for stereo mode
    if(configInfo.camId2 == -1){
        partnerMode = MODE_MONO;
    } else {
        // recursive CTOR on stereo cams
        partnerMode = MODE_STEREO_MASTER;

        PerCameraInfo newInfo = configInfo;
        sprintf(newInfo.name, "%s%s", name, "_child");
        newInfo.camId = newInfo.camId2;
        newInfo.camId2 = -1;

        // These are disabled until(if) we figure out a good way to handle them
        newInfo.en_small_video = false;
        newInfo.en_large_video = false;
        newInfo.en_snapshot = false;

        // since this is a camId_second, look at the rotation_second value for
        // independent rotation h/w config
        newInfo.en_rotate = newInfo.en_rotate2;

        otherMgr = new PerCameraMgr(newInfo);

        otherMgr->setMaster(this);
    }
}

PerCameraMgr::~PerCameraMgr() {
    if (tof_interface){
        delete tof_interface;
    }
    if (partnerMode == MODE_STEREO_MASTER){
        delete otherMgr;
    }

    if (misp){
        delete misp;
        misp = NULL;
    }
}


// -----------------------------------------------------------------------------------------------------------------------------
// Create the streams that we will use to communicate with the camera module
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::ConfigureStreams()
{

    std::vector<camera3_stream_t*> streams;

    camera3_stream_configuration_t streamConfig = { 0 };
    streamConfig.num_streams    = 0;

    uint32_t pre_usage = GRALLOC_USAGE_HW_COMPOSER | GRALLOC_USAGE_HW_TEXTURE;
    if(is_tof_sensor(configInfo.type)) {
        pre_usage = GRALLOC_USAGE_HW_CAMERA_WRITE;
    }

#ifndef APQ8096
    const char* cameraName = "";
#endif

    if(en_preview){
        pre_stream.stream_type = CAMERA3_STREAM_OUTPUT;
        pre_stream.width       = pre_width;
        pre_stream.height      = pre_height;
        pre_stream.format      = pre_halfmt;
        pre_stream.data_space  = HAL_DATASPACE_UNKNOWN;
        // TODO: figure out if there's another way to output nv12 instead of
        // nv21 for preview. these flags do it, but they also cause buffers to
        // be way oversized.
        pre_stream.usage       = pre_usage;
        pre_stream.rotation    = ROTATION_MODE;
        pre_stream.max_buffers = NUM_PREVIEW_BUFFERS;
        pre_stream.priv        = 0;
#ifndef APQ8096
        pre_stream.physical_camera_id = cameraName;
#endif

        streams.push_back(&pre_stream);
        streamConfig.num_streams ++;
        M_VERBOSE("Adding preview stream for camera: %d\n", cameraId);
    }

    if(en_small_video) {
        small_vid_stream.stream_type = CAMERA3_STREAM_OUTPUT;
        small_vid_stream.width       = small_video_width;
        small_vid_stream.height      = small_video_height;
        small_vid_stream.format      = vid_halfmt;
        small_vid_stream.data_space  = HAL_DATASPACE_UNKNOWN;
        small_vid_stream.usage       = ENCODER_USAGE;
        small_vid_stream.rotation    = ROTATION_MODE;
        small_vid_stream.max_buffers = NUM_STREAM_BUFFERS;
        small_vid_stream.priv        = 0;
#ifndef APQ8096
        small_vid_stream.physical_camera_id = cameraName;
#endif

        streams.push_back(&small_vid_stream);
        streamConfig.num_streams ++;
        M_VERBOSE("Adding small video stream for camera: %d\n", cameraId);
    }

    if(en_large_video) {
        large_vid_stream.stream_type = CAMERA3_STREAM_OUTPUT;
        large_vid_stream.width       = large_video_width;
        large_vid_stream.height      = large_video_height;
        large_vid_stream.format      = vid_halfmt;
        large_vid_stream.data_space  = HAL_DATASPACE_UNKNOWN;
        large_vid_stream.usage       = ENCODER_USAGE;
        large_vid_stream.rotation    = ROTATION_MODE;
        large_vid_stream.max_buffers = NUM_RECORD_BUFFERS;
        large_vid_stream.priv        = 0;
#ifndef APQ8096
        large_vid_stream.physical_camera_id = cameraName;
#endif

        streams.push_back(&large_vid_stream);
        streamConfig.num_streams ++;
        M_VERBOSE("Adding large video stream for camera: %d\n", cameraId);
    }

    if(en_snapshot) {
        snap_stream.stream_type = CAMERA3_STREAM_OUTPUT;
        snap_stream.width       = snap_width;
        snap_stream.height      = snap_height;
        snap_stream.format      = snap_halfmt;
        snap_stream.data_space  = SNAPSHOT_DS;
        snap_stream.usage       = GRALLOC_USAGE_SW_READ_OFTEN;
        snap_stream.rotation    = ROTATION_MODE;
        snap_stream.max_buffers = NUM_SNAPSHOT_BUFFERS;
        snap_stream.priv        = 0;
#ifndef APQ8096
        snap_stream.physical_camera_id = cameraName;
#endif

        streams.push_back(&snap_stream);
        streamConfig.num_streams ++;
        M_VERBOSE("Adding snapshot stream for camera: %d\n", cameraId);
    }

    if(streamConfig.num_streams==0){
        M_ERROR("No streams enabled for for camera: %d\n", cameraId);
        return -EINVAL;
    }

    num_streams = streamConfig.num_streams;
    streamConfig.streams        = streams.data();
    streamConfig.operation_mode = OPERATION_MODE;
#ifndef APQ8096
    pSessionParams = allocate_camera_metadata(2, 8);
    int32_t frame_rate_rate[] = {configInfo.fps,configInfo.fps};
    add_camera_metadata_entry(pSessionParams,
                              ANDROID_CONTROL_AE_TARGET_FPS_RANGE,
                              frame_rate_rate, 2);

    streamConfig.session_parameters = pSessionParams;
#endif
    // Call into the camera module to check for support of the required stream config i.e. the required usecase
    if (pDevice->ops->configure_streams(pDevice, &streamConfig))
    {
        M_ERROR("Configure streams failed for camera: %d\n", cameraId);
        return -EINVAL;
    }

    return S_OK;
}


// -----------------------------------------------------------------------------------------------------------------------------
// Construct default camera settings that will be passed to the camera module to be used for capturing the frames
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::ConstructDefaultRequestSettings()
{

    // Get the default baseline settings
    camera_metadata_t* pDefaultMetadata =
            (camera_metadata_t *)pDevice->ops->construct_default_request_settings(pDevice, CAMERA3_TEMPLATE_PREVIEW);

    if(en_snapshot){
        pDefaultMetadata =
                    //(camera_metadata_t *)pDevice->ops->construct_default_request_settings(pDevice, CAMERA3_TEMPLATE_STILL_CAPTURE);
                    (camera_metadata_t *)pDevice->ops->construct_default_request_settings(pDevice, CAMERA3_TEMPLATE_VIDEO_RECORD);
    }

    // Modify all the settings that we want to
    requestMetadata = clone_camera_metadata(pDefaultMetadata);

    switch (ae_mode){

        case AE_OFF :
        case AE_LME_HIST :
        case AE_LME_MSV : {

            //This covers the 5 below modes, we want them all off
            uint8_t controlMode = ANDROID_CONTROL_MODE_OFF;
            uint8_t aeMode            =  ANDROID_CONTROL_AE_MODE_OFF;
            uint8_t antibanding       =  ANDROID_CONTROL_AE_ANTIBANDING_MODE_OFF;
            uint8_t afMode            =  ANDROID_CONTROL_AF_MODE_OFF;
            uint8_t awbMode           =  ANDROID_CONTROL_AWB_MODE_AUTO; //ANDROID_CONTROL_AWB_MODE_OFF;
            uint8_t faceDetectMode    =  ANDROID_STATISTICS_FACE_DETECT_MODE_OFF;

            //This covers the 5 below modes, we want them all off
            requestMetadata.update(ANDROID_CONTROL_MODE,                &controlMode,        1);
            requestMetadata.update(ANDROID_CONTROL_AE_MODE,             &aeMode,             1);
            requestMetadata.update(ANDROID_STATISTICS_FACE_DETECT_MODE, &faceDetectMode,     1);
            requestMetadata.update(ANDROID_CONTROL_AF_MODE,             &afMode,             1);
            requestMetadata.update(ANDROID_CONTROL_AE_ANTIBANDING_MODE, &antibanding,        1);
            requestMetadata.update(ANDROID_CONTROL_AWB_MODE,            &awbMode,            1);

            break;
        }

        case AE_ISP : {

            uint8_t aeMode            =  ANDROID_CONTROL_AE_MODE_ON;
            uint8_t antibanding       =  ANDROID_CONTROL_AE_ANTIBANDING_MODE_OFF;
            uint8_t awbMode           =  ANDROID_CONTROL_AWB_MODE_AUTO;
            uint8_t afMode            =  ANDROID_CONTROL_AF_MODE_OFF;
            uint8_t faceDetectMode    =  ANDROID_STATISTICS_FACE_DETECT_MODE_OFF;

            requestMetadata.update(ANDROID_CONTROL_AE_MODE,             &aeMode,             1);
            requestMetadata.update(ANDROID_CONTROL_AE_ANTIBANDING_MODE, &antibanding,        1);
            requestMetadata.update(ANDROID_CONTROL_AWB_MODE,            &awbMode,            1);
            requestMetadata.update(ANDROID_STATISTICS_FACE_DETECT_MODE, &faceDetectMode,     1);
            requestMetadata.update(ANDROID_CONTROL_AF_MODE,             &afMode,             1);
            break;
        }

   	    default:
   	        M_ERROR("unknown ae mode: %d\n", ae_mode);
   	        return -1;

    }

    if(en_snapshot){
        uint8_t jpegQuality     = JPEG_DEFUALT_QUALITY;

        requestMetadata.update(ANDROID_JPEG_QUALITY, &(jpegQuality), sizeof(jpegQuality));
    }

    int fpsRange[] = {configInfo.fps, configInfo.fps};
    int64_t frameDuration = 1e9 / configInfo.fps;

    requestMetadata.update(ANDROID_CONTROL_AE_TARGET_FPS_RANGE, &fpsRange[0],        2);
    requestMetadata.update(ANDROID_SENSOR_FRAME_DURATION,       &frameDuration,      1);

    if(is_tof_sensor(configInfo.type)) {

        setExposure = 2259763;
        setGain     = 200;

        if (configInfo.type == SENSOR_TOF){
            if (configInfo.fps != 5 && configInfo.fps != 10 && configInfo.fps != 15 && configInfo.fps != 30 && configInfo.fps != 45){
                M_ERROR("Invalid TOF framerate: %d, must be either 5, 10, 15, 30, or 45\n", configInfo.fps);
                return -1;
            }
        }

        if (configInfo.type == SENSOR_TOF_LIOW2){
            if (configInfo.fps != 5 && configInfo.fps != 10 && configInfo.fps != 15 && configInfo.fps != 20 && configInfo.fps != 30 && configInfo.fps != 60){
                M_ERROR("Invalid TOF framerate: %d, must be either 5, 10, 15, 20, 30, or 60\n", configInfo.fps);
                return -1;
            }
        }

        RoyaleListenerType dataType = RoyaleListenerType::LISTENER_DEPTH_DATA;

        TOFInitializationData initializationData = { 0 };

        initializationData.pDataTypes    = &dataType;
        initializationData.numDataTypes  = 1;
        initializationData.pListener     = this;
        initializationData.frameRate     = configInfo.fps;
        initializationData.range         = RoyaleDistanceRange::LONG_RANGE;

        #ifdef APQ8096
            VCU_silent(
                tof_interface = TOFCreateInterface();
            )
            if( tof_interface == NULL) {
                M_ERROR("Failed to create tof interface\n");
                return -1;
            }
            initializationData.pTOFInterface = tof_interface;
            int ret;
            VCU_silent(
                ret = TOFInitialize(&initializationData);
            )
            if(ret) {
                M_ERROR("Failed to initialize tof interface\n");
                return -1;
            }
        #elif QRB5165
            initializationData.cameraId      = cameraId;
            auto model = (configInfo.type == SENSOR_TOF_LIOW2) ? TOFModel::LIOW2 : TOFModel::A65;
            tof_interface = new TOFInterface(&initializationData, model);

            if (!tof_interface){
                M_ERROR("FATAL: Could not create new tof interface");
                return -1;
            }

            //the max exposure limit works on both TOF sensors, but lets not change the way TOF V1 works
            if (configInfo.type == SENSOR_TOF_LIOW2) {
                int newAutoExposureMaxLimit = configInfo.ae_msv_info.exposure_max_us;
                M_VERBOSE("seting TOF LIOW2 exposure to %d\n",newAutoExposureMaxLimit);
                if (tof_interface->setMaxAutoExposure(newAutoExposureMaxLimit)){
                    M_ERROR("TOF LIOW2 failed to set max auto exposure limit of %uus\n",newAutoExposureMaxLimit);
                }
            }
        #endif
        M_VERBOSE("TOF interface created!\n");
    }

    return 0;
}


// just malloc space for now. Can put other setup stuff here in the future
void PerCameraMgr::SetupRawProcessing()
{
    if(is_tof_sensor(configInfo.type)) return;
    if(!en_preview) return;

    // only use raw processing with RAW types
    if( pre_halfmt != HAL_PIXEL_FORMAT_RAW10 && \
        pre_halfmt != HAL_PIXEL_FORMAT_RAW12 && \
        pre_halfmt != HAL_PIXEL_FORMAT_RAW16)
    {
        return;
    }

    // all cameras get a grey output
    // no need to malloc for the bayered data, that we read straight from image buffer
    previewFrameGrey8 = (uint8_t*)malloc(pre_width * pre_height);

    // color cameras also get a color output
    if(configInfo.bayer_fmt != BAYER_MONO){
        previewFrameRGB8 = (uint8_t*)malloc(pre_width * pre_height * 3);  //TODO: free memory in destructor
    }

    return;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function opens the camera and starts sending the capture requests
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::Start()
{
    if(partnerMode != MODE_STEREO_SLAVE){
        if(SetupPipes()){
            M_ERROR("Failed to setup pipes for camera: %s\n", name);

            throw -EINVAL;
        }
    }

    SetupRawProcessing();

    if(pVideoEncoderSmall) {
        pVideoEncoderSmall->Start();
    }

    if(pVideoEncoderLarge) {
        pVideoEncoderLarge->Start();
    }

    if(pVideoEncoderMisp) {
        pVideoEncoderMisp->Start();
    }

    pthread_condattr_t condAttr;
    pthread_condattr_init(&condAttr);
    pthread_condattr_setclock(&condAttr, CLOCK_MONOTONIC);
    pthread_mutex_init(&resultMutex, NULL);
    pthread_mutex_init(&stereoMutex, NULL);
    pthread_mutex_init(&aeMutex, NULL);
    pthread_cond_init(&resultCond, &condAttr);
    pthread_cond_init(&stereoCond, &condAttr);
    pthread_condattr_destroy(&condAttr);

    // Start the thread that will process the camera capture result. This thread wont exit till it consumes all expected
    // output buffers from the camera module or it encounters a fatal error
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&requestThread, &attr, [](void* data){return ((PerCameraMgr*)data)->ThreadIssueCaptureRequests();}, this);
    pthread_create(&resultThread,  &attr, [](void* data){return ((PerCameraMgr*)data)->ThreadPostProcessResult();},  this);
    pthread_attr_destroy(&attr);

    if(partnerMode == MODE_STEREO_MASTER){
        // // sleep to test stereo syncing, leave this off normally!!
        // usleep(15000);
        otherMgr->Start();
    }

}

// -----------------------------------------------------------------------------------------------------------------------------
// This function stops the camera and does all necessary clean up
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::Stop()
{

    stopped = true;

    if(partnerMode == MODE_STEREO_MASTER){
        otherMgr->stopped = true;
    }

    pthread_join(requestThread, NULL);

    pthread_cond_broadcast(&stereoCond);
    pthread_cond_broadcast(&resultCond);

    pthread_cond_signal(&resultCond);
    pthread_mutex_unlock(&resultMutex);
    pthread_join(resultThread, NULL);


    pthread_mutex_destroy(&resultMutex);
    pthread_cond_destroy(&resultCond);

    if(partnerMode == MODE_STEREO_MASTER){
        M_DEBUG("Camera %s Master calling stop to slave\n", name);
        otherMgr->Stop();
        M_DEBUG("Camera %s Master finished stopping slave\n", name);
    }

    if(pVideoEncoderSmall) {
        pVideoEncoderSmall->Stop();
        //delete pVideoEncoderSmall;
    }

    if(pVideoEncoderLarge) {
        pVideoEncoderLarge->Stop();
        //delete pVideoEncoderLarge;
    }

    if (pVideoEncoderMisp) {
        pVideoEncoderMisp->Stop();
        //delete pVideoEncoderMisp;
    }

    M_DEBUG("Deleting buffers for %s preview stream\n", name);
    bufferDeleteBuffers(pre_bufferGroup);
    M_DEBUG("Deleting buffers for %s small video stream\n", name);
    bufferDeleteBuffers(small_vid_bufferGroup);
    M_DEBUG("Deleting buffers for %s large video stream\n", name);
    bufferDeleteBuffers(large_vid_bufferGroup);
    M_DEBUG("Deleting buffers for %s snapshot stream\n", name);
    bufferDeleteBuffers(snap_bufferGroup);
    M_DEBUG("Deleting buffers for %s misp stream\n", name);
    bufferDeleteBuffers(misp_bufferGroup);


    if(tof_interface){
        M_DEBUG("stopping tof_interface for %s\n", name);
        delete tof_interface;
        tof_interface = nullptr;
    }

#ifdef APQ8096
    // Stereo master on apq8096 seems to hang on "close" for some reason
    if(pDevice != NULL && partnerMode != MODE_STEREO_MASTER){
        pDevice->common.close(&pDevice->common);
        M_DEBUG("Done closing device for camera %s\n", name);
        pDevice = NULL;
    }
#else
    if(pDevice != NULL){
        pDevice->common.close(&pDevice->common);
        M_DEBUG("Done closing device for camera %s\n", name);
        pDevice = NULL;
    }
#endif

    if(pSessionParams != NULL){
        free_camera_metadata(pSessionParams);
    }

    pthread_mutex_destroy(&stereoMutex);
    pthread_cond_destroy(&stereoCond);

    pthread_mutex_destroy(&aeMutex);

    close_my_pipes();

    M_DEBUG("Done calling stop for camera %s\n", name);
    return;
}


// -----------------------------------------------------------------------------------------------------------------------------
// Function that will process one capture result sent from the camera module. Remember this function is operating in the camera
// module thread context. So we do the bare minimum work that we need to do and return control back to the camera module. The
// bare minimum we do is to dispatch the work to another worker thread who consumes the image buffers passed to it from here.
// Our result worker thread is "ThreadPostProcessResult(..)"
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::ProcessOneCaptureResult(const camera3_capture_result* pHalResult)
{
    M_VERBOSE("Received %d buffers from camera %s, partial result:%d\n", pHalResult->num_output_buffers, name, pHalResult->partial_result);

    if(pHalResult->partial_result > 1){

        camera_image_metadata_t meta;
        meta.frame_id=pHalResult->frame_number;
        meta.framerate = fps;

        M_VERBOSE("Received metadata for frame %d from camera %s\n", pHalResult->frame_number, name);

        int result = 0;
        camera_metadata_ro_entry entry;

        result = find_camera_metadata_ro_entry(pHalResult->result, ANDROID_SENSOR_TIMESTAMP, &entry);

        if (!result && entry.count)
        {
            meta.timestamp_ns = entry.data.i64[0];
            M_VERBOSE("\tTimestamp: %llu\n", meta.timestamp_ns);
            if(fsync_en && (configInfo.type == SENSOR_AR0144 || configInfo.type == SENSOR_AR0144_12BIT)){
                fsync_fetch_ts(&meta.timestamp_ns, fps);
            }
        }

        result = find_camera_metadata_ro_entry(pHalResult->result, ANDROID_SENSOR_SENSITIVITY, &entry);

        if (!result && entry.count)
        {
            meta.gain = entry.data.i32[0];
            M_VERBOSE("\tGain: %d\n", meta.gain);
        }

        result = find_camera_metadata_ro_entry(pHalResult->result, ANDROID_SENSOR_EXPOSURE_TIME, &entry);

        if (!result && entry.count)
        {
            meta.exposure_ns = entry.data.i64[0];
            M_VERBOSE("\tExposure: %ld\n", meta.exposure_ns);
        }

        resultMetaRing.insert_data(meta);

        // check if there are result buffer returned before the meta data is avaliable
        image_result result_list[16]; // only 3 is needed
        int num = getAllResult(pHalResult->frame_number, result_list);
        if (num > 0 ) {
            M_VERBOSE("Find %d buffer in result ring for frame %d\n", num, pHalResult->frame_number);
            pthread_mutex_lock(&resultMutex);

            // Queue up work for the result thread "ThreadPostProcessResult"
            for (int i = 0; i < num; i++){
               resultMsgQueue.push(result_list[i]);
            }
            pthread_cond_signal(&resultCond);
            pthread_mutex_unlock(&resultMutex);
        }

    }


    for (uint i = 0; i < pHalResult->num_output_buffers; i++)
    {

        M_VERBOSE("Received output buffer %d from camera %s\n", pHalResult->frame_number, name);

        // Mutex is required for msgQueue access from here and from within the thread wherein it will be de-queued
        // if buffer arrive before meta data, save them in the ringbuffer
        image_result i_result = {pHalResult->frame_number, pHalResult->output_buffers[i]};
        camera_image_metadata_t imageInfo;
        if(getMeta(i_result.first, &imageInfo)) {
            M_VERBOSE("Buffer arrive before meta frame %d\n", i_result.first);
            resultMsgRing.insert_data(i_result);
        } else {
            pthread_mutex_lock(&resultMutex);

            // Queue up work for the result thread "ThreadPostProcessResult"
            resultMsgQueue.push(i_result);
            pthread_cond_signal(&resultCond);
            pthread_mutex_unlock(&resultMutex);
        }

    }

}

// -----------------------------------------------------------------------------------------------------------------------------
// Process the result from the camera module. Essentially handle the metadata and the image buffers that are sent back to us.
// We call the PerCameraMgr class function to handle it so that it can have access to any (non-static)class member data it needs
// Remember this function is operating in the camera module thread context. So we should do the bare minimum work and return.
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::CameraModuleCaptureResult(const camera3_callback_ops_t *cb, const camera3_capture_result* pHalResult)
{
    M_VERBOSE("Received result from HAl3 for frame number %d\n", pHalResult->frame_number);
    Camera3Callbacks* pCamera3Callbacks = (Camera3Callbacks*)cb;
    PerCameraMgr* pPerCameraMgr = (PerCameraMgr*)pCamera3Callbacks->pPrivate;

    pPerCameraMgr->ProcessOneCaptureResult(pHalResult);
}

// -----------------------------------------------------------------------------------------------------------------------------
// Handle any messages sent to us by the camera module
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::CameraModuleNotify(const camera3_callback_ops_t *cb, const camera3_notify_msg_t *msg)
{

    PerCameraMgr* pPerCameraMgr = (PerCameraMgr*)((Camera3Callbacks*)cb)->pPrivate;
    if(pPerCameraMgr->stopped) return;

    if (msg->type == CAMERA3_MSG_ERROR)
    {
        switch (msg->message.error.error_code) {

            case CAMERA3_MSG_ERROR_DEVICE:
                //Another thread has already detected the fatal error, return since it has already been handled
                if(pPerCameraMgr->EStopped) return;

                M_ERROR("Received \"Device\" error from camera: %s\n",
                             pPerCameraMgr->name);
                M_PRINT(  "                          Camera server will be stopped\n");
                EStopCameraServer();
                break;
            case CAMERA3_MSG_ERROR_REQUEST:
                M_ERROR("Received \"Request\" error from camera: %s\n",
                             pPerCameraMgr->name);
                break;
            case CAMERA3_MSG_ERROR_RESULT:
                M_ERROR("Received \"Result\" error from camera: %s\n",
                             pPerCameraMgr->name);
                break;
            case CAMERA3_MSG_ERROR_BUFFER:
                M_ERROR("Received \"Buffer\" error from camera: %s\n",
                             pPerCameraMgr->name);
                break;

            default:
                M_ERROR("Camera: %s Framenumber: %d ErrorCode: %d\n", pPerCameraMgr->name,
                       msg->message.error.frame_number, msg->message.error.error_code);
        }
    }
}



// this is used for preview, small, and large video streams
void PerCameraMgr::publishISPFrame(BufferBlock const& b, camera_image_metadata_t& meta, int32_t fmt,
                      int grey_pipe, int color_pipe)
{
    uint8_t const* first_plane_ptr;
    size_t const first_plane_size = b.width * b.height;
    size_t const uv_plane_size    = first_plane_size / 2;

    bool pub_grey_frame = pipe_server_get_num_clients(grey_pipe)  > 0;
    bool pub_yuv_frame  = pipe_server_get_num_clients(color_pipe) > 0;

    // exit right away if we don't need to publish anything
    if (!pub_grey_frame && !pub_yuv_frame){
        return;
    }

    // on apq, hal3 lies about stride -- don't actually need to remove
    bool needs_remove_plane_stride = false;
#ifndef APQ8096
    // is10bit flag only set when using monochrome raw mode and driver happens
    // to give 10-bit, in which case we already converted to 8-bit flat before here
    if((b.width != b.stride)  && !is10bit){
        //M_PRINT("removing plane stride for %s.. width = %d, stride = %d, is10bit = %d\n",configInfo.name, b.width, b.stride, is10bit);
        needs_remove_plane_stride = true;
    }
#endif

    if(needs_remove_plane_stride){
        yuvOutputBuffer.resize(first_plane_size+uv_plane_size);      //resize to hold the whole YUV frame
        //first_plane_ptr = new uint8_t[first_plane_size];
        first_plane_ptr = &(yuvOutputBuffer[0]);
        removePlaneStride(b.stride, b.width, b.height,
                          static_cast<uint8_t const*>(b.vaddress),
                          const_cast<uint8_t*>(first_plane_ptr));
    }
    else{
        first_plane_ptr = static_cast<uint8_t const*>(b.vaddress);
    }

    camera_image_metadata_t out_meta = meta;

    out_meta.magic_number = CAMERA_MAGIC_NUMBER;
    out_meta.width        = static_cast<int16_t>(b.width);
    out_meta.stride       = static_cast<int32_t>(b.width);
    out_meta.height       = static_cast<int16_t>(b.height);
    out_meta.format       = IMAGE_FORMAT_RAW8;
    out_meta.size_bytes   = meta.width * meta.height;
    
    if (pub_grey_frame){
        pipe_server_write_camera_frame(grey_pipe, out_meta, first_plane_ptr);
    }

    // also send to color pipe if color camera
    if(pub_yuv_frame && fmt == HAL3_FMT_YUV){
        uint8_t const* uv_plane_ptr;
        if(needs_remove_plane_stride){
            //uv_plane_ptr = new uint8_t[uv_plane_size];
            uv_plane_ptr = first_plane_ptr + first_plane_size;
            removePlaneStride(b.stride, b.width, b.height / 2,
                              static_cast<uint8_t const*>(b.uvHead),
                              const_cast<uint8_t*>(uv_plane_ptr));
        }
        else{
            uv_plane_ptr = static_cast<uint8_t const*>(b.uvHead);
        }

        out_meta.format        = IMAGE_FORMAT_NV12;
        out_meta.size_bytes    = static_cast<int32_t>(first_plane_size + uv_plane_size);
        const void* buffers[3] = { &out_meta, first_plane_ptr, uv_plane_ptr };
        size_t lengths[3]      = { sizeof(camera_image_metadata_t), first_plane_size, uv_plane_size };
        pipe_server_write_list(color_pipe, 3, buffers, lengths);

        //if(needs_remove_plane_stride){
        //    delete[] uv_plane_ptr;
        //}
    }

    //if(needs_remove_plane_stride){
    //    delete[] first_plane_ptr;
    //}
}


void PerCameraMgr::ProcessTOFPreviewFrame(BufferBlock* bufferBlockInfo, camera_image_metadata_t meta)
{
    tofFrameCounter++;

    if(grab_cpu_standby_active() && tofFrameCounter % (int)configInfo.decimator != 0){
        return;
    }
    #ifdef APQ8096
        TOFProcessRAW16(tof_interface,
                    (uint16_t *)bufferBlockInfo->vaddress,
                    meta.timestamp_ns);
    #elif QRB5165
        auto noStridePlaneSize = static_cast<size_t>(pre_width*pre_height*1.5);
        auto realWidth = static_cast<uint32_t>(pre_width*1.5);
        uint8_t* noStridePlane;
        if (bufferBlockInfo->stride != realWidth) {
            noStridePlane = new uint8_t[noStridePlaneSize];
            removePlaneStride(bufferBlockInfo->stride, realWidth, bufferBlockInfo->height,
                              (uint8_t*) bufferBlockInfo->vaddress, noStridePlane);
        }
        else {
            noStridePlane = static_cast<uint8_t*>(bufferBlockInfo->vaddress);
        }

        uint16_t srcPixel16[pre_width * pre_height] = {0};
        // NOTE we don't actually puvblish tis particular metadata to the pipe
        // TOF data is published separately in a very different way to cameras
        meta.format     = IMAGE_FORMAT_RAW8;
        meta.size_bytes = pre_width * pre_height;
        meta.stride     = pre_width;

        Mipi12ToRaw16(meta.size_bytes, noStridePlane, srcPixel16);
        tof_interface->ProcessRAW16(srcPixel16, meta.timestamp_ns);

        if (bufferBlockInfo->stride != realWidth) {
            delete[] noStridePlane;
        }
    #endif
    M_VERBOSE("Sent tof data to royale for processing\n");

    return;
}

void PerCameraMgr::publishRAWPreviewFrame(BufferBlock* bufferBlockInfo, camera_image_metadata_t meta)
{
    M_VERBOSE("publishing raw preview frame for mono camera %s\n", name);

    meta.format     = IMAGE_FORMAT_RAW8;
    meta.size_bytes = pre_width*pre_height;
    meta.stride     = pre_width;

    // TODO check if we need to remove plane stride here

    // if the bayer pipe is set up, we must be a color raw sensor, write the raw bayer data
    /*
    if(previewPipeBayer >= 0){
        pipe_server_write_camera_frame(previewPipeBayer, meta, (uint8_t*)bufferBlockInfo->vaddress);
    }
    */

    // all pipes have a grey channel, publish that.
    pipe_server_write_camera_frame(previewPipeGrey, meta, previewFrameGrey8);


    // only debayer to color if there are clients
    if(previewPipeColor>=0 && pipe_server_get_num_clients(previewPipeColor)>0)
    {
        if( (configInfo.bayer_fmt == BAYER_BGGR && configInfo.en_rotate==0) ||\
            (configInfo.bayer_fmt == BAYER_RGGB && configInfo.en_rotate!=0))
        {
            debayer_BGGR8_to_rgb8((uint8_t*)bufferBlockInfo->vaddress, previewFrameRGB8, pre_width, pre_height);
        }else{
            debayer_RGGB8_to_rgb8((uint8_t*)bufferBlockInfo->vaddress, previewFrameRGB8, pre_width, pre_height);
        }

        meta.format = IMAGE_FORMAT_RGB;
        meta.size_bytes = pre_width*pre_height*3;
        pipe_server_write_camera_frame(previewPipeColor, meta, previewFrameRGB8);
    }

    return;
}



void PerCameraMgr::publishStereoRAWPreviewFrame(camera_image_metadata_t meta)
{
    M_VERBOSE("publishing raw preview frame for stereo camera %s\n", name);

    // TODO check if we need to remove plane stride here

    // raw cameras only publish a grey debayered image pair for DFS or VIO
    size_t ylen = pre_width*pre_height;
    const void* bufs[] = {&meta, previewFrameGrey8, childFrame};
    size_t lens[] = {sizeof(camera_image_metadata_t), ylen, ylen};
    meta.format = IMAGE_FORMAT_STEREO_RAW8;
    meta.size_bytes = ylen*2;
    meta.stride     = pre_width;
    pipe_server_write_list(previewPipeGrey, 3, bufs, lens);

    return;
}


// this is used for preview, small, and large video streams
void PerCameraMgr::publishStereoISPFrame(BufferBlock* bufferBlockInfo, camera_image_metadata_t meta)
{
    // these are lengths to be written to a pipe which require stride=width
    size_t ylen =  bufferBlockInfo->width * bufferBlockInfo->height;
    size_t uvlen = ylen/2;

    // write the Y data out to grey pipe for both
    const void* bufs[] = {&meta, bufferBlockInfo->vaddress, childFrame};
    size_t lens[] = {sizeof(camera_image_metadata_t), ylen, ylen};
    meta.format = IMAGE_FORMAT_STEREO_RAW8;
    meta.size_bytes = 2*ylen;
    pipe_server_write_list(previewPipeGrey, 3, bufs, lens);

    // for color cameras also write UV
    if(previewPipeColor>=0 && pipe_server_get_num_clients(previewPipeColor)>0)
    {
        const void* bufs[] = {  &meta,
                                bufferBlockInfo->vaddress,
                                bufferBlockInfo->uvHead,
                                childFrame,
                                childFrame_uvHead};
        size_t lens[] = {sizeof(camera_image_metadata_t), ylen, uvlen, ylen, uvlen};
        meta.format = IMAGE_FORMAT_STEREO_NV12;
        meta.size_bytes = 2*(ylen+uvlen);
        pipe_server_write_list(previewPipeColor, 5, bufs, lens);
    }

    return;
}


void PerCameraMgr::runModalExposure(uint8_t* grey_ptr, camera_image_metadata_t meta)
{
    if(ae_mode != AE_LME_HIST && \
        ae_mode != AE_LME_MSV){
        return;
    }

    int64_t    new_exposure_ns;
    int32_t    new_gain;

    pthread_mutex_lock(&aeMutex);

    if(ae_mode == AE_LME_HIST && expHistInterface.update_exposure(
            grey_ptr,
            meta.width,
            meta.height,
            meta.exposure_ns,
            meta.gain,
            &new_exposure_ns,
            &new_gain))
    {
        setExposure = new_exposure_ns;
        setGain     = new_gain;
    }

    if(ae_mode == AE_LME_MSV && expMSVInterface.update_exposure(
            grey_ptr,
            meta.width,
            meta.stride,
            meta.height,
            meta.exposure_ns,
            meta.gain,
            &new_exposure_ns,
            &new_gain))
    {
        setExposure = new_exposure_ns;
        setGain     = new_gain;
    }
    pthread_mutex_unlock(&aeMutex);

    return;
}


// return -1 to indicate that ProcessPreviewFrame should NOT continue
int PerCameraMgr::handleStereoSync(camera_image_metadata_t* meta)
{
    NEED_CHILD:
    pthread_mutex_lock(&stereoMutex);
    if(childFrame == NULL){
        pthread_cond_wait(&stereoCond, &stereoMutex);
    }

    if(EStopped | stopped) {
        pthread_cond_signal(&(otherMgr->stereoCond));
        pthread_mutex_unlock(&stereoMutex);
        return -1;
    }

    if(childFrame == NULL){
        pthread_mutex_unlock(&stereoMutex);
        M_WARN("Child frame not received, assuming missing and discarding master\n");
        return -1;
    }

    int64_t diff = meta->timestamp_ns - childInfo.timestamp_ns;
    M_DEBUG("----- STEREO TIMING: %s timestamps(ms): %llu, %llu, diff: %5.1fms\n",
        name,
        meta->timestamp_ns/1000000,
        childInfo.timestamp_ns/1000000,
        diff/1000000.0);

    //Much newer master, discard the child and get a new one
    if(diff > MAX_STEREO_DISCREPENCY_NS){
        M_WARN("Camera %s Received much newer master than child (%0.1fms), discarding child and trying again\n",
            name, diff/1000000.0);
        childFrame = NULL;
        pthread_mutex_unlock(&stereoMutex);
        pthread_cond_signal(&(otherMgr->stereoCond));
        goto NEED_CHILD;
    }

    diff *= -1;
    //Much newer child, discard master but keep the child
    if(diff > MAX_STEREO_DISCREPENCY_NS){
        M_WARN("Camera %s Received much newer child than master (%0.1fms), discarding master and trying again\n",
            name, diff/1000000.0);
        pthread_mutex_unlock(&stereoMutex);
        return -1;
    }

    // Assume the earlier timestamp is correct
    if(meta->timestamp_ns > childInfo.timestamp_ns){
        meta->timestamp_ns = childInfo.timestamp_ns;
    }


    return 0;
}

void PerCameraMgr::ProcessIMU(camera_image_metadata_t & meta)
{
    if (!imu_manager){
        M_ERROR("Attempting to process IMU, but IMU manager is NULL\n");
        return;
    }
    int64_t readout_time_ns = 14000000; //14ms, approx IMX412 4K, need to verify

    // meta.timestamp_ns reports start of exposure
    int64_t center_of_exposure_ns =  meta.timestamp_ns + (meta.exposure_ns + readout_time_ns)/2 -10000000;

    imu_manager->update(&imu_context, center_of_exposure_ns);

    //R_since_start contains the latest IMU rotation from start

    //K is the camera calibration matrix
    double fx = 800;
    double fy = fx;
    double cx = pre_width  / 2;
    double cy = pre_height / 2;

    rc_matrix_t K    = RC_MATRIX_INITIALIZER;
    rc_matrix_t Kinv = RC_MATRIX_INITIALIZER;
    rc_matrix_alloc(&K, 3, 3);
    rc_matrix_alloc(&Kinv, 3, 3);
    K.d[0][0] = fx;
    K.d[0][1] = 0;
    K.d[0][2] = cx;
    K.d[1][0] = 0;
    K.d[1][1] = fy;
    K.d[1][2] = cy;
    K.d[2][0] = 0;
    K.d[2][1] = 0;
    K.d[2][2] = 1;

    //inverse of upper triangular 3x3 matrix has a closed form solution
    Kinv.d[0][0] = 1.0 / K.d[0][0];
    Kinv.d[0][1] = -K.d[0][1] / (K.d[0][0]*K.d[1][1]);
    Kinv.d[0][2] = (K.d[0][1]*K.d[1][2] - K.d[0][2]*K.d[1][1])/(K.d[0][0]*K.d[1][1]*K.d[2][2]);
    Kinv.d[1][0] = 0.0;
    Kinv.d[1][1] = 1.0 / K.d[1][1];
    Kinv.d[1][2] = -K.d[1][2]/(K.d[1][1]*K.d[2][2]);
    Kinv.d[2][0] = 0.0;
    Kinv.d[2][1] = 0.0;
    Kinv.d[2][2] = 1.0 / K.d[2][2];

    rc_matrix_t iRc = RC_MATRIX_INITIALIZER;
    rc_matrix_t cRw = RC_MATRIX_INITIALIZER;
    rc_matrix_alloc(&iRc, 3, 3);
    rc_matrix_alloc(&cRw, 3, 3);

    iRc.d[0][0] = 0.0; iRc.d[0][1] = 0.0; iRc.d[0][2] = 1.0;
    iRc.d[1][0] = 1.0; iRc.d[1][1] = 0.0; iRc.d[1][2] = 0.0; 
    iRc.d[2][0] = 0.0; iRc.d[2][1] = 1.0; iRc.d[2][2] = 0.0;

    rc_matrix_transpose(iRc,&cRw);

    rc_matrix_t wRi = RC_MATRIX_INITIALIZER;
    rc_matrix_transpose(imu_context.R_since_start,&wRi);

    //printf("\n");
    //rc_matrix_print(K);
    //rc_matrix_print(Kinv);

    //cRi : rotation from imu frame to camera frame
    //iRc : rotation from camera to imu frame (cRi')
    //wRi : rotation of IMU to world frame (attitude estimate)
    //vcRw: rotation of virtual camera in the world frame  

    // H  = Kvc * vcRw * wRi * iRc * Kinv
    // V  = (px, py, 1)
    // V* = H * V = (x*, y*, z*)
    // px* = x*/z*
    // py* = y*/z*

    //px,py are pixel coordinates from input image
    //px*, py* are the rotated coordinates of the original point in the original frame

    //(px**, py**) = distort(px*, py*)
    //px**, py** are the distorted coordinates so the pixel value can be looked up in the original image

    //simple test
    //H  = K * wRi * Kinv
    //H  = Kvc * vcRw * wRi * iRc * Kinv

    rc_matrix_t H  = RC_MATRIX_INITIALIZER;
    rc_matrix_alloc(&H, 3, 3);

    //rc_matrix_multiply(K,imu_context.R_since_start,&H);
    //rc_matrix_right_multiply_inplace(&H,Kinv);

    rc_matrix_multiply(K,cRw,&H);
    rc_matrix_right_multiply_inplace(&H,wRi);
    rc_matrix_right_multiply_inplace(&H,iRc);
    rc_matrix_right_multiply_inplace(&H,Kinv);

    //save the H matrix as a regular array that will be passed to GPU
    imu_rotation_vec[0] = H.d[0][0]; imu_rotation_vec[1] = H.d[0][1]; imu_rotation_vec[2] = H.d[0][2];
    imu_rotation_vec[3] = H.d[1][0]; imu_rotation_vec[4] = H.d[1][1]; imu_rotation_vec[5] = H.d[1][2];
    imu_rotation_vec[6] = H.d[2][0]; imu_rotation_vec[7] = H.d[2][1]; imu_rotation_vec[8] = H.d[2][2];

    //free temporary matrices (maybe should just keep them allocated and re-use)
    rc_matrix_free(&K);
    rc_matrix_free(&Kinv);
    rc_matrix_free(&H);
    rc_matrix_free(&iRc);
    rc_matrix_free(&cRw);

}


void PerCameraMgr::ProcessAR0144Frame(BufferBlock* bufferBlockInfo, camera_image_metadata_t meta)
{
    bool pub_preview       = (previewPipeGrey)  >= 0 && pipe_server_get_num_clients(previewPipeGrey)  > 0;
    bool pub_bayer         = (previewPipeBayer) >= 0 && pipe_server_get_num_clients(previewPipeBayer) > 0;
    bool pub_grey_frame    = (mispPipeGrey)     >= 0 && pipe_server_get_num_clients(mispPipeGrey)     > 0;
    bool pub_encoded_frame = (mispPipeEncoded)  >= 0 && pipe_server_get_num_clients(mispPipeEncoded)  > 0;
    bool pub_norm_frame    = (mispPipeNorm)     >= 0 && pipe_server_get_num_clients(mispPipeNorm)     > 0;

    BufferBlock* b_in = bufferBlockInfo;

    //find the filled size
    if (raw_src_bpp == 0){
        int zeros_start = b_in->size;
        uint8_t * p     = (uint8_t*)b_in->vaddress;
        for (unsigned int i=0; i<b_in->size; i++){
            zeros_start--;
            if (p[zeros_start] != 0){
                break;
            }
        }
        int frame_size = zeros_start + 1;

        switch(frame_size){
            case 1024000:
                raw_src_bpp          = 8;
                raw_src_line_stride  = 1280;  //bytes
                raw_src_plane_stride = 800;   //lines
                raw_src_size         = frame_size;
                raw_src_format       = IMAGE_FORMAT_RAW8;
                break;
            case 1280000:
                raw_src_bpp          = 10;
                raw_src_line_stride  = 1600;  //bytes
                raw_src_plane_stride = 800;   //lines
                raw_src_size         = frame_size;
                raw_src_format       = IMAGE_FORMAT_RAW10;
                break;
            case 1536000:
                raw_src_bpp          = 12;
                raw_src_line_stride  = 1920;  //bytes
                raw_src_plane_stride = 800;   //lines
                raw_src_size         = frame_size;
                raw_src_format       = IMAGE_FORMAT_RAW12;
                break;
            default:
                M_ERROR("MISP: Unexpected frame size for camera %s, width %d, height %d, stride %d, slice %d, alloc %d bytes, calc frame size %d\n",
                    name,b_in->width, b_in->height, b_in->stride, b_in->slice, b_in->size, frame_size);
                return;
        }
        M_PRINT("camera %s frame size: %d (%d alloc).. raw bpp: %d\n",name, frame_size, b_in->size,raw_src_bpp);
    }

    //get buffer for output output image
    buffer_handle_t* dst_h = bufferPop(misp_bufferGroup);
    if (dst_h == NULL){
        M_ERROR("ProcessMispFrame: could not get a buffer\n");
        return;
    }
    BufferBlock* dstBufferBlockInfo = bufferGetBufferInfo(&misp_bufferGroup, dst_h);
    if (dstBufferBlockInfo == NULL){
        M_ERROR("ProcessMispFrame: could not get dstBufferBlockInfo\n");
        return;
    }

    buffer_handle_t* dst2_h = NULL;
    BufferBlock* normBuffer = NULL;


    bool use_encoder_stride = pub_encoded_frame;

    uint32_t src_rggb   = 0;
    uint32_t dst_stride = dstBufferBlockInfo->width;
    if (use_encoder_stride){
        dst_stride = dstBufferBlockInfo->stride;
    }

    uint8_t * original_raw8_frame = (uint8_t*)bufferBlockInfo->vaddress;

    //AR0144MipiToRaw8LSC accepts RAW8/10/12 image and outputs LSC-corrected and uncorrected 8-bit image
    if (raw_src_bpp == 8){
        //LSC corrected image will be in dstBufferBlockInfo->vaddress, uncorrected 8bit image is just the original frame
        if (pub_grey_frame || pub_encoded_frame){
            misp->AR0144MipiToRaw8LSC(bufferBlockInfo->vaddress,    bufferBlockInfo->fd,    bufferBlockInfo->size,    bufferBlockInfo->width,    bufferBlockInfo->height,    raw_src_line_stride, raw_src_plane_stride, raw_src_bpp,
                                        dstBufferBlockInfo->vaddress, dstBufferBlockInfo->fd, dstBufferBlockInfo->size, dstBufferBlockInfo->width, dstBufferBlockInfo->height, dst_stride,          dstBufferBlockInfo->slice);
        }

        original_raw8_frame = (uint8_t*)bufferBlockInfo->vaddress;
    }
    else if (raw_src_bpp == 10){
        //need to run this for any output pipe client because Auto Exposure control needs 8 bit image

        //LSC corrected image will be in dstBufferBlockInfo->vaddress, uncorrected 8bit image will be in the scratch buffer
        misp->AR0144MipiToRaw8LSC(bufferBlockInfo->vaddress,    bufferBlockInfo->fd,    bufferBlockInfo->size,    bufferBlockInfo->width,    bufferBlockInfo->height,    raw_src_line_stride, raw_src_plane_stride, raw_src_bpp,
                                    dstBufferBlockInfo->vaddress, dstBufferBlockInfo->fd, dstBufferBlockInfo->size, dstBufferBlockInfo->width, dstBufferBlockInfo->height, dst_stride,          dstBufferBlockInfo->slice);
        
        original_raw8_frame = (uint8_t*)misp->GetFullScratchBuffer();
    }
    else if (raw_src_bpp == 12){
        //need to run this for any output pipe client because Auto Exposure control needs 8 bit image

        //LSC corrected image will be in dstBufferBlockInfo->vaddress, uncorrected 8bit image will be in the scratch buffer
        misp->AR0144MipiToRaw8LSC(bufferBlockInfo->vaddress,    bufferBlockInfo->fd,    bufferBlockInfo->size,    bufferBlockInfo->width,    bufferBlockInfo->height,    raw_src_line_stride, raw_src_plane_stride, raw_src_bpp,
                                    dstBufferBlockInfo->vaddress, dstBufferBlockInfo->fd, dstBufferBlockInfo->size, dstBufferBlockInfo->width, dstBufferBlockInfo->height, dst_stride,          dstBufferBlockInfo->slice);
        
        original_raw8_frame = (uint8_t*)misp->GetFullScratchBuffer();
    }

    //if normalized image is requested, run the processing
    if (pub_norm_frame){
        dst2_h = bufferPop(misp_bufferGroup);
        if (dst2_h == NULL){
            M_ERROR("ProcessMispFrame: could not get a buffer\n");
            return;
        }

        normBuffer = bufferGetBufferInfo(&misp_bufferGroup, dst2_h);
        if (normBuffer == NULL){
            M_ERROR("ProcessMispFrame: could not get normBuffer\n");
            return;
        }

        misp->NormalizeImage(bufferBlockInfo->vaddress,    bufferBlockInfo->fd,    bufferBlockInfo->size,    bufferBlockInfo->width,    bufferBlockInfo->height, raw_src_line_stride ,    raw_src_plane_stride, src_rggb, raw_src_bpp,
                    normBuffer->vaddress, normBuffer->fd, normBuffer->size, normBuffer->width, normBuffer->height, normBuffer->width, normBuffer->slice);
    }


    camera_image_metadata_t  misp_meta;
    misp_meta.frame_id     = meta.frame_id;
    misp_meta.timestamp_ns = meta.timestamp_ns;
    misp_meta.exposure_ns  = meta.exposure_ns;
    misp_meta.gain         = meta.gain;
    misp_meta.magic_number = CAMERA_MAGIC_NUMBER;
    misp_meta.width        = dstBufferBlockInfo->width;
    misp_meta.stride       = dstBufferBlockInfo->stride;
    misp_meta.height       = dstBufferBlockInfo->height;
    misp_meta.size_bytes   = bufferBlockInfo->size;
    misp_meta.format       = IMAGE_FORMAT_NV12;

    //push the frame into encoder as soon as possible (before publishing other frames)
    if (configInfo.en_misp_encoder && pub_encoded_frame){
        //set the UV plane to 127 because this is a monochrome image
        uint32_t uv_offset = dstBufferBlockInfo->slice*dstBufferBlockInfo->stride;
        if (((uint8_t*)(dstBufferBlockInfo->vaddress))[uv_offset] != 127){
            memset(dstBufferBlockInfo->vaddress+uv_offset,127,(dstBufferBlockInfo->stride*dstBufferBlockInfo->height)/2);
        }
        
        // check health of the encoder and drop this frame if it's getting backed up
        int n = pVideoEncoderMisp->ItemsInQueue();
        if(n>SMALL_VID_ALLOWED_ITEMS_IN_OMX_QUEUE){
            M_DEBUG("dropping misp frame, OMX is getting backed up, has %d in queue already\n", n);
            bufferPush(misp_bufferGroup, dst_h);
        }
        else {
            // add to the OMX queue
            pVideoEncoderMisp->ProcessFrameToEncode(misp_meta, dstBufferBlockInfo);
        }
    }

    camera_image_metadata_t  grey_meta = misp_meta;
    grey_meta.format       = IMAGE_FORMAT_RAW8;
    grey_meta.stride       = grey_meta.width;
    grey_meta.size_bytes   = grey_meta.width*grey_meta.height;

    if (pub_grey_frame)   //lsc corrected frame
    {
        if (use_encoder_stride){
            uint8_t * misp_temp_buffer = misp->GetTempBuffer(misp_meta.width*misp_meta.height*1.5);
            //copy Y plane, removing stride > width
            uint8_t * in  = (uint8_t*)dstBufferBlockInfo->vaddress;
            uint8_t * out = misp_temp_buffer;
            uint32_t in_stride = dst_stride;
            for (int i=0; i<misp_height; i++){
                memcpy(out,in,misp_width);
                in += in_stride;
                out += misp_width;
            }
            pipe_server_write_camera_frame(mispPipeGrey, grey_meta, misp_temp_buffer);
        } else{
            pipe_server_write_camera_frame(mispPipeGrey, grey_meta, dstBufferBlockInfo->vaddress);
        }
    }

    if (pub_bayer)
    {
        camera_image_metadata_t  bayer_meta = misp_meta;
        bayer_meta.format       = raw_src_format;
        bayer_meta.width        = bufferBlockInfo->width;
        bayer_meta.height       = bufferBlockInfo->height;
        bayer_meta.stride       = raw_src_line_stride;
        bayer_meta.size_bytes   = raw_src_size;
        pipe_server_write_camera_frame(previewPipeBayer, bayer_meta, bufferBlockInfo->vaddress);
    }

    camera_image_metadata_t  raw8_meta = misp_meta;
    raw8_meta.width        = misp_width;
    raw8_meta.stride       = raw8_meta.width;
    raw8_meta.height       = misp_height;
    raw8_meta.size_bytes   = raw8_meta.width*raw8_meta.height;
    raw8_meta.format       = IMAGE_FORMAT_RAW8;

    if (pub_preview)
    {
        pipe_server_write_camera_frame(previewPipeGrey, raw8_meta, original_raw8_frame);
    }

    if (pub_norm_frame){
        pipe_server_write_camera_frame(mispPipeNorm, raw8_meta, normBuffer->vaddress);
    }

    runModalExposure(original_raw8_frame, raw8_meta);
    
    //WARNING: do not release the buffer if encoder is enabled, it will be done by the encoder after encoding is finished
    if (!configInfo.en_misp_encoder || !pub_encoded_frame){
        bufferPush(misp_bufferGroup, dst_h);  //release the buffer if encoder is not enabled
    }

    if (dst2_h){
        bufferPush(misp_bufferGroup, dst2_h);
    }
}

void PerCameraMgr::ProcessIMX412Frame10Bit(BufferBlock* srcBufferBlockInfo, camera_image_metadata_t meta)
{
    //bool pub_preview       = (previewPipeGrey)  >= 0 && pipe_server_get_num_clients(previewPipeGrey)  > 0;
    bool pub_bayer         = (previewPipeBayer) >= 0 && pipe_server_get_num_clients(previewPipeBayer) > 0;
    bool pub_grey_frame    = (mispPipeGrey)     >= 0 && pipe_server_get_num_clients(mispPipeGrey)     > 0;
    bool pub_yuv_frame     = (mispPipeColor)    >= 0 && pipe_server_get_num_clients(mispPipeColor)    > 0;
    bool pub_encoded_frame = (mispPipeEncoded)  >= 0 && pipe_server_get_num_clients(mispPipeEncoded)  > 0;

    raw_src_bpp          = 10;
    raw_src_format       = IMAGE_FORMAT_RAW10;

    //get buffer for output output image
    buffer_handle_t* dst_h = bufferPop(misp_bufferGroup);
    if (dst_h == NULL){
        M_ERROR("ProcessMispFrame: could not get a buffer\n");
        return;
    }
    BufferBlock* dstBufferBlockInfo = bufferGetBufferInfo(&misp_bufferGroup, dst_h);
    if (dstBufferBlockInfo == NULL){
        M_ERROR("ProcessMispFrame: could not get dstBufferBlockInfo\n");
        return;
    }

    misp->UpdateZoomFilter();

    int src_rggb = 1;

    misp->ConvertMipi10ToYuv(srcBufferBlockInfo->vaddress, srcBufferBlockInfo->fd, srcBufferBlockInfo->size, srcBufferBlockInfo->width, srcBufferBlockInfo->height, srcBufferBlockInfo->stride, srcBufferBlockInfo->slice, src_rggb,
                             dstBufferBlockInfo->vaddress, dstBufferBlockInfo->fd, dstBufferBlockInfo->size, dstBufferBlockInfo->width, dstBufferBlockInfo->height, dstBufferBlockInfo->stride, dstBufferBlockInfo->slice);

    if (misp->GetAWBMode() == "auto"){
        misp->ComputeAWB(dstBufferBlockInfo->vaddress, dstBufferBlockInfo->fd, dstBufferBlockInfo->size, dstBufferBlockInfo->width, dstBufferBlockInfo->height, dstBufferBlockInfo->stride, dstBufferBlockInfo->slice);
    }


    camera_image_metadata_t  misp_meta;
    misp_meta.frame_id     = meta.frame_id;
    misp_meta.timestamp_ns = meta.timestamp_ns;
    misp_meta.exposure_ns  = meta.exposure_ns;
    misp_meta.gain         = meta.gain;
    misp_meta.magic_number = CAMERA_MAGIC_NUMBER;
    misp_meta.width        = dstBufferBlockInfo->width;
    misp_meta.stride       = dstBufferBlockInfo->stride;
    misp_meta.height       = dstBufferBlockInfo->height;
    misp_meta.size_bytes   = dstBufferBlockInfo->size;
    misp_meta.format       = IMAGE_FORMAT_NV12;

    //push the frame into encoder as soon as possible (before publishing other frames)
    if (configInfo.en_misp_encoder && pub_encoded_frame){
        //M_PRINT("Running encoder\n");
        // check health of the encoder and drop this frame if it's getting backed up
        int n = pVideoEncoderMisp->ItemsInQueue();
        if(n>LARGE_VID_ALLOWED_ITEMS_IN_OMX_QUEUE){
            M_DEBUG("dropping misp frame, OMX is getting backed up, has %d in queue already\n", n);
            bufferPush(misp_bufferGroup, dst_h);
        }
        else {
            // Write OSD if enabled
            if(configInfo.misp_venc_config.osd) writeOSD((uint8_t*)dstBufferBlockInfo->vaddress, dstBufferBlockInfo->width, dstBufferBlockInfo->stride, dstBufferBlockInfo->height, smallOsdConfig);
            // add to the OMX queue
            pVideoEncoderMisp->ProcessFrameToEncode(misp_meta, dstBufferBlockInfo);
        }
    }    

    camera_image_metadata_t  grey_meta = misp_meta;
    grey_meta.format       = IMAGE_FORMAT_RAW8;
    grey_meta.stride       = grey_meta.width;
    grey_meta.size_bytes   = grey_meta.width*grey_meta.height;

    
    //check for number of subscribers, so we don't have to copy data for no reason
    if (pub_grey_frame || pub_yuv_frame)
    {
        uint8_t * misp_temp_buffer = (uint8_t*)misp->GetTempBuffer(misp_meta.width*misp_meta.height*1.5);
        //copy Y plane, removing stride
        uint8_t * in  = (uint8_t*)dstBufferBlockInfo->vaddress;
        uint8_t * out = misp_temp_buffer;
        uint32_t in_stride = dstBufferBlockInfo->stride;
        for (int i=0; i<misp_height; i++){
            memcpy(out,in,misp_width);
            in += in_stride;
            out += misp_width;
        }

        if (pub_grey_frame){
            pipe_server_write_camera_frame(mispPipeGrey, grey_meta, misp_temp_buffer);
            //pipe_server_write_camera_frame(mispPipeGrey, grey_meta, dstBufferBlockInfo->vaddress);
        }

        if (pub_yuv_frame){
            //copy the UV plane, removing stride
            uint8_t * in  = (uint8_t*)(dstBufferBlockInfo->vaddress) + dstBufferBlockInfo->stride * dstBufferBlockInfo->slice;
            uint8_t * out = misp_temp_buffer + misp_width*misp_height;
            uint32_t in_stride = dstBufferBlockInfo->stride;
            for (int i=0; i<misp_height/2; i++){
                memcpy(out,in,misp_width);
                in += in_stride;
                out += misp_width;
            }

            //publish YUV color frame
            grey_meta.format       = IMAGE_FORMAT_NV12;
            grey_meta.stride       = grey_meta.width;
            grey_meta.size_bytes   = grey_meta.width*grey_meta.height*1.5;
            pipe_server_write_camera_frame(mispPipeColor, grey_meta, misp_temp_buffer);
        }
    }

    //publish raw bayer frame
    if (pub_bayer){
        camera_image_metadata_t  bayer_meta = misp_meta;
        bayer_meta.format       = IMAGE_FORMAT_RAW10;
        bayer_meta.width        = srcBufferBlockInfo->width;
        bayer_meta.height       = srcBufferBlockInfo->height;
        bayer_meta.stride       = srcBufferBlockInfo->stride;
        bayer_meta.size_bytes   = srcBufferBlockInfo->size;
        pipe_server_write_camera_frame(previewPipeBayer, bayer_meta, srcBufferBlockInfo->vaddress);
    }
    

    runModalExposure((uint8_t*)dstBufferBlockInfo->vaddress, misp_meta);

    //WARNING: do not release the buffer if encoder is enabled, it will be done by the encoder after encoding is finished
    if (!configInfo.en_misp_encoder || !pub_encoded_frame){
        bufferPush(misp_bufferGroup, dst_h);  //release the buffer if encoder is not enabled
    }
}


void PerCameraMgr::ProcessOV9782Frame10Bit(BufferBlock* srcBufferBlockInfo, camera_image_metadata_t meta)
{
    //bool pub_preview       = (previewPipeGrey)  >= 0 && pipe_server_get_num_clients(previewPipeGrey)  > 0;
    bool pub_bayer         = (previewPipeBayer) >= 0 && pipe_server_get_num_clients(previewPipeBayer) > 0;
    bool pub_grey_frame    = (mispPipeGrey)     >= 0 && pipe_server_get_num_clients(mispPipeGrey)     > 0;
    bool pub_yuv_frame     = (mispPipeColor)    >= 0 && pipe_server_get_num_clients(mispPipeColor)    > 0;
    bool pub_encoded_frame = (mispPipeEncoded)  >= 0 && pipe_server_get_num_clients(mispPipeEncoded)  > 0;
    bool pub_norm_frame    = (mispPipeNorm)     >= 0 && pipe_server_get_num_clients(mispPipeNorm)     > 0;

    raw_src_bpp          = 10;
    raw_src_line_stride  = 1600;  //bytes
    raw_src_plane_stride = 800;   //lines
    raw_src_format       = IMAGE_FORMAT_RAW10;

    //get buffer for output output image
    buffer_handle_t* dst_h = bufferPop(misp_bufferGroup);
    if (dst_h == NULL){
        M_ERROR("ProcessMispFrame: could not get a buffer\n");
        return;
    }
    BufferBlock* dstBufferBlockInfo = bufferGetBufferInfo(&misp_bufferGroup, dst_h);
    if (dstBufferBlockInfo == NULL){
        M_ERROR("ProcessMispFrame: could not get dstBufferBlockInfo\n");
        return;
    }

    int src_rggb = 0;

    buffer_handle_t* dst2_h = NULL;
    BufferBlock* normBuffer = NULL;

    misp->ConvertMipi10ToYuv(srcBufferBlockInfo->vaddress, srcBufferBlockInfo->fd, srcBufferBlockInfo->size, srcBufferBlockInfo->width, srcBufferBlockInfo->height, srcBufferBlockInfo->stride, srcBufferBlockInfo->slice, src_rggb,
                             dstBufferBlockInfo->vaddress, dstBufferBlockInfo->fd, dstBufferBlockInfo->size, dstBufferBlockInfo->width, dstBufferBlockInfo->height, dstBufferBlockInfo->stride, dstBufferBlockInfo->slice);

    if (misp->GetAWBMode() == "auto"){
        misp->ComputeAWB(dstBufferBlockInfo->vaddress, dstBufferBlockInfo->fd, dstBufferBlockInfo->size, dstBufferBlockInfo->width, dstBufferBlockInfo->height, dstBufferBlockInfo->stride, dstBufferBlockInfo->slice);
    }

    //if normalized image is requested, run the processing
    if (pub_norm_frame){
        dst2_h = bufferPop(misp_bufferGroup);
        if (dst2_h == NULL){
            M_ERROR("ProcessMispFrame: could not get a buffer\n");
            return;
        }

        normBuffer = bufferGetBufferInfo(&misp_bufferGroup, dst2_h);
        if (normBuffer == NULL){
            M_ERROR("ProcessMispFrame: could not get normBuffer\n");
            return;
        }

        misp->NormalizeImage(srcBufferBlockInfo->vaddress,    srcBufferBlockInfo->fd,    srcBufferBlockInfo->size,    srcBufferBlockInfo->width,    srcBufferBlockInfo->height, raw_src_line_stride, raw_src_plane_stride, src_rggb, raw_src_bpp,
                    normBuffer->vaddress, normBuffer->fd, normBuffer->size, normBuffer->width, normBuffer->height, normBuffer->width, normBuffer->slice);
    }


    camera_image_metadata_t  misp_meta;
    misp_meta.frame_id     = meta.frame_id;
    misp_meta.timestamp_ns = meta.timestamp_ns;
    misp_meta.exposure_ns  = meta.exposure_ns;
    misp_meta.gain         = meta.gain;
    misp_meta.magic_number = CAMERA_MAGIC_NUMBER;
    misp_meta.width        = dstBufferBlockInfo->width;
    misp_meta.stride       = dstBufferBlockInfo->stride;
    misp_meta.height       = dstBufferBlockInfo->height;
    misp_meta.size_bytes   = dstBufferBlockInfo->size;
    misp_meta.format       = IMAGE_FORMAT_NV12;

    //push the frame into encoder as soon as possible (before publishing other frames)
    if (configInfo.en_misp_encoder && pub_encoded_frame){
        //M_PRINT("Running encoder\n");
        // check health of the encoder and drop this frame if it's getting backed up
        int n = pVideoEncoderMisp->ItemsInQueue();
        if(n>LARGE_VID_ALLOWED_ITEMS_IN_OMX_QUEUE){
            M_DEBUG("dropping misp frame, OMX is getting backed up, has %d in queue already\n", n);
            bufferPush(misp_bufferGroup, dst_h);
        }
        else {
            // add to the OMX queue
            pVideoEncoderMisp->ProcessFrameToEncode(misp_meta, dstBufferBlockInfo);
        }
    }    

    camera_image_metadata_t  grey_meta = misp_meta;
    grey_meta.format       = IMAGE_FORMAT_RAW8;
    grey_meta.stride       = grey_meta.width;
    grey_meta.size_bytes   = grey_meta.width*grey_meta.height;

    
    //check for number of subscribers, so we don't have to copy data for no reason
    if (pub_grey_frame || pub_yuv_frame)
    {
        uint8_t * misp_temp_buffer = (uint8_t*)misp->GetTempBuffer(misp_meta.width*misp_meta.height*1.5);
        //copy Y plane, removing stride
        uint8_t * in  = (uint8_t*)dstBufferBlockInfo->vaddress;
        uint8_t * out = misp_temp_buffer;
        uint32_t in_stride = dstBufferBlockInfo->stride;
        for (int i=0; i<misp_height; i++){
            memcpy(out,in,misp_width);
            in += in_stride;
            out += misp_width;
        }

        if (pub_grey_frame){
            pipe_server_write_camera_frame(mispPipeGrey, grey_meta, misp_temp_buffer);
            //pipe_server_write_camera_frame(mispPipeGrey, grey_meta, dstBufferBlockInfo->vaddress);
        }

        if (pub_yuv_frame){
            //copy the UV plane, removing stride
            uint8_t * in  = (uint8_t*)(dstBufferBlockInfo->vaddress) + dstBufferBlockInfo->stride * dstBufferBlockInfo->slice;
            uint8_t * out = misp_temp_buffer + misp_width*misp_height;
            uint32_t in_stride = dstBufferBlockInfo->stride;
            for (int i=0; i<misp_height/2; i++){
                memcpy(out,in,misp_width);
                in += in_stride;
                out += misp_width;
            }

            //publish YUV color frame
            grey_meta.format       = IMAGE_FORMAT_NV12;
            grey_meta.stride       = grey_meta.width;
            grey_meta.size_bytes   = grey_meta.width*grey_meta.height*1.5;
            pipe_server_write_camera_frame(mispPipeColor, grey_meta, misp_temp_buffer);
        }
    }

    //publish raw bayer frame
    if (pub_bayer){
        camera_image_metadata_t  bayer_meta = misp_meta;
        bayer_meta.format       = IMAGE_FORMAT_RAW10;
        bayer_meta.width        = srcBufferBlockInfo->width;
        bayer_meta.height       = srcBufferBlockInfo->height;
        bayer_meta.stride       = srcBufferBlockInfo->stride;
        bayer_meta.size_bytes   = srcBufferBlockInfo->size;
        pipe_server_write_camera_frame(previewPipeBayer, bayer_meta, srcBufferBlockInfo->vaddress);
    }
    
    if (pub_norm_frame){
        camera_image_metadata_t  raw8_meta = misp_meta;
        raw8_meta.width        = misp_width;
        raw8_meta.stride       = raw8_meta.width;
        raw8_meta.height       = misp_height;
        raw8_meta.size_bytes   = raw8_meta.width*raw8_meta.height;
        raw8_meta.format       = IMAGE_FORMAT_RAW8;
        pipe_server_write_camera_frame(mispPipeNorm, raw8_meta, normBuffer->vaddress);
    }

    //run the auto exposure algo on the Y channel of the color YUV
    runModalExposure((uint8_t*)dstBufferBlockInfo->vaddress, misp_meta);

    //WARNING: do not release the buffer if encoder is enabled, it will be done by the encoder after encoding is finished
    if (!configInfo.en_misp_encoder || !pub_encoded_frame){
        bufferPush(misp_bufferGroup, dst_h);  //release the buffer if encoder is not enabled
    }

    if (dst2_h){
        bufferPush(misp_bufferGroup, dst2_h);
    }
}

void PerCameraMgr::ProcessMispFrame(BufferBlock* bufferBlockInfo, camera_image_metadata_t meta)
{
    // ProcessAR0144Frame handles 8 10 or 12 bit raw images
    if (configInfo.type == SENSOR_AR0144_12BIT || configInfo.type == SENSOR_AR0144){
        ProcessAR0144Frame(bufferBlockInfo, meta);
        return;
    } else if (configInfo.type == SENSOR_IMX412 || configInfo.type == SENSOR_IMX412FPVMISP){
        ProcessIMX412Frame10Bit(bufferBlockInfo, meta);
        return;
    } else if (configInfo.type == SENSOR_OV9782){
        ProcessOV9782Frame10Bit(bufferBlockInfo, meta);
        return;
    } else if (configInfo.type == SENSOR_IMX664 || configInfo.type == SENSOR_IMX664FPV){
        ProcessIMX412Frame10Bit(bufferBlockInfo, meta);
        return;
    } 


    //M_PRINT("misp input frame: width=%d, height=%d, stride=%d, size=%d\n", 
    //    bufferBlockInfo->width, bufferBlockInfo->height, bufferBlockInfo->stride, bufferBlockInfo->size);

    M_ERROR("unsupported misp camera\n");
    return;
}

void PerCameraMgr::ProcessPreviewFrame(image_result result)
{
    if(!has_set_preview_thread_affinity){
        M_DEBUG("setting thread affinity for cam %s preview\n", name);
        lock_this_thread_to_big_cores();
        has_set_preview_thread_affinity = 1;
    }


    BufferBlock* bufferBlockInfo = bufferGetBufferInfo(&pre_bufferGroup, result.second.buffer);

    // fetch metadata with timestamp that should have come from hal3 via callback already
    camera_image_metadata_t meta;
    if(getMeta(result.first, &meta)) {
        M_WARN("Trying to process encode buffer without metadata\n");
        return;
    }
    initMetaFromBlock(*bufferBlockInfo, meta);

    // Tof is different from the rest, pass the data off to spectre then send it out
    if(is_tof_sensor(configInfo.type)) {
        ProcessTOFPreviewFrame(bufferBlockInfo, meta);
        return;
    }

    // okay, at this point we know the camera is not tof, might be raw mono,
    // raw bayer, or ISP YUV image

    if (!en_misp){
        // TODO more robustly check 8 vs 10 vs 12
        // if raw, might need to convert from 8 to 10 bit regardless if we are
        // a mono or a stereo camera, so do that here
        if(configInfo.en_raw_preview && meta.frame_id == 1){
            M_DEBUG("%s checking to see if it is raw8 or raw 10\n", name);

            is10bit = Check10bit((uint8_t*)bufferBlockInfo->vaddress, pre_width, pre_height);
            if(is10bit){
                M_PRINT("Received RAW10 frame from camera %s, will be converting to RAW8 on cpu\n", name);
            } else {
                M_DEBUG("Frame was actually 8 bit, sending as is\n");
            }
        }

        // always debayer to mono, this is the most common use case and also
        // needed for autoexposure. TODO this might not be necessary for
        // hires color cams in raw mode. Need to do autoexposure on a color
        // image in that case though. Later need to see if we need to debayer
        // those cameras to YUV or if RGB is okay
        if(configInfo.en_raw_preview){
            if(is10bit){
                ConvertTo8bitRaw((uint8_t*)bufferBlockInfo->vaddress, pre_width, pre_height,bufferBlockInfo->stride);
                // for AR0144 12-bit testing. TODO swap is10bit logic to use raw_src_bpp
                //Convert12To8bitRaw((uint8_t*)bufferBlockInfo->vaddress, pre_width, pre_height,bufferBlockInfo->stride);
            }
            if(configInfo.bayer_fmt != BAYER_MONO){
                debayer_BGGR8_or_RGGB8_to_mono((uint8_t*)bufferBlockInfo->vaddress, previewFrameGrey8, pre_width, pre_height);
            }
            else{
                previewFrameGrey8 = (uint8_t*)bufferBlockInfo->vaddress;
            }
        }
        else{
            // for most frames, the grey data is just the start of the image buffer
            // when it's not, this will be changed later
            previewFrameGrey8 = (uint8_t*)bufferBlockInfo->vaddress;
        }
    }

    // mono camera is most simple, publish to pipe based on raw or ISP and run AE
    if(partnerMode == MODE_MONO){
        if(configInfo.en_raw_preview){
            if (en_misp){
                ProcessMispFrame(bufferBlockInfo, meta);
            }
            else {
                publishRAWPreviewFrame(bufferBlockInfo, meta);
            }
        }else{
            publishISPFrame(*bufferBlockInfo, meta, pre_halfmt, previewPipeGrey, previewPipeColor);
        }
        if (!en_misp){
            runModalExposure(previewFrameGrey8, meta);
        }
    }


    // stereo slave is fairly simple too, debayer if necessary then indicate
    // to the master that we have a new frame
    else if (partnerMode == MODE_STEREO_SLAVE)
    {
        // lock mutex before modifying the master
        pthread_mutex_lock(&(otherMgr->stereoMutex));

        // point the master to our new image
        otherMgr->childFrame = previewFrameGrey8;
        otherMgr->childInfo  = meta;

        if(configInfo.en_raw_preview){
            otherMgr->childFrame_uvHead = NULL; // no yuv mode for raw images
        }else{
            otherMgr->childFrame_uvHead = (uint8_t*)bufferBlockInfo->uvHead;
        }

        // signal to the master that we have a new frame
        pthread_cond_signal(&(otherMgr->stereoCond));
        pthread_cond_wait(&stereoCond, &(otherMgr->stereoMutex));
        pthread_mutex_unlock(&(otherMgr->stereoMutex));

        // slave runs autoexposure in independent exposure mode only
        if(configInfo.ind_exp){
            runModalExposure(previewFrameGrey8, meta);
        }
    }


    // master is similar to mono mode, except we need to do the stereo sync
    // logic first, then publish two frames with some extra logic
    else if (partnerMode == MODE_STEREO_MASTER)
    {
        // do the stereo syncing duties
        if(handleStereoSync(&meta)) return;

        if(configInfo.en_raw_preview){
            publishStereoRAWPreviewFrame(meta);
            runModalExposure(previewFrameGrey8, meta);
        }else{
            publishStereoISPFrame(bufferBlockInfo, meta);
            runModalExposure((uint8_t*)bufferBlockInfo->vaddress, meta);
        }

        //Pass back the new AE values to the other camera
        if(!configInfo.ind_exp) {
            otherMgr->setExposure = setExposure;
            otherMgr->setGain = setGain;
        }

        //Clear the pointers and signal the child thread for cleanup
        childFrame = NULL;
        pthread_mutex_unlock(&stereoMutex);
        pthread_cond_signal(&(otherMgr->stereoCond));
    }


    else{
        M_ERROR("UNKNOWN partnerMode\n");
    }

    //M_ERROR("1 %s \n", name);
    return;
} // end of PerCameraMgr::ProcessPreviewFrame


void PerCameraMgr::ProcessSmallVideoFrame(image_result result)
{
    BufferBlock* bufferBlockInfo = bufferGetBufferInfo(&small_vid_bufferGroup, result.second.buffer);

    // return if there are no clients
    if(!HasClientForSmallVideo()){
      bufferPush(small_vid_bufferGroup, result.second.buffer);
        return;
    }

    camera_image_metadata_t meta;
    if(getMeta(result.first, &meta)) {
        M_WARN("Trying to process encode buffer without metadata\n");
        bufferPush(small_vid_bufferGroup, result.second.buffer);
        return;
    }

    initMetaFromBlock(*bufferBlockInfo, meta);
    publishISPFrame(*bufferBlockInfo, meta, vid_halfmt, smallVideoPipeGrey,       smallVideoPipeColor);

    // no need to pass data to OMX if there are no h264/h265 clients, might be clients
    // for the uncompressed streams published above though!
    if(pipe_server_get_num_clients(smallVideoPipeEncoded)<1){
        bufferPush(small_vid_bufferGroup, result.second.buffer);
        return;
    }
    
    // check health of the encoder and drop this frame if it's getting backed up
    int n = pVideoEncoderSmall->ItemsInQueue();
    if(n>SMALL_VID_ALLOWED_ITEMS_IN_OMX_QUEUE){
        M_DEBUG("dropping small video frame, OMX is getting backed up, has %d in queue already\n", n);
        bufferPush(small_vid_bufferGroup, result.second.buffer);
        return;
    }

    // Write OSD if enabled
    if(configInfo.small_venc_config.osd) writeOSD((uint8_t*)bufferBlockInfo->vaddress, bufferBlockInfo->width, bufferBlockInfo->stride, bufferBlockInfo->height, smallOsdConfig);

    // add to the OMX queue
    pVideoEncoderSmall->ProcessFrameToEncode(meta, bufferBlockInfo);

    return;
}


void PerCameraMgr::ProcessLargeVideoFrame(image_result result)
{
    BufferBlock* bufferBlockInfo = bufferGetBufferInfo(&large_vid_bufferGroup, result.second.buffer);

    // return if there are no clients
    if(!HasClientForLargeVideo()){
        bufferPush(large_vid_bufferGroup, result.second.buffer);
        return;
    }

    camera_image_metadata_t meta;
    if(getMeta(result.first, &meta)) {
        M_WARN("Trying to process encode buffer without metadata\n");
        bufferPush(large_vid_bufferGroup, result.second.buffer);
        return;
    }

    initMetaFromBlock(*bufferBlockInfo, meta);
    publishISPFrame(*bufferBlockInfo, meta, vid_halfmt, largeVideoPipeGrey, largeVideoPipeColor);

    // no need to pass data to OMX if there are no h264/h265 clients, might be clients
    // for the uncompressed streams published above though!
    if(pipe_server_get_num_clients(largeVideoPipeEncoded)<1){
        bufferPush(large_vid_bufferGroup, result.second.buffer);
        return;
    }

    // check health of the encoder and drop this frame if it's getting backed up
    int n = pVideoEncoderLarge->ItemsInQueue();
    if(n>LARGE_VID_ALLOWED_ITEMS_IN_OMX_QUEUE){
        M_DEBUG("dropping large video frame, OMX is getting backed up, has %d in queue already\n", n);
        bufferPush(large_vid_bufferGroup, result.second.buffer);
        return;
    }

    // Write OSD if enabled
    if(configInfo.large_venc_config.osd) writeOSD((uint8_t*)bufferBlockInfo->vaddress, bufferBlockInfo->width, bufferBlockInfo->stride, bufferBlockInfo->height, smallOsdConfig);

    // add to the OMX queue
    pVideoEncoderLarge->ProcessFrameToEncode(meta, bufferBlockInfo);

    return;
}

void PerCameraMgr::writeOSD(uint8_t* frame, uint32_t width, uint32_t stride, uint32_t height, osd_t osdConfig){
    bool found_menu_header{false};
    int betaflight_menu_active_cnt{0};
    int betaflight_menu_exit_pending_cnt{0};
    static bool betaflight_menu_active{false};
    static bool betaflight_menu_exit_pending{false};
    static std::string bf_menu_header{""};
    static std::map<std::pair<uint8_t, uint8_t>, std::string> static_map;

    msp_dp_canvas_t dimensions = grab_msp_osd_canvas_dims();
    uint32_t x_scale = width/(dimensions.col_max+1);
    uint32_t y_scale = height/(dimensions.row_max+1);

    // Prevent OSD flickering by caching & using the latest map contents until we get a new full map 
    std::map<std::pair<uint8_t, uint8_t>, std::string> map = grab_msp_osd_map();
    if(map.empty()) map = static_map;
    for(auto& entry : map){
        if(entry.second.empty()) {
            continue;
        }
        if(entry.second.find("-- MAIN --") != std::string::npos){
            betaflight_menu_active_cnt++;
        }
        if(entry.second.find("-- SAVE/EXIT --") != std::string::npos){
            betaflight_menu_exit_pending_cnt++;
        }
        if(entry.second.find("-") != std::string::npos && entry.second != bf_menu_header){
            if(!found_menu_header) {
                bf_menu_header = entry.second;
                found_menu_header=true;
            }
        }
        cCharacter_write_white_res(frame, stride, width, height, (char*)entry.second.c_str(), 
                            (int)(entry.first.second*x_scale), 
                            (int)(entry.first.first*y_scale), 
                            true);
    }

    if(betaflight_menu_active_cnt) betaflight_menu_active=true;
    if(betaflight_menu_exit_pending_cnt) betaflight_menu_exit_pending=true;
    if(betaflight_menu_active) bf_menu_active(true);
    if(betaflight_menu_exit_pending && betaflight_menu_exit_pending_cnt==0){
        betaflight_menu_active=false;
        betaflight_menu_exit_pending=false;
        bf_menu_header = "";
        bf_menu_active(false);
    } 

    static_map = map;

    // Return for now.. maybe will make a debug mode to print the rest below later
    return;

    /*
	// Battery info
	char batteryVoltage[MAX_STRING_LEN];
	char batteryAmps[MAX_STRING_LEN];
	snprintf(batteryVoltage, MAX_STRING_LEN, "BATT: %2.2fV", grab_battery_voltage());
	snprintf(batteryAmps, MAX_STRING_LEN, "AMP: %3.2fA", grab_battery_amps());
	cCharacter_write_white_res(frame, stride, width, height, batteryVoltage, osdConfig.batt_col, osdConfig.batt_row);
	cCharacter_write_white_res(frame, stride, width, height, batteryAmps, osdConfig.amps_col, osdConfig.amps_row);

	// CPU Temp 
	char cpuTemp[MAX_STRING_LEN];
	snprintf(cpuTemp, MAX_STRING_LEN, "VTX: %.1fC", grab_cpu_temp());
	cCharacter_write_white_res(frame, stride, width, height, cpuTemp, osdConfig.cpu_t_col, osdConfig.cpu_t_row);

	// CPU Load
	char cpuLoad[MAX_STRING_LEN];
	snprintf(cpuLoad, MAX_STRING_LEN, "CPU: %.1f%%", grab_cpu_load());
	cCharacter_write_white_res(frame, stride, width, height, cpuLoad, osdConfig.cpu_l_col, osdConfig.cpu_l_row);

	// Horizon
	int horizonColumnOffset=5;
	double horizonRollOffset = tan(grab_uav_state().roll_deg*M_PI/180.0)/2;  
	for (int i=0;i<11;++i){ // There are 5 dashes on either side of the middle dash -> 11 dashes for the roll/horizon indicator
		cCharacter_write_white_res(frame, stride, width, height, 
									(char*)"-",
									osdConfig.hrzn_col+(OSD_CHAR_HEIGHT*i), 
									round(osdConfig.hrzn_row+horizonRollOffset*OSD_CHAR_HEIGHT*horizonColumnOffset--));    
    }

	// Pitch
	float pitch = grab_uav_state().pitch_deg;
	cCharacter_write_white_res(frame, stride, width, height, (char*)PITCH, osdConfig.pitch_col, osdConfig.pitch_top_row +((int)pitch*2));
	cCharacter_write_white_res(frame, stride, width, height, (char*)PITCH, osdConfig.pitch_col, osdConfig.pitch_bott_row+((int)pitch*2));


	// RC
	char rssi[MAX_STRING_LEN];
	snprintf(rssi, MAX_STRING_LEN, "RC: %d", grab_rssi());
	cCharacter_write_white_res(frame, stride, width, height, rssi, osdConfig.rssi_col, osdConfig.rssi_row);

	// Armed/Disarmed
	if (!grab_armed()) cCharacter_write_white_res(frame, stride, width, height, (char*)"DISARMED", osdConfig.dsrm_col, osdConfig.dsrm_row);

	// Flight mode | Arming | Heading 
	static int displayOffset = (width == 1280 || width == 1024) ? 3 : 1; // Only worrying about video resolutions that make sense right now!
	char displayMsg[MAX_STRING_LEN];
	snprintf(displayMsg, MAX_STRING_LEN, "%s | %s | %s", grab_flight_mode(), grab_armed() ? "ARM" : "DSRM", grab_heading());
	cCharacter_write_white_res(frame, stride, width, height, displayMsg, osdConfig.disp_col-(strlen(displayMsg)*(displayOffset)), osdConfig.disp_row);
    */
}



#ifndef APQ8096
/* Get an existing tag, or create one if it doesn't exist */
static ExifEntry *init_tag(ExifData *exif, ExifIfd ifd, ExifTag tag)
{
	ExifEntry *entry;
	/* Return an existing tag if one exists */
	if (!((entry = exif_content_get_entry (exif->ifd[ifd], tag)))) {
	    /* Allocate a new entry */
	    entry = exif_entry_new ();
	    assert(entry != NULL); /* catch an out of memory condition */
	    entry->tag = tag; /* tag must be set before calling
				 exif_content_add_entry */

	    /* Attach the ExifEntry to an IFD */
	    exif_content_add_entry (exif->ifd[ifd], entry);

	    /* Allocate memory for the entry and fill with default data */
	    exif_entry_initialize (entry, tag);

	    /* Ownership of the ExifEntry has now been passed to the IFD.
	     * One must be very careful in accessing a structure after
	     * unref'ing it; in this case, we know "entry" won't be freed
	     * because the reference count was bumped when it was added to
	     * the IFD.
	     */
	    exif_entry_unref(entry);
	}
	return entry;
}

static ExifEntry *create_tag(ExifData *exif, ExifIfd ifd, ExifTag tag, size_t len)
{
	void *buf;
	ExifEntry *entry;
	
	/* Create a memory allocator to manage this ExifEntry */
	ExifMem *mem = exif_mem_new_default();
	assert(mem != NULL); /* catch an out of memory condition */

	/* Create a new ExifEntry using our allocator */
	entry = exif_entry_new_mem (mem);
	assert(entry != NULL);

	/* Allocate memory to use for holding the tag data */
	buf = exif_mem_alloc(mem, len);
	assert(buf != NULL);

	/* Fill in the entry */
	entry->data = (unsigned char*)buf;
	entry->size = len;
	entry->tag = tag;
	entry->components = len;
	entry->format = EXIF_FORMAT_UNDEFINED;

	/* Attach the ExifEntry to an IFD */
	exif_content_add_entry (exif->ifd[ifd], entry);

	/* The ExifMem and ExifEntry are now owned elsewhere */
	exif_mem_unref(mem);
	exif_entry_unref(entry);

	return entry;
}

// Function to create a new tag or update an existing one
static ExifEntry* create_or_update_tag(ExifData* exif, ExifIfd ifd, ExifTag tag, ExifFormat fmt, unsigned int size, unsigned int components) {
    ExifEntry* entry = exif_content_get_entry(exif->ifd[ifd], tag);
    if (!entry) {
        // Create a new entry if it doesn't exist
        //printf("Creating entry: %d\n", (int)tag);
        entry = create_tag(exif, ifd, tag, size);
        entry->tag = tag;
        entry->components = components;
        entry->format = fmt;
    } else {
        // Update the existing entry
        if (entry->size < size) {
            // If the existing entry size is insufficient, reallocate memory
            entry->data = (unsigned char*)realloc(entry->data, size);
            entry->size = size;
        }
        entry->components = components;
        entry->format = fmt;
    }
    return entry;
}

#endif

void PerCameraMgr::ProcessSnapshotFrame(image_result result)
{
    BufferBlock* bufferBlockInfo = bufferGetBufferInfo(&snap_bufferGroup, result.second.buffer);

    // first write to pipe if subscribed
    camera_image_metadata_t meta;
    if(getMeta(result.first, &meta)) {
        M_WARN("Trying to process encode buffer without metadata\n");
        return;
    }

    int start_index = 0;
    uint8_t* src_data = (uint8_t*)bufferBlockInfo->vaddress;
    // the data buffer is actually much bigger than the jpeg image. We need to find 
    // where the actual image ends in memory
    int extractJpgSize = find_jpeg_buffer_size(src_data, bufferBlockInfo->size, &start_index);
    
    assert(start_index == 0);
    
    // find APP1 block start and length
    // we will modify the existing exif block and then insert our modified
    // block instead of the original
    size_t prev_exif_block_start_idx = 0;
    size_t prev_exif_block_size = find_exif_start(src_data, bufferBlockInfo->size, &prev_exif_block_start_idx);

    if(extractJpgSize == 1){
        M_ERROR("Real Size of JPEG is incorrect");
        return;
    }

#ifndef APQ8096
    // Load the EXIF data from the file
    unsigned char *exif_data;
    unsigned int exif_data_len;
    ExifEntry *entry;
   
    // the jpeg image already has exif data so we construct the struct from jpeg buffer
    ExifData* exif = exif_data_new_from_data(src_data, extractJpgSize);
    if (exif == nullptr) {
        printf("Issue getting exif data from origin\n");
        return;
    }
    exif_data_fix(exif);
    
    /**
     * @brief Good description of exif format https://www.media.mit.edu/pia/Research/deepview/exif.html 
     * 
     */
    ExifByteOrder imageByteOrder = exif_data_get_byte_order(exif); /**< Each camera saves its Exif data in a different byte order */
    uav_state_t uav_state = grab_uav_state();

    // Code to add latitude to exif tag
    entry = create_or_update_tag(exif, EXIF_IFD_GPS, (ExifTag)EXIF_TAG_GPS_LATITUDE_REF, (ExifFormat)EXIF_FORMAT_ASCII, 2*sizeof(char), 1);
    if (uav_state.lat_deg >= 0) {
        memcpy(entry->data, "N", sizeof(char));
    } else {
        memcpy(entry->data, "S", sizeof(char));
        uav_state.lat_deg *= -1;
    }
    
    
    entry = create_or_update_tag(exif, EXIF_IFD_GPS, (ExifTag)EXIF_TAG_GPS_LATITUDE, (ExifFormat)EXIF_FORMAT_RATIONAL, 3*sizeof(ExifRational), 3);

    ExifLong degrees_lat = static_cast<ExifLong>(uav_state.lat_deg);
    
    double fractional = (uav_state.lat_deg - degrees_lat);
    ExifLong minutes_lat = static_cast<ExifLong>(fractional*60);
    
    fractional = (fractional*60) - minutes_lat;
    double seconds_lat = (fractional * 60);
    
    ExifRational degrees_r = { degrees_lat, 1 };
    ExifRational minutes_r = { minutes_lat, 1 };
    ExifRational seconds_r = { static_cast<ExifLong>(seconds_lat * 1000000), 1000000 }; // Increased precision for seconds
    
    exif_set_rational(entry->data, imageByteOrder, degrees_r);
    exif_set_rational(entry->data + sizeof(ExifRational), imageByteOrder, minutes_r);
    exif_set_rational(entry->data + 2 * sizeof(ExifRational), imageByteOrder, seconds_r);

    // Code to add longitude to exif tag
    entry = create_or_update_tag(exif, EXIF_IFD_GPS, (ExifTag)EXIF_TAG_GPS_LONGITUDE_REF, (ExifFormat)EXIF_FORMAT_ASCII, 2*sizeof(char), 1);
    if (uav_state.lon_deg >= 0) {
        memcpy(entry->data, "E", sizeof(char));
    } else {
        memcpy(entry->data, "W", sizeof(char));
        uav_state.lon_deg *= -1;
    }

    entry = create_or_update_tag(exif, EXIF_IFD_GPS, (ExifTag)EXIF_TAG_GPS_LONGITUDE, (ExifFormat)EXIF_FORMAT_RATIONAL, 3*sizeof(ExifRational), 3);

    ExifLong degrees_lon = static_cast<ExifLong>(uav_state.lon_deg);
    
    fractional = (uav_state.lon_deg - degrees_lon);
    ExifLong minutes_lon = static_cast<ExifLong>(fractional*60);
    
    fractional = (fractional*60) - minutes_lon;
    double seconds_lon = (fractional * 60);
    
    degrees_r = { degrees_lon, 1 };
    minutes_r = { minutes_lon, 1 };
    seconds_r = { static_cast<ExifLong>(seconds_lon * 1000000), 1000000 }; // Increased precision for seconds
    
    exif_set_rational(entry->data, imageByteOrder, degrees_r);
    exif_set_rational(entry->data + sizeof(ExifRational), imageByteOrder, minutes_r);
    exif_set_rational(entry->data + 2 * sizeof(ExifRational), imageByteOrder, seconds_r); 

    // Code to add altitude to exif tag
    entry = create_or_update_tag(exif, EXIF_IFD_GPS, (ExifTag)EXIF_TAG_GPS_ALTITUDE, (ExifFormat)EXIF_FORMAT_RATIONAL, sizeof(ExifRational), 1);

    double alt_lon = uav_state.alt_msl_meters;
    unsigned int tmp = static_cast<unsigned int>(alt_lon * 1000);
    exif_set_rational(entry->data, imageByteOrder, (ExifRational){tmp, 1000});
    
    
    entry = create_or_update_tag(exif, EXIF_IFD_GPS, (ExifTag)EXIF_TAG_GPS_ALTITUDE_REF, (ExifFormat)EXIF_FORMAT_BYTE, sizeof(uint8_t), 1);
    entry->data[0] = 0; /**< 0 for above sea level */
    
    entry = create_or_update_tag(exif, EXIF_IFD_EXIF, (ExifTag)EXIF_TAG_FOCAL_LENGTH, (ExifFormat)EXIF_FORMAT_RATIONAL, sizeof(ExifRational), 1);
    ExifRational focal_len_r = {static_cast<ExifLong>(this->focal_length * 1000000), 1000000};
    exif_set_rational(entry->data, imageByteOrder, focal_len_r);
    
    entry = create_or_update_tag(exif, EXIF_IFD_EXIF, (ExifTag)EXIF_TAG_FNUMBER, (ExifFormat)EXIF_FORMAT_RATIONAL, sizeof(ExifRational), 1);
    ExifRational f_number_r = {static_cast<ExifLong>(this->fnumber * 1000000), 1000000};
    exif_set_rational(entry->data, imageByteOrder, f_number_r);
    
    
    entry = create_or_update_tag(exif, EXIF_IFD_EXIF, (ExifTag)EXIF_TAG_FOCAL_LENGTH_IN_35MM_FILM, (ExifFormat)EXIF_FORMAT_SHORT, sizeof(ExifShort), 1);
    ExifShort f_35_mm_r = this->focal_len_35mm_format;
    exif_set_short(entry->data, imageByteOrder, f_35_mm_r);
    
    exif_data_save_data(exif, &exif_data, &exif_data_len);
    
    assert(exif_data != NULL);
#endif

    meta.magic_number = CAMERA_MAGIC_NUMBER;
    meta.width        = snap_width;
    meta.height       = snap_height;
    meta.format       = IMAGE_FORMAT_JPG;
    meta.size_bytes   = extractJpgSize;
    pipe_server_write_camera_frame(snapshotPipe, meta, &src_data[start_index]);

    // now, if there is a filename in the queue, write it too
    if(snapshotQueue.size() != 0){
        char *filename = snapshotQueue.front();
        snapshotQueue.pop();

        M_PRINT("Camera: %s writing snapshot to :\"%s\"\n", name, filename);
        //WriteSnapshot(bufferBlockInfo, snap_halfmt, filename);

        FILE* file_descriptor = fopen(filename, "wb");
        if(! file_descriptor){

            //Check to see if we were just missing parent directories
            CreateParentDirs(filename);

            file_descriptor = fopen(filename, "wb");

            if(! file_descriptor){
                M_ERROR("failed to open file descriptor for snapshot save to: %s\n", filename);
                return;
            }
        }

#ifndef APQ8096
        
        // first write the beginning of jpeg and stop where the previous exif APP1 header started
        // this will include the FFD8 marker
        if (fwrite(src_data, prev_exif_block_start_idx, 1, file_descriptor) != 1) {
            fprintf(stderr, "Error writing to file with jpeg %s\n", filename);
        }
        
        if (fwrite(exif_header, exif_header_len, 1, file_descriptor) != 1) {
           fprintf(stderr, "Error writing to file inin exif header %s\n", filename);
        }
        if (fputc((exif_data_len+2) >> 8, file_descriptor) < 0) {
           fprintf(stderr, "Error writing to file in big endian order %s\n", filename);
        }
        if (fputc((exif_data_len+2) & 0xff, file_descriptor) < 0) {
           fprintf(stderr, "Error writing to file with fputc %s\n", filename);
        }
        if (fwrite(exif_data, exif_data_len, 1, file_descriptor) != 1) {
           fprintf(stderr, "Error writing to file with data block %s\n", filename);
        }
        
        // next write from the last bits of the prev APP1 block
        size_t jpeg_start_idx = prev_exif_block_start_idx + prev_exif_block_size;
        size_t jpeg_size = extractJpgSize - prev_exif_block_size;
        
        
        if (fwrite(src_data + jpeg_start_idx, jpeg_size, 1, file_descriptor) != 1) {
            fprintf(stderr, "Error writing to file with jpeg %s\n", filename);
        }
        
        free(exif_data);
        exif_data_unref(exif);
#else 
        int ret = fwrite(&src_data[start_index], extractJpgSize, 1, file_descriptor);
        if(ret!=1){
            M_ERROR("snapshot failed to write to disk\n");
        }
#endif

        fclose(file_descriptor);
        free(filename);
    }
    else{
        M_VERBOSE("wrote snapshot to pipe but not to disk\n");
    }
}








#ifdef APQ8096
// This is for the old royale lib on APQ8096

// -----------------------------------------------------------------------------------------------------------------------------
// The TOF library calls this function when it receives data from the Royale PMD libs
// -----------------------------------------------------------------------------------------------------------------------------
// Callback function for the TOF bridge to provide the post processed TOF data
bool PerCameraMgr::RoyaleDataDone(const void*             pData,
                                  uint32_t                size,
                                  int64_t                 timestamp,
                                  RoyaleListenerType      dataType)
{

    M_VERBOSE("Received royale data for camera: %s\n", name);

    constexpr int MAX_IR_VALUE_IN  = 2895;
    constexpr int MAX_IR_VALUE_OUT = (1<<8);

    const royale::DepthData* pDepthData               = static_cast<const royale::DepthData *> (pData);
    const royale::Vector<royale::DepthPoint>& pointIn = pDepthData->points;
    int numPoints = (int)pointIn.size();

    camera_image_metadata_t IRMeta, DepthMeta, ConfMeta;
    point_cloud_metadata_t PCMeta;

    // set up some common metadata
    IRMeta.timestamp_ns = pDepthData->timeStamp.count();
    IRMeta.gain         = 0;
    IRMeta.exposure_ns  = 0;
    IRMeta.frame_id     = ++TOFFrameNumber;
    IRMeta.width        = pDepthData->width;
    IRMeta.height       = pDepthData->height;
    int w               = pDepthData->width;
    int h               = pDepthData->height;
    DepthMeta = IRMeta;
    ConfMeta  = IRMeta;

    if(pipe_server_get_num_clients(tofPipeIR)>0){
        IRMeta.stride         = IRMeta.width * sizeof(uint8_t);
        IRMeta.size_bytes     = IRMeta.stride * IRMeta.height;
        IRMeta.format         = IMAGE_FORMAT_RAW8;
        uint8_t IRData[numPoints];
        for(int i = 0; i < numPoints; i++){
            royale::DepthPoint point = pointIn[i];
            uint32_t longval = point.grayValue;
            longval *= MAX_IR_VALUE_OUT;
            longval /= MAX_IR_VALUE_IN;
            IRData[i]    = longval;
        }
    pipe_server_write_camera_frame(tofPipeIR, IRMeta, IRData);
    }

    if(pipe_server_get_num_clients(tofPipeDepth)>0){
        DepthMeta.stride      = DepthMeta.width * sizeof(uint8_t);
        DepthMeta.size_bytes  = DepthMeta.stride * DepthMeta.height;
        DepthMeta.format      = IMAGE_FORMAT_RAW8;
        uint8_t DepthData[numPoints];
        for (int i = 0; i < numPoints; i++)
        {
            DepthData[i] = (uint8_t)((pointIn[i].z / 5) * 255);
        }
        pipe_server_write_camera_frame(tofPipeDepth, DepthMeta, DepthData);
    }

    if(pipe_server_get_num_clients(tofPipeConf)>0){
        ConfMeta.stride       = ConfMeta.width * sizeof(uint8_t);
        ConfMeta.size_bytes   = ConfMeta.stride * ConfMeta.height;
        ConfMeta.format       = IMAGE_FORMAT_RAW8;
        uint8_t ConfData[numPoints];
        for (int i = 0; i < numPoints; i++)
        {
            royale::DepthPoint point = pointIn[i];
            ConfData[i] = point.depthConfidence;
        }
        pipe_server_write_camera_frame(tofPipeConf, ConfMeta, ConfData);
    }

    if(pipe_server_get_num_clients(tofPipePC)>0){
        PCMeta.timestamp_ns   = IRMeta.timestamp_ns;
        PCMeta.n_points       = numPoints;
        float PointCloud[numPoints*3];
        for (int i = 0; i < numPoints; i++)
        {
            royale::DepthPoint point = pointIn[i];
            PointCloud[(i*3)]   = point.x;
            PointCloud[(i*3)+1] = point.y;
            PointCloud[(i*3)+2] = point.z;
        }
        pipe_server_write_point_cloud(tofPipePC, PCMeta, PointCloud);
    }

    if(pipe_server_get_num_clients(tofPipeFull)>0){
        tof2_data_t FullData;
        FullData.magic_number = TOF2_MAGIC_NUMBER;
        FullData.timestamp_ns = IRMeta.timestamp_ns;
        FullData.width        = pDepthData->width;
        FullData.height       = pDepthData->height;

        for (int i = 0; i < numPoints; i++)
        {
            royale::DepthPoint point = pointIn[i];
            FullData.points     [i][0] = point.x;
            FullData.points     [i][1] = point.y;
            FullData.points     [i][2] = point.z;
            FullData.noises     [i]    = point.noise;
            uint32_t longval = point.grayValue;
            longval *= MAX_IR_VALUE_OUT;
            longval /= MAX_IR_VALUE_IN;
            FullData.grayValues [i]    = longval;
            FullData.confidences[i]    = point.depthConfidence;
        }
        pipe_server_write(tofPipeFull, (const char *)(&FullData), sizeof(tof2_data_t));
    }

    return true;
}


#else
// this is for the new Royale 5.8 lib on QRB5165


// -----------------------------------------------------------------------------------------------------------------------------
// The TOF library calls this function when it receives data from the Royale PMD libs
// -----------------------------------------------------------------------------------------------------------------------------
// Callback function for the TOF bridge to provide the post processed TOF data
bool PerCameraMgr::RoyaleDataDone(const void*             pData,
                                  uint32_t                size,
                                  int64_t                 timestamp,
                                  RoyaleListenerType      dataType)
{

    M_VERBOSE("Received royale data for camera: %s\n", name);

    // the real max value is 2000+ but these are overexposed areas
    // we scale it so every value over 40 corresponds to 255 on the output
    // image so we can a nice pretty picture
    constexpr int MAX_IR_VALUE_IN  = 40;

    const royale::DepthData* pDepthData               = static_cast<const royale::DepthData *> (pData);
    int numPoints = pDepthData->width * pDepthData->height;

    camera_image_metadata_t IRMeta, DepthMeta, ConfMeta;
    point_cloud_metadata_t PCMeta;

    //temperature is only reported from LIOW2
    if (configInfo.type == SENSOR_TOF_LIOW2){
        sensorTemperature   = pDepthData->illuminationTemperature;
        M_VERBOSE("TOF LIOW2 temp: %f\n",sensorTemperature);
    }

    royale::Vector<uint32_t> exposureTimes = pDepthData->exposureTimes;
    if (exposureTimes.size() > 0){
        M_VERBOSE("TOF exp(%u): %uus\n",exposureTimes.size(),exposureTimes[exposureTimes.size()-1]);
        setExposure = exposureTimes[exposureTimes.size()-1];
        //M_VERBOSE("TOF LIOW2 exp(%u): %uus %uus %uus\n",exposureTimes.size(),exposureTimes[0],exposureTimes[1],exposureTimes[2]);
    }

    // set up some common metadata
    IRMeta.timestamp_ns = pDepthData->timeStamp.count();
    IRMeta.gain         = 0;
    IRMeta.exposure_ns  = setExposure*1000 * (tof_interface->getPhases()-1);
    IRMeta.frame_id     = ++TOFFrameNumber;
    int w               = pDepthData->width;
    int h               = pDepthData->height;
    IRMeta.width        = w;
    IRMeta.height       = h;
    IRMeta.stride       = w;
    IRMeta.format       = IMAGE_FORMAT_RAW8;
    IRMeta.size_bytes   = w*h;

    // depth and conf images share dimensions and type
    DepthMeta = IRMeta;
    ConfMeta  = IRMeta;

    if(pipe_server_get_num_clients(tofPipeIR)>0){
        uint8_t IRData[numPoints];
        // normal unrotated
        if(!configInfo.en_rotate){
            for (int i = 0; i < numPoints; i++){
                uint32_t longval = pDepthData->amplitudes[i]*255;
                longval /= MAX_IR_VALUE_IN;
                if(longval>255) longval=255;
                IRData[i]    = longval;
            }
        }
        // rotate for starling
        else{
            IRMeta.width        = h;
            IRMeta.height       = w;
            IRMeta.stride       = h;
            for(int i = 0; i < h; i++){
                for(int j = 0; j < w; j++){
                    uint32_t longval = pDepthData->amplitudes[(i*w)+j]*255;
                    longval /= MAX_IR_VALUE_IN;
                    if(longval>255) longval=255;
                    IRData[(j*h)+h-1-i]    = longval;
                }
            }
        }
        pipe_server_write_camera_frame(tofPipeIR, IRMeta, IRData);
    }

    if(pipe_server_get_num_clients(tofPipeDepth)>0){
        uint8_t DepthData[numPoints];
        // normal unrotated
        if(!configInfo.en_rotate){
            for (int i = 0; i < numPoints; i++){
                DepthData[i] = (uint8_t)((pDepthData->getZ(i) / 5.0) * 255.0);
            }
        }
        // rotate for starling
        else{
            DepthMeta.width        = h;
            DepthMeta.height       = w;
            DepthMeta.stride       = h;
            for(int i = 0; i < h; i++){
                for(int j = 0; j < w; j++){
                    DepthData[(j*h)+h-1-i] = (uint8_t)((pDepthData->getZ((i*w)+j) / 5.0) * 255.0);
                }
            }
        }
        pipe_server_write_camera_frame(tofPipeDepth, DepthMeta, DepthData);
    }

    if(pipe_server_get_num_clients(tofPipeConf)>0){
        uint8_t ConfData[numPoints];
        // normal unrotated
        if(!configInfo.en_rotate){
            for (int i = 0; i < numPoints; i++){
                // don't know why this is called noise, 1 is a good point, 0 is bad
                float noise = pDepthData->getDepthConfidence(i);
                uint8_t conf = 0;
                if(noise>=1.0) conf = 255;
                else conf = noise*255;
                ConfData[i] = conf;
            }
        }
        // rotate for starling
        else{
            ConfMeta.width        = h;
            ConfMeta.height       = w;
            ConfMeta.stride       = h;
            for(int i = 0; i < h; i++){
                for(int j = 0; j < w; j++){
                    // don't know why this is called noise, 1 is a good point, 0 is bad
                    float noise = pDepthData->getDepthConfidence((i*w)+j);
                    uint8_t conf = 0;
                    if(noise>=1.0) conf = 255;
                    else conf = noise*255;
                    ConfData[(j*h)+h-1-i] = conf;
                }
            }
        }
        pipe_server_write_camera_frame(tofPipeConf, ConfMeta, ConfData);
    }

    if(pipe_server_get_num_clients(tofPipePC)>0){
        PCMeta.timestamp_ns   = IRMeta.timestamp_ns;
        PCMeta.n_points       = numPoints;
        float PointCloud[numPoints*3];
        for (int i = 0; i < numPoints; i++)
        {
            PointCloud[(i*3)]   = pDepthData->getX(i);
            PointCloud[(i*3)+1] = pDepthData->getY(i);
            PointCloud[(i*3)+2] = pDepthData->getZ(i);
        }
        pipe_server_write_point_cloud(tofPipePC, PCMeta, PointCloud);
    }

    if(pipe_server_get_num_clients(tofPipeFull)>0){
        tof2_data_t FullData;
        FullData.magic_number = TOF2_MAGIC_NUMBER;
        FullData.timestamp_ns = IRMeta.timestamp_ns;
        FullData.width        = pDepthData->width;
        FullData.height       = pDepthData->height;

        for (int i = 0; i < numPoints; i++)
        {
            // normal xyz
            FullData.points[i][0] = pDepthData->getX(i);
            FullData.points[i][1] = pDepthData->getY(i);
            FullData.points[i][2] = pDepthData->getZ(i);

            // intensity
            uint32_t longval = pDepthData->amplitudes[i]*255;
            longval /= MAX_IR_VALUE_IN;
            if(longval>255) longval=255;
            FullData.grayValues [i]    = longval;

            // don't know why this is called noise, 1 is a good point, 0 is bad
            float noise = pDepthData->getDepthConfidence(i);
            uint8_t conf = 0;
            if(noise>=1.0) conf = 255;
            else conf = noise*255;
            FullData.confidences[i] = conf;

            // noise, TODO figure out how to get real numbers from royale
            FullData.noises[i]    = (float)conf/255.0;
        }

        pipe_server_write(tofPipeFull, (const char *)(&FullData), sizeof(tof2_data_t));
    }

    return true;
}


#endif // end of apq8096/qrb5165 split


// -----------------------------------------------------------------------------------------------------------------------------
// PerCameraMgr::CameraModuleCaptureResult(..) is the entry callback that is registered with the camera module to be called when
// the camera module has frame result available to be processed by this application. We do not want to do much processing in
// that function since it is being called in the context of the camera module. So we do the bare minimum processing and leave
// the remaining process upto this function. PerCameraMgr::CameraModuleCaptureResult(..) just pushes a message in a queue that
// is monitored by this thread function. This function goes through the message queue and processes all the messages in it. The
// messages are nothing but camera images that this application has received.
// -----------------------------------------------------------------------------------------------------------------------------
void* PerCameraMgr::ThreadPostProcessResult()
{
    { // Configuration, these variables don't need to persist
        char buf[16];
        pid_t tid = syscall(SYS_gettid);
        sprintf(buf, "cam%d-result", cameraId);
        pthread_setname_np(pthread_self(), buf);
        M_VERBOSE("Entered thread: %s(tid: %lu)\n", buf, tid);

        // Set thread priority
        int which = PRIO_PROCESS;
        int nice  = -10;
        setpriority(which, tid, nice);
    }

    uint8_t num_finished_streams = en_snapshot;
    
    // The condition of the while loop is such that this thread will not terminate till it receives the last expected image
    // frame from the camera module or detects the ESTOP flag
    while (!EStopped && num_finished_streams != num_streams)
    {
        pthread_mutex_lock(&resultMutex);
        if (resultMsgQueue.empty())
        {
            //Wait for a signal that we have Received a frame or an estop
            pthread_cond_wait(&resultCond, &resultMutex);
        }

        if(EStopped || stopped) {
            pthread_mutex_unlock(&resultMutex);
            break;
        }

        if (resultMsgQueue.empty()) {
            pthread_mutex_unlock(&resultMutex);
            continue;
        }

        image_result result = resultMsgQueue.front();
        resultMsgQueue.pop();
        pthread_mutex_unlock(&resultMutex);

        buffer_handle_t  *handle      = result.second.buffer;
        camera3_stream_t *stream      = result.second.stream;
        BufferGroup      *bufferGroup = GetBufferGroup(stream);


        // Coming here means we have a result frame to process
        M_VERBOSE("%s procesing new buffer\n", name);
        switch (GetStreamId(stream)){
            case STREAM_PREVIEW:
                M_VERBOSE("Camera: %s processing preview frame\n", name);
                ProcessPreviewFrame(result);
                bufferPush(*bufferGroup, handle); // This queues up the buffer for recycling
                break;

            case STREAM_SMALL_VID: // Not Ready
                M_VERBOSE("Camera: %s processing small vid frame\n", name);
                ProcessSmallVideoFrame(result);
                break;

            case STREAM_LARGE_VID: // Not Ready
                M_VERBOSE("Camera: %s processing large vid frame\n", name);
                ProcessLargeVideoFrame(result);
                break;

            case STREAM_SNAPSHOT:
                M_VERBOSE("Camera: %s processing snapshot frame\n", name);
                ProcessSnapshotFrame(result);
                bufferPush(*bufferGroup, handle); // This queues up the buffer for recycling
                break;

            default:
                M_ERROR("Camera: %s Received frame for unknown stream\n", name);
                bufferPush(*bufferGroup, handle); // This queues up the buffer for recycling
                break;
        }

        if (lastResultFrameNumber == result.first){
            num_finished_streams++;
        }



    }

    if(EStopped){
        M_WARN("Thread: %s result thread Received ESTOP\n", name);
    }else{
        M_DEBUG("------ Last %s result frame: %d\n", name, lastResultFrameNumber);
    }

    M_VERBOSE("Leaving %s result thread\n", name);

    return NULL;
}


int PerCameraMgr::HasClientForPreviewFrame()
{
    if(is_tof_sensor(configInfo.type)){
        if(pipe_server_get_num_clients(tofPipeIR   )>0) return 1;
        if(pipe_server_get_num_clients(tofPipeDepth)>0) return 1;
        if(pipe_server_get_num_clients(tofPipeConf )>0) return 1;
        if(pipe_server_get_num_clients(tofPipePC   )>0) return 1;
        if(pipe_server_get_num_clients(tofPipeFull )>0) return 1;
    }
    else if(partnerMode == MODE_STEREO_SLAVE){
        if(pipe_server_get_num_clients(otherMgr->previewPipeGrey)>0) return 1;
        if(otherMgr->previewPipeColor>=0 && pipe_server_get_num_clients(otherMgr->previewPipeColor)>0) return 1;
    }
    else{
        if(previewPipeGrey >=0 && pipe_server_get_num_clients(previewPipeGrey)>0)  return 1;
        if(previewPipeColor>=0 && pipe_server_get_num_clients(previewPipeColor)>0) return 1;
        if(previewPipeBayer>=0 && pipe_server_get_num_clients(previewPipeBayer)>0) return 1;
        if(mispPipeGrey >=0    && pipe_server_get_num_clients(mispPipeGrey)>0)     return 1;
        if(mispPipeColor>=0    && pipe_server_get_num_clients(mispPipeColor)>0)    return 1;
        if(mispPipeGrey >=0    && pipe_server_get_num_clients(mispPipeGrey)>0)     return 1;
        if(mispPipeEncoded>=0  && pipe_server_get_num_clients(mispPipeEncoded)>0)  return 1;
        if(mispPipeNorm>=0     && pipe_server_get_num_clients(mispPipeNorm)>0)     return 1;
    }
    return 0;
}


int PerCameraMgr::HasClientForSmallVideo()
{
    if(pipe_server_get_num_clients(smallVideoPipeGrey  )>0) return 1;
    if(pipe_server_get_num_clients(smallVideoPipeColor )>0) return 1;
    if(pipe_server_get_num_clients(smallVideoPipeEncoded  )>0) return 1;
    return 0;
}

int PerCameraMgr::HasClientForLargeVideo()
{
    if(pipe_server_get_num_clients(largeVideoPipeGrey  )>0) return 1;
    if(pipe_server_get_num_clients(largeVideoPipeColor )>0) return 1;
    if(pipe_server_get_num_clients(largeVideoPipeEncoded  )>0) return 1;
    return 0;
}


// -----------------------------------------------------------------------------------------------------------------------------
// Send one capture request to the camera module
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::SendOneCaptureRequest(uint32_t* frameNumber)
{
    camera3_capture_request_t request;

    if(ae_mode != AE_ISP){
        M_VERBOSE("setting hal3 exposure for %s  %10ld %5d\n", name, setExposure, setGain);
        requestMetadata.update(ANDROID_SENSOR_EXPOSURE_TIME, &setExposure, 1);
        requestMetadata.update(ANDROID_SENSOR_SENSITIVITY,   &setGain, 1);
    }

    //int fpsRange[] = {fps, fps};
    int64_t frameDuration = 1e9 / fps;

    //requestMetadata.update(ANDROID_CONTROL_AE_TARGET_FPS_RANGE, &fpsRange[0],        2);
    requestMetadata.update(ANDROID_SENSOR_FRAME_DURATION,       &frameDuration,      1);

    std::vector<camera3_stream_buffer_t> streamBufferList;
    request.num_output_buffers  = 0;

    // we may need to keep the small vid frame running even if there are no subscribers
    // in the case where snapshot is enabled but no requests have been made to
    // any of the video streams. See similar logic below for the preview stream
    // which will do this is small video is not enabled. This is to handle the
    // case where the user has ONLY enabled the snapshot (no video streams). The
    // config file parsing will have detected that scenario and forced the preview
    // stream on to enable this.
    int needs_small_vid_frame_for_ae = 0;
    if(ae_mode != AE_OFF && request.num_output_buffers==0 && en_snapshot && !en_preview){
        needs_small_vid_frame_for_ae = 1;
    }

    if(en_small_video &&
        (HasClientForSmallVideo() || needs_small_vid_frame_for_ae))
    {

        int nFree = bufferNumFree(small_vid_bufferGroup);
        if(nFree<1){
            M_WARN("small vid stream buffer pool for Cam(%s), Frame(%d) has %d free, skipping request\n", name, *frameNumber, nFree);
        }
        else{

            camera3_stream_buffer_t streamBuffer;
            if ((streamBuffer.buffer   = (const native_handle_t**)bufferPop(small_vid_bufferGroup)) == NULL) {
                M_ERROR("Failed to get buffer for small vid stream: Cam(%s), Frame(%d)\n", name, *frameNumber);
                EStopCameraServer();
                return -1;
            }

            streamBuffer.stream        = &small_vid_stream;
            streamBuffer.status        = 0;
            streamBuffer.acquire_fence = -1;
            streamBuffer.release_fence = -1;

            request.num_output_buffers ++;
            streamBufferList.push_back(streamBuffer);
             M_VERBOSE("added request for small video stream\n");
        }
    }

    if(en_large_video && HasClientForLargeVideo()){

        int nFree = bufferNumFree(large_vid_bufferGroup);
        if(nFree<1){
            M_WARN("record stream buffer pool for Cam(%s), Frame(%d) has %d free, skipping request\n", name, *frameNumber, nFree);
        }
        else{
            camera3_stream_buffer_t streamBuffer;
            if ((streamBuffer.buffer   = (const native_handle_t**)bufferPop(large_vid_bufferGroup)) == NULL) {
                M_ERROR("Failed to get buffer for record stream: Cam(%s), Frame(%d)\n", name, *frameNumber);
                EStopCameraServer();
                return -1;
            }

            streamBuffer.stream        = &large_vid_stream;
            streamBuffer.status        = 0;
            streamBuffer.acquire_fence = -1;
            streamBuffer.release_fence = -1;

            request.num_output_buffers ++;
            streamBufferList.push_back(streamBuffer);
             M_VERBOSE("added request for large video stream\n");
        }
    }

    if(en_snapshot && numNeededSnapshots > 0){

        int nFree = bufferNumFree(snap_bufferGroup);
        if(nFree<1){
            M_WARN("snapshot buffer pool for Cam(%s), Frame(%d) has %d free, skipping request\n", name, *frameNumber, nFree);
        }
        else{
            numNeededSnapshots --;

            camera3_stream_buffer_t streamBuffer;
            if((streamBuffer.buffer    = (const native_handle_t**)bufferPop(snap_bufferGroup)) == NULL) {
                M_ERROR("Failed to get buffer for snapshot stream: Cam(%s), Frame(%d)\n", name, *frameNumber);
                EStopCameraServer();
                return -1;
            }
            streamBuffer.stream        = &snap_stream;
            streamBuffer.status        = 0;
            streamBuffer.acquire_fence = -1;
            streamBuffer.release_fence = -1;

            request.num_output_buffers ++;
            streamBufferList.push_back(streamBuffer);
             M_VERBOSE("added request for snapshot stream\n");
        }
    }

    // we may need to keep the preview frame running even if there are no subscribers
    // in the case where snapshot is enabled but no requests have been made to
    // any of the video streams. See similar logic above for the small video stream
    // which should take care of this first since normal hires config is to have
    // small and large video streams enabled but not preview. This is to handle the
    // case where the user has ONLY enabled the snapshot (no video streams). The
    // config file parsing will have detected that scenario and forced the preview
    // stream on to enable this.
    int needs_preview_frame_for_ae = 0;
    if(ae_mode != AE_OFF && request.num_output_buffers==0 && en_snapshot){
        needs_preview_frame_for_ae = 1;
    }

    if( en_preview &&
        (HasClientForPreviewFrame() || needs_preview_frame_for_ae))
    {
        int nFree = bufferNumFree(pre_bufferGroup);
        if(nFree<1){
            M_WARN("preview buffer pool for Cam(%s), Frame(%d) has %d free, skipping request\n", name, *frameNumber, nFree);
        }
        else{
            camera3_stream_buffer_t streamBuffer;
            if((streamBuffer.buffer    = (const native_handle_t**)bufferPop(pre_bufferGroup)) == NULL) {
                M_ERROR("Failed to get buffer for preview stream: Cam(%s), Frame(%d)\n", name, *frameNumber);
                EStopCameraServer();
                return -1;
            }
            streamBuffer.stream        = &pre_stream;
            streamBuffer.status        = 0;
            streamBuffer.acquire_fence = -1;
            streamBuffer.release_fence = -1;

            request.num_output_buffers ++;
            streamBufferList.push_back(streamBuffer);
            M_VERBOSE("added request for preview stream\n");
        }
    }

    request.output_buffers      = streamBufferList.data();
    request.frame_number        = *frameNumber;
    request.settings            = requestMetadata.getAndLock();
    request.input_buffer        = nullptr;

    // If there are no output buffers just do nothing
    // Without this an illegal zero output buffer request will be made
    if (request.num_output_buffers == 0){
        // Output buffers are full delay the next request
        // Without this wait at high CPU loads the loop will run away with CPU usage
        usleep(10000);
        return S_OK;
    }

    /* Return values (from hardware/camera3.h):
     *
     *  0:      On a successful start to processing the capture request
     *
     * -EINVAL: If the input is malformed (the settings are NULL when not
     *          allowed, invalid physical camera settings,
     *          there are 0 output buffers, etc) and capture processing
     *          cannot start. Failures during request processing should be
     *          handled by calling camera3_callback_ops_t.notify(). In case of
     *          this error, the framework will retain responsibility for the
     *          stream buffers' fences and the buffer handles; the HAL should
     *          not close the fences or return these buffers with
     *          process_capture_result.
     *
     * -ENODEV: If the camera device has encountered a serious error. After this
     *          error is returned, only the close() method can be successfully
     *          called by the framework.
     *
     */
    M_VERBOSE("Sending request for frame %d for camera %s for %d streams\n", *frameNumber, name, request.num_output_buffers);

    if (int status = pDevice->ops->process_capture_request(pDevice, &request))
    {

        //Another thread has already detected the fatal error, return since it has already been handled
        if(stopped) return 0;

        M_ERROR("Received Fatal error from camera: %s\n", name);
        switch (status){
            case -EINVAL :
                M_ERROR("Sending request %d, ErrorCode: -EINVAL\n", *frameNumber);
                break;
            case -ENODEV:
                M_ERROR("Sending request %d, ErrorCode: -ENODEV\n", *frameNumber);
                break;
            default:
                M_ERROR("Sending request %d, ErrorCode: %d\n", *frameNumber, status);
                break;
        }

        EStopCameraServer();
        return -EINVAL;
    }

    M_VERBOSE("finished sending request for frame %d for camera %s\n", *frameNumber, name);
    *frameNumber = *frameNumber+1;
    requestMetadata.unlock(request.settings);

    M_VERBOSE("returning from SendOneCaptureRequest for frame %d for camera %s\n", *frameNumber, name);

    return S_OK;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Main thread function to initiate the sending of capture requests to the camera module. Keeps on sending the capture requests
// to the camera module till a "stop message" is passed to this thread function
// -----------------------------------------------------------------------------------------------------------------------------
void* PerCameraMgr::ThreadIssueCaptureRequests()
{

    uint32_t frame_number = 0;
    char buf[16];
    sprintf(buf, "cam%d-request", cameraId);
    pthread_setname_np(pthread_self(), buf);

    M_VERBOSE("Entered thread: %s(tid: %lu)\n", buf, syscall(SYS_gettid));

    if (ConstructDefaultRequestSettings()){

        M_ERROR("Failed to construct request settings for camera: %s\n", name);

        EStopCameraServer();
    }

    while (!stopped && !EStopped)
    {

        /** This is an old TODO comment, I think SendOneCaptureRequest handles
          * this now, but leaving the comment here in case it misbahves and better
          * behavior is needed in the future
                    // if(!getNumClients() && !numNeededSnapshots){
                    //     //TODODODODODODOD THIS NEEDS TO BE A COND SLEEP
                    //     usleep(100000);
                    //     if(stopped || EStopped) break;
                    // }
        **/
        SendOneCaptureRequest(&frame_number);
    }

    // Stop message received. Inform about the last framenumber requested from the camera module. This in turn will be used
    // by the result thread to wait for this frame's image buffers to arrive.
    if(EStopped){
        M_WARN("Thread: %s request thread Received ESTOP\n", name);
    }else{
        // SendOneCaptureRequest increments frame_number after successfully
        // sending the last frame -- therefore, the last request which was
        // actually sent to hal3 was frame_number - 1
        lastResultFrameNumber = (frame_number -  1);
        M_DEBUG("------ Last request frame for %s: %d\n", name, frame_number);
    }

    M_VERBOSE("Leaving %s request thread\n", name);

    return NULL;
}

enum AECommandVals {
    SET_EXP_GAIN,
    SET_EXP,
    SET_GAIN,
    SET_FPS,
    START_AE,
    STOP_AE,
    SNAPSHOT,
    SNAPSHOT_NS,
    SET_SMALL_VENC_MBPS,
    SET_LARGE_VENC_MBPS,
    SET_MISP_VENC_MBPS,
    SET_MISP_AWB,
    SET_MISP_GAMMA
};
static const char* CmdStrings[] =
{
    "set_exp_gain",
    "set_exp",
    "set_gain",
    "set_fps",
    "start_ae",
    "stop_ae",
    "snapshot",
    "snapshot_no_save",
    "set_small_venc_mbps",
    "set_large_venc_mbps",
    "set_misp_venc_mbps",
    "set_misp_awb",
    "set_misp_gamma"
};

static int UpdatePipeJson(int channel, int int_format, int width, int height, int framerate)
{
    cJSON* json = pipe_server_get_info_json_ptr(channel);
    if(json == NULL){
        M_ERROR("got NULL pointer in %s\n", __FUNCTION__);
        return -1;
    }
    cJSON_AddStringToObject(json, "string_format", pipe_image_format_to_string(int_format));
    cJSON_AddNumberToObject(json, "int_format", int_format);
    cJSON_AddNumberToObject(json, "width", width);
    cJSON_AddNumberToObject(json, "height", height);
    cJSON_AddNumberToObject(json, "framerate", framerate);
    pipe_server_update_info(channel);
    return 0;
}

int PerCameraMgr::SetupPipes()
{
    if (!is_tof_sensor(configInfo.type)) {


        char cont_cmds[256];
        snprintf(cont_cmds, 255, "%s%s%s%s",
            EXPOSURE_CONTROL_COMMANDS,
            en_snapshot    ? ",snapshot,snapshot_no_save" : "",
            en_small_video ? ",set_small_venc_mbps"       : "",
            en_large_video ? ",set_large_venc_mbps"       : "");
        int flags = SERVER_FLAG_EN_CONTROL_PIPE;

        pipe_info_t info;
        strcpy(info.type       , "camera_image_metadata_t");
        strcpy(info.server_name, PROCESS_NAME);
        info.size_bytes = 128*1024*1024;

        // preview streams
        if(en_preview){

            // legacy naming without a suffix, just for
            // old black and white cameras like OV7251 tracking and stereo
            if( configInfo.en_raw_preview && \
                configInfo.bayer_fmt == BAYER_MONO)
            {
                strncpy(info.name, name, MODAL_PIPE_MAX_NAME_LEN-1);
                previewPipeGrey = pipe_server_get_next_available_channel();
                pipe_server_set_control_cb(previewPipeGrey, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
                pipe_server_create(previewPipeGrey, info, flags);
                pipe_server_set_available_control_commands(previewPipeGrey, cont_cmds);
                int fmt = IMAGE_FORMAT_RAW8;
                if(configInfo.camId2>=0){
                    fmt = IMAGE_FORMAT_STEREO_RAW8;
                }
                UpdatePipeJson(previewPipeGrey, fmt, pre_width, pre_height, fps);
            }

            // for color camera in raw mode and NOT stereo, advertize a raw bayered image
            // Also allow mono AR0144 to advertize raw
            if( configInfo.en_raw_preview && ((configInfo.bayer_fmt != BAYER_MONO) || (configInfo.type == SENSOR_AR0144)) && configInfo.camId2 < 0)
            {
                snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_bayer", name);
                previewPipeBayer = pipe_server_get_next_available_channel();
                pipe_server_set_control_cb(previewPipeBayer, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
                pipe_server_create(previewPipeBayer, info, flags);
                pipe_server_set_available_control_commands(previewPipeBayer, cont_cmds);
                UpdatePipeJson(previewPipeBayer, IMAGE_FORMAT_RAW10, pre_width, pre_height, fps);  //IMAGE_FORMAT_RAW12
            }

            // color cameras get a grey and color pipe when running RAW or through ISP
            if((configInfo.en_raw_preview && configInfo.bayer_fmt != BAYER_MONO && !en_misp) || pre_halfmt==HAL3_FMT_YUV)
            {
                snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_grey", name);
                previewPipeGrey = pipe_server_get_next_available_channel();
                pipe_server_set_control_cb(previewPipeGrey, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
                pipe_server_create(previewPipeGrey, info, flags);
                pipe_server_set_available_control_commands(previewPipeGrey, cont_cmds);
                UpdatePipeJson(previewPipeGrey, IMAGE_FORMAT_RAW8, pre_width, pre_height, fps);
            }

            if((configInfo.en_raw_preview && configInfo.bayer_fmt != BAYER_MONO && !en_misp && configInfo.camId2 < 0) \
                    || pre_halfmt==HAL3_FMT_YUV)
            {
                snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_color", name);
                previewPipeColor = pipe_server_get_next_available_channel();
                pipe_server_set_control_cb(previewPipeColor, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
                pipe_server_create(previewPipeColor, info, flags);
                pipe_server_set_available_control_commands(previewPipeColor, cont_cmds);
                if(configInfo.en_raw_preview){
                    // raw preview gets debayered to RGB
                    UpdatePipeJson(previewPipeColor, IMAGE_FORMAT_RGB, pre_width, pre_height, fps);
                }else{// yuv ISP generated preview comes out as NV12
                    UpdatePipeJson(previewPipeColor, IMAGE_FORMAT_NV12, pre_width, pre_height, fps);
                }
            }
        }


        // small encoded video stream for hires cameras
        if(en_small_video){
            snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_small_grey", name);
            smallVideoPipeGrey = pipe_server_get_next_available_channel();
            pipe_server_set_control_cb(smallVideoPipeGrey, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
            pipe_server_create(smallVideoPipeGrey, info, flags);
            pipe_server_set_available_control_commands(smallVideoPipeGrey, cont_cmds);
            UpdatePipeJson(smallVideoPipeGrey, IMAGE_FORMAT_RAW8, small_video_width, small_video_height, fps);

            snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_small_color", name);
            smallVideoPipeColor = pipe_server_get_next_available_channel();
            pipe_server_set_control_cb(smallVideoPipeColor, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
            pipe_server_create(smallVideoPipeColor, info, flags);
            pipe_server_set_available_control_commands(smallVideoPipeColor, cont_cmds);
            UpdatePipeJson(smallVideoPipeColor, IMAGE_FORMAT_NV12, small_video_width, small_video_height, fps);

            snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_small_encoded", name);
            smallVideoPipeEncoded = pipe_server_get_next_available_channel();
            pipe_server_set_control_cb(smallVideoPipeEncoded, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
            pipe_server_create(smallVideoPipeEncoded, info, flags);
            pipe_server_set_available_control_commands(smallVideoPipeEncoded, cont_cmds);
            int fmt = IMAGE_FORMAT_H264;
            if(configInfo.small_venc_config.mode == VENC_H265) fmt = IMAGE_FORMAT_H265;
            UpdatePipeJson(smallVideoPipeEncoded, fmt, small_video_width, small_video_height, fps);
        }

        // large encoded video stream for hires cameras
        if(en_large_video){
            snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_large_grey", name);
            largeVideoPipeGrey = pipe_server_get_next_available_channel();
            pipe_server_set_control_cb(largeVideoPipeGrey, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
            pipe_server_create(largeVideoPipeGrey, info, flags);
            pipe_server_set_available_control_commands(largeVideoPipeGrey, cont_cmds);
            (void) UpdatePipeJson(largeVideoPipeGrey, IMAGE_FORMAT_RAW8, large_video_width, large_video_height, fps);

            snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_large_color", name);
            largeVideoPipeColor = pipe_server_get_next_available_channel();
            pipe_server_set_control_cb(largeVideoPipeColor, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
            pipe_server_create(largeVideoPipeColor, info, flags);
            pipe_server_set_available_control_commands(largeVideoPipeColor, cont_cmds);
            (void) UpdatePipeJson(largeVideoPipeColor, IMAGE_FORMAT_NV12, large_video_width, large_video_height, fps);

            snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_large_encoded", name);
            largeVideoPipeEncoded = pipe_server_get_next_available_channel();
            pipe_server_set_control_cb(largeVideoPipeEncoded, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
            pipe_server_create(largeVideoPipeEncoded, info, flags);
            pipe_server_set_available_control_commands(largeVideoPipeEncoded, cont_cmds);
            int fmt = IMAGE_FORMAT_H264;
            if(configInfo.large_venc_config.mode == VENC_H265) fmt = IMAGE_FORMAT_H265;
            UpdatePipeJson(largeVideoPipeEncoded, fmt, large_video_width, large_video_height, fps);
        }

        if (en_misp){
            if (configInfo.bayer_fmt != BAYER_MONO) {
                snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_misp_color", name);
                mispPipeColor = pipe_server_get_next_available_channel();
                pipe_server_set_control_cb(mispPipeColor, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
                pipe_server_create(mispPipeColor, info, flags);
                pipe_server_set_available_control_commands(mispPipeColor, cont_cmds);
                UpdatePipeJson(mispPipeColor, IMAGE_FORMAT_NV12, misp_width, misp_height, fps);   //IMAGE_FORMAT_RGB
            }

            snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_misp_grey", name);
            mispPipeGrey = pipe_server_get_next_available_channel();
            pipe_server_set_control_cb(mispPipeGrey, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
            pipe_server_create(mispPipeGrey, info, flags);
            pipe_server_set_available_control_commands(mispPipeGrey, cont_cmds);
            UpdatePipeJson(mispPipeGrey, IMAGE_FORMAT_RAW8, misp_width, misp_height, fps);

            //normalized image is only enabled for specific sensors
            if (configInfo.type == SENSOR_AR0144 || configInfo.type == SENSOR_AR0144_12BIT || configInfo.type == SENSOR_OV9782) {
                snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_misp_norm", name);
                mispPipeNorm = pipe_server_get_next_available_channel();
                pipe_server_set_control_cb(mispPipeNorm, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
                pipe_server_create(mispPipeNorm, info, flags);
                pipe_server_set_available_control_commands(mispPipeNorm, cont_cmds);
                UpdatePipeJson(mispPipeNorm, IMAGE_FORMAT_RAW8, misp_width, misp_height, fps);
            }

            if (configInfo.en_misp_encoder){
                snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_misp_encoded", name);
                mispPipeEncoded = pipe_server_get_next_available_channel();
                pipe_server_set_control_cb(mispPipeEncoded, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
                pipe_server_create(mispPipeEncoded, info, flags);
                pipe_server_set_available_control_commands(mispPipeEncoded, cont_cmds);
                int fmt = IMAGE_FORMAT_H264;
                if(configInfo.misp_venc_config.mode == VENC_H265) fmt = IMAGE_FORMAT_H265;
                UpdatePipeJson(mispPipeEncoded, fmt, misp_width, misp_height, fps);
            }
        }


        if(en_snapshot){
            snprintf(info.name, MODAL_PIPE_MAX_NAME_LEN-1, "%s_snapshot", name);
            snapshotPipe = pipe_server_get_next_available_channel();
            pipe_server_set_control_cb(snapshotPipe, [](int ch, char * string, int bytes, void* context){((PerCameraMgr*)context)->HandleControlCmd(string);},this);
            pipe_server_create(snapshotPipe, info, flags);
            pipe_server_set_available_control_commands(snapshotPipe, cont_cmds);
            UpdatePipeJson(snapshotPipe, IMAGE_FORMAT_JPG, snap_width, snap_height, 0);
        }




    } else {

        tofPipeIR    = pipe_server_get_next_available_channel();
        tofPipeDepth = pipe_server_get_next_available_channel();
        tofPipeConf  = pipe_server_get_next_available_channel();
        tofPipePC    = pipe_server_get_next_available_channel();
        tofPipeFull  = pipe_server_get_next_available_channel();

        pipe_info_t IRInfo;
        pipe_info_t DepthInfo;
        pipe_info_t ConfInfo;
        pipe_info_t PCInfo;
        pipe_info_t FullInfo;

        sprintf(IRInfo.name,    "%s%s", name, "_ir");
        sprintf(DepthInfo.name, "%s%s", name, "_depth");
        sprintf(ConfInfo.name,  "%s%s", name, "_conf");
        sprintf(PCInfo.name,    "%s%s", name, "_pc");
        sprintf(FullInfo.name,  "%s%s", name, "");

        strcpy(IRInfo.type,     "camera_image_metadata_t");
        strcpy(DepthInfo.type,  "camera_image_metadata_t");
        strcpy(ConfInfo.type,   "camera_image_metadata_t");
        strcpy(PCInfo.type,     "point_cloud_metadata_t");
        strcpy(FullInfo.type,   "tof2_data_t");

        strcpy(IRInfo.server_name,    PROCESS_NAME);
        strcpy(DepthInfo.server_name, PROCESS_NAME);
        strcpy(ConfInfo.server_name,  PROCESS_NAME);
        strcpy(PCInfo.server_name,    PROCESS_NAME);
        strcpy(FullInfo.server_name,  PROCESS_NAME);

        IRInfo.size_bytes    =    1024*1024;
        DepthInfo.size_bytes =    1024*1024;
        ConfInfo.size_bytes  =    1024*1024;
        PCInfo.size_bytes    = 32*1024*1024;
        FullInfo.size_bytes  = 32*1024*1024;

        int flags = 0;
        pipe_server_create(tofPipeIR,    IRInfo,    flags);
        pipe_server_create(tofPipeDepth, DepthInfo, flags);
        pipe_server_create(tofPipeConf,  ConfInfo,  flags);
        pipe_server_create(tofPipePC,    PCInfo,    flags);
        pipe_server_create(tofPipeFull,  FullInfo,  flags);

        UpdatePipeJson(tofPipeIR,    IMAGE_FORMAT_RAW8, 224, 172, fps);
        UpdatePipeJson(tofPipeDepth, IMAGE_FORMAT_RAW8, 224, 172, fps);
        UpdatePipeJson(tofPipeConf,  IMAGE_FORMAT_RAW8, 224, 172, fps);

    }
    return S_OK;
}


static std::vector<std::string> split_string(std::string input)
{
    std::vector<std::string> result;
    const char * str = input.c_str();
    const char c     = ' ';

    do
    {
        const char *begin = str;

        while(*str != c && *str)
            str++;

        result.push_back(std::string(begin, str));
    } while (*str++ != 0);

    return result;
}

void PerCameraMgr::HandleControlCmd(char* cmd)
{

    const float MIN_EXP  = ((float)configInfo.ae_msv_info.exposure_min_us)/1000;
    const float MAX_EXP  = ((float)configInfo.ae_msv_info.exposure_max_us)/1000;
    const int MIN_GAIN = configInfo.ae_msv_info.gain_min;
    const int MAX_GAIN = configInfo.ae_msv_info.gain_max;

    std::vector<std::string> cmd_strings = split_string(cmd);
    /*
    for (auto s : ss){
        M_PRINT("%s\n",s.c_str());
    }*/

    /**************************
     *
     * SET Exposure and Gain
     *
     */
    if(strncmp(cmd, CmdStrings[SET_EXP_GAIN], strlen(CmdStrings[SET_EXP_GAIN])) == 0){

        char buffer[strlen(CmdStrings[SET_EXP_GAIN])+1];
        float exp = -1.0;
        int gain = -1;

        if(sscanf(cmd, "%s %f %d", buffer, &exp, &gain) == 3){
            if(exp < MIN_EXP || exp > MAX_EXP){
                M_ERROR("Invalid Control Pipe Exposure: %f,\n\tShould be between %f and %f\n", exp, MIN_EXP, MAX_EXP);
            } else if(gain < MIN_GAIN || gain > MAX_GAIN){
                M_ERROR("Invalid Control Pipe Gain: %d,\n\tShould be between %d and %d\n", gain, MIN_GAIN, MAX_GAIN);
            } else {
                pthread_mutex_lock(&aeMutex);

                if(ae_mode != AE_OFF) {
                    ae_mode = AE_OFF;
                    ConstructDefaultRequestSettings();

                    if(otherMgr){
                        otherMgr->ae_mode = AE_OFF;
                        otherMgr->ConstructDefaultRequestSettings();
                    }
                }

                M_DEBUG("Camera: %s Received new exp/gain values: %6.3f(ms) %d\n", name, exp, gain);

                setExposure = exp*1000000;
                setGain =     gain;

                if(otherMgr){
                    otherMgr->setExposure = exp*1000000;
                    otherMgr->setGain =     gain;
                }

                pthread_mutex_unlock(&aeMutex);
            }
        } else {
            M_ERROR("Camera: %s failed to get valid exposure/gain values from control pipe\n\tShould follow format: \"%s 25 350\"\n",
             name, CmdStrings[SET_EXP_GAIN]);
        }

    } else
    /**************************
     *
     * SET Exposure
     *
     */
    if(strncmp(cmd, CmdStrings[SET_EXP], strlen(CmdStrings[SET_EXP])) == 0){

        char buffer[strlen(CmdStrings[SET_EXP])+1];
        float exp = -1.0;

        if(sscanf(cmd, "%s %f", buffer, &exp) == 2){
            if(exp < MIN_EXP || exp > MAX_EXP){
                M_ERROR("Invalid Control Pipe Exposure: %f,\n\tShould be between %f and %f\n", exp, MIN_EXP, MAX_EXP);
            } else {
                pthread_mutex_lock(&aeMutex);

                if(ae_mode != AE_OFF) {
                    ae_mode = AE_OFF;
                    ConstructDefaultRequestSettings();

                    if(otherMgr){
                        otherMgr->ae_mode = AE_OFF;
                        otherMgr->ConstructDefaultRequestSettings();
                    }
                }

                M_DEBUG("Camera: %s Received new exp value: %6.3f(ms)\n", name, exp);
                setExposure = exp*1000000;

                if(otherMgr){
                    otherMgr->setExposure = exp*1000000;
                }

                pthread_mutex_unlock(&aeMutex);
            }
        } else {
            M_ERROR("Camera: %s failed to get valid exposure value from control pipe\n\tShould follow format: \"%s 25\"\n",
            name, CmdStrings[SET_EXP]);
        }
    } else
    /**************************
     *
     * SET Gain
     *
     */
    if(strncmp(cmd, CmdStrings[SET_GAIN], strlen(CmdStrings[SET_GAIN])) == 0){

        char buffer[strlen(CmdStrings[SET_GAIN])+1];
        int gain = -1;

        if(sscanf(cmd, "%s %d", buffer, &gain) == 2){
            if(gain < MIN_GAIN || gain > MAX_GAIN){
                M_ERROR("Invalid Control Pipe Gain: %d,\n\tShould be between %d and %d\n", gain, MIN_GAIN, MAX_GAIN);
            } else {
                pthread_mutex_lock(&aeMutex);

                if(ae_mode != AE_OFF) {
                    ae_mode = AE_OFF;
                    ConstructDefaultRequestSettings();

                    if(otherMgr){
                        otherMgr->ae_mode = AE_OFF;
                        otherMgr->ConstructDefaultRequestSettings();
                    }
                }

                M_DEBUG("Camera: %s Received new gain value: %d\n", name, gain);
                setGain = gain;

                if(otherMgr){
                    otherMgr->setGain = gain;
                }

                pthread_mutex_unlock(&aeMutex);
            }
        } else {
            M_ERROR("Camera: %s failed to get valid gain value from control pipe\n\tShould follow format: \"%s 350\"\n",
            name, CmdStrings[SET_GAIN]);
        }
    } else
    /**************************
     *
     * START Auto Exposure
     *
     */
    if(strncmp(cmd, CmdStrings[START_AE], strlen(CmdStrings[START_AE])) == 0){

        pthread_mutex_lock(&aeMutex);

        if(ae_mode != configInfo.ae_mode) {
            ae_mode = configInfo.ae_mode;
            ConstructDefaultRequestSettings();

            if(otherMgr){
                otherMgr->ae_mode = configInfo.ae_mode;
                otherMgr->ConstructDefaultRequestSettings();
            }

            M_DEBUG("Camera: %s starting to use Auto Exposure\n", name);
        }
        pthread_mutex_unlock(&aeMutex);

    } else
    /**************************
     *
     * STOP Auto Exposure
     *
     */
    if(strncmp(cmd, CmdStrings[STOP_AE], strlen(CmdStrings[STOP_AE])) == 0){

        pthread_mutex_lock(&aeMutex);

        if(ae_mode != AE_OFF) {
            ae_mode = AE_OFF;
            ConstructDefaultRequestSettings();

            if(otherMgr){
                otherMgr->ae_mode = AE_OFF;
                otherMgr->ConstructDefaultRequestSettings();
            }
            M_DEBUG("Camera: %s ceasing to use Auto Exposure\n", name);
        }
        pthread_mutex_unlock(&aeMutex);

    } else

    /**************************
     *
     * Take snapshot without saving
     *
     */
    if(strncmp(cmd, CmdStrings[SNAPSHOT_NS], strlen(CmdStrings[SNAPSHOT_NS])) == 0 ||
       strncmp(cmd, "snapshot-no-save",      strlen(CmdStrings[SNAPSHOT_NS])) == 0){
        if(!en_snapshot){
            M_ERROR("Camera: %s declining to take snapshot, mode not enabled\n", name);
        }
        else{
            M_PRINT("Camera: %s taking snapshot for pipe only (not saving it)\n", name);
            numNeededSnapshots++;
        }
    } else

    /**************************
     *
     * Take snapshot to save with filename
     *
     */
    if(strncmp(cmd, CmdStrings[SNAPSHOT], strlen(CmdStrings[SNAPSHOT])) == 0){
        if(!en_snapshot){
            M_ERROR("Camera: %s declining to take snapshot, mode not enabled\n", name);
        }
        else{
            char buffer[strlen(CmdStrings[SET_EXP_GAIN])+1];
            char *filename = (char *)malloc(256);

            if(sscanf(cmd, "%s %s", buffer, filename) != 2){
                // We weren't given a proper file, generate one from the date and time
                // fetch time to use for video filename
                time_t rawtime = time(NULL);

                // add millis to filename to prevent snapshots taken close together from
                // overwriting each other
                auto now = std::chrono::system_clock::now();
                auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;

                char time_formatted[100];
                strftime(time_formatted, sizeof(time_formatted)-1, "%Y-%m-%d_%T", localtime(&rawtime));

                // construct the video filename
                sprintf(filename,"/data/snapshots/%s-%s.%03ld.jpg", name, time_formatted, millis.count());

            }

            M_PRINT("Camera: %s taking snapshot (destination: %s)\n", name, filename);

            snapshotQueue.push(filename);
            numNeededSnapshots++;
        }
    } else

    if(strncmp(cmd, CmdStrings[SET_LARGE_VENC_MBPS], strlen(CmdStrings[SET_LARGE_VENC_MBPS])) == 0){
        char buffer[strlen(CmdStrings[SET_LARGE_VENC_MBPS])+1];
        float venc_mbps = 0.0;

        if(sscanf(cmd, "%s %f", buffer, &venc_mbps) == 2){
            //TODO: check desired bitrate for sanity
            if (!en_large_video){
                M_ERROR("Camera: %s declining to update large encoder bitrate, encoder not enabled\n", name);
            }
            else{
                M_PRINT("Camera: %s setting large encoder bitrate to %.2f mbps\n", name,venc_mbps);
                pVideoEncoderLarge->SetTargetBitrate(venc_mbps*1000000);
            }
        } else {
            M_ERROR("Camera: %s failed to get valid encoder bitrate value from control pipe\n\tShould follow format: \"%s 3.0\"\n",
            name, CmdStrings[SET_LARGE_VENC_MBPS]);
        }
    } else

    if(strncmp(cmd, CmdStrings[SET_SMALL_VENC_MBPS], strlen(CmdStrings[SET_SMALL_VENC_MBPS])) == 0){
        char buffer[strlen(CmdStrings[SET_SMALL_VENC_MBPS])+1];
        float venc_mbps = 0.0;

        if(sscanf(cmd, "%s %f", buffer, &venc_mbps) == 2){
            //TODO: check desired bitrate for sanity
            if (!en_small_video){
                M_ERROR("Camera: %s declining to update small encoder bitrate, encoder not enabled\n", name);
            }
            else{
                M_PRINT("Camera: %s setting small encoder bitrate to %.2f mbps\n", name,venc_mbps);
                pVideoEncoderSmall->SetTargetBitrate(venc_mbps*1000000);
            }
        } else {
            M_ERROR("Camera: %s failed to get valid encoder bitrate value from control pipe\n\tShould follow format: \"%s 3.0\"\n",
            name, CmdStrings[SET_SMALL_VENC_MBPS]);
        }
    } else

    if (cmd_strings[0] == CmdStrings[SET_FPS] && cmd_strings.size() == 2)
    {
        float new_fps = std::stof(cmd_strings[1]);
        fps = new_fps;  //the new fps setting will be used in the future frame request

        //update the nPframes setting in video encoders - should we do this automatically or not?
        /*
        if (pVideoEncoderSmall){
            pVideoEncoderSmall->SetIntraPeriod(configInfo.small_venc_config.nPframes * fps / configInfo.fps);
        }

        if (pVideoEncoderSmall){
            pVideoEncoderLarge->SetIntraPeriod(configInfo.large_venc_config.nPframes * fps / configInfo.fps);
        }
        */
        if (pVideoEncoderMisp){
            pVideoEncoderMisp->SetIntraPeriod(configInfo.misp_venc_config.nPframes * fps / configInfo.fps);
        }

        M_PRINT("Setting %s FPS to %f\n",name, fps);
    } else

    if(strncmp(cmd, CmdStrings[SET_MISP_VENC_MBPS], strlen(CmdStrings[SET_MISP_VENC_MBPS])) == 0){
        char buffer[strlen(CmdStrings[SET_MISP_VENC_MBPS])+1];
        float venc_mbps = 0.0;

        if(sscanf(cmd, "%s %f", buffer, &venc_mbps) == 2){
            //TODO: check desired bitrate for sanity
            if (!configInfo.en_misp || !configInfo.en_misp_encoder || !pVideoEncoderMisp){
                M_ERROR("Camera: %s declining to update misp encoder bitrate, encoder not enabled\n", name);
            }
            else{
                M_PRINT("Camera: %s setting misp encoder bitrate to %.2f mbps\n", name,venc_mbps);
                pVideoEncoderMisp->SetTargetBitrate(venc_mbps*1000000);
            }
        } else {
            M_ERROR("Camera: %s failed to get valid encoder bitrate value from control pipe\n\tShould follow format: \"%s 3.0\"\n",
            name, CmdStrings[SET_MISP_VENC_MBPS]);
        }
    } else

    if(strncmp(cmd, CmdStrings[SET_MISP_VENC_MBPS], strlen(CmdStrings[SET_MISP_VENC_MBPS])) == 0){
        char buffer[strlen(CmdStrings[SET_MISP_VENC_MBPS])+1];
        float venc_mbps = 0.0;

        if(sscanf(cmd, "%s %f", buffer, &venc_mbps) == 2){
            //TODO: check desired bitrate for sanity
            if (!configInfo.en_misp || !configInfo.en_misp_encoder || !pVideoEncoderMisp){
                M_ERROR("Camera: %s declining to update misp encoder bitrate, encoder not enabled\n", name);
            }
            else{
                M_PRINT("Camera: %s setting misp encoder bitrate to %.2f mbps\n", name,venc_mbps);
                pVideoEncoderMisp->SetTargetBitrate(venc_mbps*1000000);
            }
        } else {
            M_ERROR("Camera: %s failed to get valid encoder bitrate value from control pipe\n\tShould follow format: \"%s 3.0\"\n",
            name, CmdStrings[SET_MISP_VENC_MBPS]);
        }
    } else


    if(strncmp(cmd, CmdStrings[SET_MISP_AWB], strlen(CmdStrings[SET_MISP_AWB])) == 0){
        if (misp){
            char a[64];
            char b[64];
            float rgb_gains[3] = {1.0, 1.0, 1.0};
            if(sscanf(cmd, "%s %s %f %f %f", a, b, &rgb_gains[0], &rgb_gains[1], &rgb_gains[2]) > 0){
                if (strncmp("manual",b,6) == 0){
                    M_PRINT("setting misp awb manual gains %f %f %f\n",rgb_gains[0], rgb_gains[1], rgb_gains[2]);
                    misp->SetAwbManualGains(rgb_gains[0], rgb_gains[1], rgb_gains[2]);
                }
                else if (strncmp("auto",b,4) == 0){
                    M_PRINT("setting misp awb to auto\n");
                    misp->SetAwbManualGains(0, 0, 0);
                }
                else {
                    M_ERROR("Unknown misp awb mode");
                }

            }
            else {
                M_ERROR("cmd %s failed: %s\n", CmdStrings[SET_MISP_AWB], cmd);
            }
        }
        else{
            M_ERROR("MISP is not enabled for cmd %s\n",cmd);
        }
    } else

    if(strncmp(cmd, CmdStrings[SET_MISP_GAMMA], strlen(CmdStrings[SET_MISP_GAMMA])) == 0){
        if (misp){
            char a[64];
            float gamma = 1.0;

            if(sscanf(cmd, "%s %f", a, &gamma) > 0){
                M_PRINT("setting misp gamma to %f\n",gamma);
                misp->SetGammaCorrection(gamma);
            }
            else {
                M_ERROR("unable to parse command: %s\n",cmd);
            }
        }
        else{
            M_ERROR("MISP is not enabled for cmd %s\n",cmd);
        }
    } else
    
    if (misp && (misp->HandleControlCommand(cmd) == 0))
    {

    }


    /**************************
     *
     *  ¯\_(ツ)_/¯
     *
     */
    else {
        M_ERROR("Camera: %s got unknown Command: %s\n", name, cmd);
    }
}



void PerCameraMgr::EStop(){

    EStopped = true;
    stopped = true;
    pthread_cond_broadcast(&stereoCond);
    pthread_cond_broadcast(&resultCond);

    if(partnerMode == MODE_STEREO_MASTER){
        otherMgr->EStop();
    }
}


