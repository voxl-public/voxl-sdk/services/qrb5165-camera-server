/*******************************************************************************
 * Copyright 2025 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with hardware devices provided by
 *    ModalAI® Inc. Reverse engineering or copying this code for use on hardware 
 *    not manufactured by ModalAI is not permitted.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

//#define __CL_ENABLE_EXCEPTIONS
//#include <CL/cl.h>
//#include <CL/cl.hpp>
#include <CL/cl_ext.h>
//#include <CL/cl_ext_qcom.h>

#include "misp.hpp"
#include <time.h>

#include <iostream>
#include <fstream>
#include <string>

#include <modalcv/common.h>

#include "kernels.h"

#define CL_CONTEXT_PERF_HINT_QCOM                   0x40C2
#define CL_PERF_HINT_HIGH_QCOM                      0x40C3
#define CL_PERF_HINT_NORMAL_QCOM                    0x40C4
#define CL_PERF_HINT_LOW_QCOM                       0x40C5


#define CL_CONTEXT_PRIORITY_HINT_QCOM               0x40C9
#define CL_PRIORITY_HINT_NONE_QCOM                  0x0000
#define CL_PRIORITY_HINT_HIGH_QCOM                  0x40CA
#define CL_PRIORITY_HINT_NORMAL_QCOM                0x40CB
#define CL_PRIORITY_HINT_LOW_QCOM                   0x40CC

#define CL_QCOM_BAYER                               0x414E
#define CL_QCOM_UNORM_MIPI10                        0x4159
#define CL_QCOM_UNORM_MIPI12                        0x415A
#define CL_QCOM_NV12                                0x4133
#define CL_QCOM_NV12_Y                              0x4134
#define CL_QCOM_NV12_UV                             0x4135

#define CL_QCOM_UNSIGNED_MIPI10                     0x415B

#define CL_MEM_OBJECT_WEIGHT_IMAGE_QCOM                         0x4110
#define CL_DEVICE_HOF_MAX_NUM_PHASES_QCOM                       0x4111
#define CL_DEVICE_HOF_MAX_FILTER_SIZE_X_QCOM                    0x4112
#define CL_DEVICE_HOF_MAX_FILTER_SIZE_Y_QCOM                    0x4113
#define CL_DEVICE_BLOCK_MATCHING_MAX_REGION_SIZE_X_QCOM         0x4114
#define CL_DEVICE_BLOCK_MATCHING_MAX_REGION_SIZE_Y_QCOM         0x4115


/*
static float gaussian_filter_3x3[9] = {
    0.057118, 0.124758, 0.057118, 
    0.124758, 0.272496, 0.124758, 
    0.057118, 0.124758, 0.057118, 
};

static float gaussian_filter_5x5[25] = {
    0.005008, 0.017300, 0.026151, 0.017300, 0.005008, 
    0.017300, 0.059761, 0.090339, 0.059761, 0.017300, 
    0.026151, 0.090339, 0.136565, 0.090339, 0.026151, 
    0.017300, 0.059761, 0.090339, 0.059761, 0.017300, 
    0.005008, 0.017300, 0.026151, 0.017300, 0.005008, 
};

static float gaussian_filter_7x7[49] = {
    0.000841, 0.003010, 0.006471, 0.008351, 0.006471, 0.003010, 0.000841, 
    0.003010, 0.010778, 0.023169, 0.029902, 0.023169, 0.010778, 0.003010, 
    0.006471, 0.023169, 0.049806, 0.064280, 0.049806, 0.023169, 0.006471, 
    0.008351, 0.029902, 0.064280, 0.082959, 0.064280, 0.029902, 0.008351, 
    0.006471, 0.023169, 0.049806, 0.064280, 0.049806, 0.023169, 0.006471, 
    0.003010, 0.010778, 0.023169, 0.029902, 0.023169, 0.010778, 0.003010, 
    0.000841, 0.003010, 0.006471, 0.008351, 0.006471, 0.003010, 0.000841, 
};
*/

static float gaussian_filter_9x9[81] = {
    0.000220, 0.000739, 0.001756, 0.002951, 0.003508, 0.002951, 0.001756, 0.000739, 0.000220, 
    0.000739, 0.002482, 0.005895, 0.009905, 0.011776, 0.009905, 0.005895, 0.002482, 0.000739, 
    0.001756, 0.005895, 0.014000, 0.023526, 0.027969, 0.023526, 0.014000, 0.005895, 0.001756, 
    0.002951, 0.009905, 0.023526, 0.039533, 0.047000, 0.039533, 0.023526, 0.009905, 0.002951, 
    0.003508, 0.011776, 0.027969, 0.047000, 0.055877, 0.047000, 0.027969, 0.011776, 0.003508, 
    0.002951, 0.009905, 0.023526, 0.039533, 0.047000, 0.039533, 0.023526, 0.009905, 0.002951, 
    0.001756, 0.005895, 0.014000, 0.023526, 0.027969, 0.023526, 0.014000, 0.005895, 0.001756, 
    0.000739, 0.002482, 0.005895, 0.009905, 0.011776, 0.009905, 0.005895, 0.002482, 0.000739, 
    0.000220, 0.000739, 0.001756, 0.002951, 0.003508, 0.002951, 0.001756, 0.000739, 0.000220, 
};


std::vector<cl::Platform> Misp::platforms;
std::vector<cl::Device>   Misp::devices;
cl::Device                Misp::gpu_device;
cl::Context               Misp::gpu_context;
std::string               Misp::device_name;
std::string               Misp::driver_version;
std::string               Misp::device_extensions;
ProgramMap                Misp::cl_programs;  //static map for caching programs
int                       Misp::gpu_initialized = 0;

Misp::Misp()
{

}

Misp::~Misp()
{

}

int Misp::Init(uint32_t width, uint32_t height)
{
    std::cout << "MISP Initializing!!!" << std::endl;
    if (gpu_initialized==0){
        try {
            // Get list of OpenCL platforms.
            cl::Platform::get(&platforms);

            if (platforms.empty()) {
                std::cerr << "OpenCL platforms not found." << std::endl;
                return -1;
            }

            std::cout << " Detected " << platforms.size() << " platform(s)" << std::endl;

            // Get first available GPU device as there is only one.
            platforms[0].getDevices(CL_DEVICE_TYPE_GPU, &devices);

            std::cout << " Detected " << devices.size() << " GPU device(s)" << std::endl;
            if (devices.size() <  1){
                std::cerr << "Failed to detect any GPUs" <<std::endl;
                return -1;
            }

            cl_context_properties properties[] = {CL_CONTEXT_PERF_HINT_QCOM, CL_PERF_HINT_HIGH_QCOM, CL_CONTEXT_PRIORITY_HINT_QCOM, CL_PRIORITY_HINT_HIGH_QCOM, 0};

            gpu_device        = devices[0];
            gpu_context       = cl::Context(devices[0],properties);
            device_name       = gpu_device.getInfo<CL_DEVICE_NAME>();
            driver_version    = gpu_device.getInfo<CL_DRIVER_VERSION>();
            device_extensions = gpu_device.getInfo<CL_DEVICE_EXTENSIONS>();

            //std::cout << " " << device_name       << std::endl;
            //std::cout << " " << driver_version    << std::endl;
            //std::cout << " Supported Extensions: " << device_extensions << std::endl << std::endl;

    /*
            uint32_t max_filter_size_x = 0;
            size_t max_filter_size_x_param_size = 0;
            //clGetDeviceInfo(gpu_device(), CL_DEVICE_HOF_MAX_FILTER_SIZE_X_QCOM, sizeof(max_filter_size_x), &max_filter_size_x, &max_filter_size_x_param_size);
            //std::cout << " Max Box Filter Size: " << max_filter_size_x <<" "<<max_filter_size_x_param_size<< std::endl;

            std::cout << " Max Box Filter Size: " << gpu_device.getInfo<CL_DEVICE_HOF_MAX_FILTER_SIZE_X_QCOM>() << std::endl;
    */
        } catch (const cl::Error &err) {
            std::cerr
                << "OpenCL error: "
                << err.what() << "(" << err.err() << ")"
                << std::endl;
            return -1;
        }

        gpu_initialized = 1;
    }

    // Create command queue
    queue = cl::CommandQueue(gpu_context, gpu_device);

    //load kernel(s)
    std::string kernel_file;
    std::string kernel_name;
    int kernel_load_ret;

    kernel_name     = "debayer_to_yuv";
    kernel_load_ret = LoadKernelFromString(debayer_to_yuv_str, kernel_name, kernel, "-cl-fast-relaxed-math");
    if (kernel_load_ret){
        std::cout << "Failed to load the kernel:  " << kernel_name <<std::endl;
        return -1;
    }

    kernel_name = "bayer_downscale_blur_to_yuv_color";
    kernel_load_ret = LoadKernelFromString(bayer_downscale_blur_to_yuv_color_str, kernel_name, kernel4);
    if (kernel_load_ret){
        std::cout << "Failed to load the kernel:  " << kernel_name <<std::endl;
        return -1;
    }

    kernel_name = "bayer_to_yuv_color_norm";
    kernel_load_ret = LoadKernelFromString(bayer_to_yuv_color_norm_str, kernel_name,  kernel5);
    if (kernel_load_ret){
        std::cout << "Failed to load the kernel:  " << kernel_name <<std::endl;
        return -1;
    }

    kernel_name = "blur_image";
    //kernel_file = "/usr/share/modalai/voxl-camera-server/opencl/kernels.cl";
    //kernel_load_ret = LoadKernelFromFile(kernel_file, kernel_name, blur_kernel);
    kernel_load_ret = LoadKernelFromString(blur_image_str, kernel_name, blur_kernel);
    if (kernel_load_ret){
        std::cout << "Failed to load the kernel:  " << kernel_name <<std::endl;
        return -1;
    }

    kernel_name = "ar0144_8bit_lsc";
    kernel_load_ret = LoadKernelFromString(ar0144_lsc_str, kernel_name, ar0144_8bit_lsc_kernel);
    if (kernel_load_ret){
        std::cout << "Failed to load the kernel:  " << kernel_name <<std::endl;
        return -1;
    }

    kernel_name = "ar0144_10bit_lsc";
    kernel_load_ret = LoadKernelFromString(ar0144_lsc_str, kernel_name, ar0144_10bit_lsc_kernel);
    if (kernel_load_ret){
        std::cout << "Failed to load the kernel:  " << kernel_name <<std::endl;
        return -1;
    }

    kernel_name = "ar0144_12bit_lsc";
    kernel_load_ret = LoadKernelFromString(ar0144_lsc_str, kernel_name, ar0144_12bit_lsc_kernel);
    if (kernel_load_ret){
        std::cout << "Failed to load the kernel:  " << kernel_name <<std::endl;
        return -1;
    }

    kernel_name = "raw8_to_raw8_norm";
    kernel_load_ret = LoadKernelFromString(bayer_to_yuv_color_norm_str, kernel_name, raw8_to_raw8_norm_kernel);
    if (kernel_load_ret){
        std::cout << "Failed to load the kernel:  " << kernel_name <<std::endl;
        return -1;
    }

    kernel_name = "raw8_downscale_blur_to_raw8";
    kernel_load_ret = LoadKernelFromString(bayer_downscale_blur_to_yuv_color_str, kernel_name, raw8_downscale_blur_to_raw8_kernel);
    if (kernel_load_ret){
        std::cout << "Failed to load the kernel:  " << kernel_name <<std::endl;
        return -1;
    }

    
    kernel_name = "raw10_12_to_mono8_norm";
    kernel_load_ret = LoadKernelFromString(bayer_to_yuv_color_norm_str, kernel_name, raw10_12_to_mono8_norm_kernel);
    if (kernel_load_ret){
        std::cout << "Failed to load the kernel:  " << kernel_name <<std::endl;
        return -1;
    }


    kernel_name = "transform";
    kernel_load_ret = LoadKernelFromString(transform_str, kernel_name, transform_kernel);
    if (kernel_load_ret){
        std::cout << "Failed to load the kernel:  " << kernel_name <<std::endl;
        return -1;
    }
    
    //GRALLOC_USAGE_HW_COMPOSER | | GRALLOC_USAGE_PRIVATE_UNCACHED /*ENCODER_USAGE | GRALLOC_USAGE_PRIVATE_UNCACHED*/
    //GRALLOC_USAGE_HW_TEXTURE 
    if (bufferAllocateBuffers(scratch1_buffer_group,
                                  2,
                                  width/16,
                                  height/16,
                                  HAL_PIXEL_FORMAT_YCbCr_420_888,
                                  GRALLOC_USAGE_PRIVATE_UNCACHED
                                  )) {
        M_ERROR("Failed to allocate misp buffer\n");
        return -1;
    }

    //get buffer for output output image
    buffer_handle_t* scratch_buffer_handle = bufferPop(scratch1_buffer_group);
    if (scratch_buffer_handle == NULL){
        M_ERROR("could not get a buffer\n");
        return -1;
    }

    scratch_buffer = bufferGetBufferInfo(&scratch1_buffer_group, scratch_buffer_handle);
    if (scratch_buffer == NULL){
        M_ERROR("could not get scratch_buffer\n");
        return -1;
    }

    //M_PRINT("allocated scratch buffer\n");


    buffer_handle_t* scratch_buffer2_handle = bufferPop(scratch1_buffer_group);
    if (scratch_buffer2_handle == NULL){
        M_ERROR("could not get a buffer\n");
        return -1;
    }

    scratch_buffer2 = bufferGetBufferInfo(&scratch1_buffer_group, scratch_buffer2_handle);
    if (scratch_buffer2 == NULL){
        M_ERROR("could not get scratch_buffer2\n");
        return -1;
    }

    memset(scratch_buffer->vaddress,  127, scratch_buffer->size);
    memset(scratch_buffer2->vaddress, 127, scratch_buffer2->size);

    //M_PRINT("allocated scratch buffer2\n");


    //GRALLOC_USAGE_SW_READ_OFTEN  /*GRALLOC_USAGE_HW_COMPOSER | GRALLOC_USAGE_HW_TEXTURE ENCODER_USAGE | GRALLOC_USAGE_PRIVATE_UNCACHED*/
    if (bufferAllocateBuffers(scratch2_buffer_group,
                                  1,
                                  width,
                                  height,
                                  HAL_PIXEL_FORMAT_YCbCr_420_888,
                                  GRALLOC_USAGE_HW_RENDER | GRALLOC_USAGE_PRIVATE_UNCACHED /* if uncached is not speicified, there are artifacts */
                                  )) {
        M_ERROR("Failed to allocate misp buffer\n");
        return -1;
    }

    //get buffer for output output image
    buffer_handle_t* scratch_full_buffer_handle = bufferPop(scratch2_buffer_group);
    if (scratch_full_buffer_handle == NULL){
        M_ERROR("could not get a buffer\n");
        return -1;
    }

    scratch_full_buffer = bufferGetBufferInfo(&scratch2_buffer_group, scratch_full_buffer_handle);
    if (scratch_full_buffer == NULL){
        M_ERROR("could not get full scratch_buffer\n");
        return -1;
    }

    //M_PRINT("allocated full scratch buffer\n");


    return 0;
}


int Misp::NormalizeImage(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_rggb, uint32_t src_bit_width,
                void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride)
{
    cl_int     err_code;
    cl::Memory src_mem_cl;
    cl::Memory dst_mem_cl;
    cl::Memory scratch_mem_cl;
    cl::Memory blurred_mem_cl;
    cl::Memory weight_mem_cl;

    int downscale_factor = 16;
    //box size is larger than downscale factor so we actually also filter the output image
    uint32_t scratch_width  = dst_width / downscale_factor;
    uint32_t scratch_height = dst_height / downscale_factor;

    if (src_bit_width == 8){
        MapIonBufferToGpuImage2dY8(     gpu_context, src_host_ptr, src_fd, src_size, src_width, src_height, src_line_stride, &src_mem_cl, &err_code);
    } else if (src_bit_width == 10){
        MapIonBufferToGpuImage2dBayer10(gpu_context, src_host_ptr, src_fd, src_size, src_width, src_height, src_line_stride, &src_mem_cl, &err_code);
    } else if (src_bit_width == 12){
        MapIonBufferToGpuImage2dBayer12(gpu_context, src_host_ptr, src_fd, src_size, src_width, src_height, src_line_stride, &src_mem_cl, &err_code);
    } else {
        std::cerr << "invalid src bit width: " << src_bit_width <<std::endl;
        return -1;
    }
    
    //map the output YUV buffer as regular buffer (not YUV multi-plane opencl type)
    MapIonBufferToGpu(gpu_context, dst_host_ptr, dst_fd, dst_size, &dst_mem_cl, &err_code);

    //map the scratch buffer as raw buffer
    MapIonBufferToGpu(gpu_context, scratch_buffer->vaddress, scratch_buffer->fd, scratch_buffer->size, &scratch_mem_cl, &err_code);
    
     cl::Memory scratch2_mem_cl;
    MapIonBufferToGpuImage2dY8(gpu_context, scratch_buffer->vaddress, scratch_buffer->fd, scratch_buffer->size, scratch_width, scratch_height, scratch_buffer->stride, &scratch2_mem_cl, &err_code);
    CreateGaussianKernelImage(gpu_context, &weight_mem_cl, &err_code);
    MapIonBufferToGpu(gpu_context, scratch_buffer2->vaddress, scratch_buffer2->fd, scratch_buffer2->size, &blurred_mem_cl, &err_code);

    
    cl::Memory blurred2_mem_cl;
    MapIonBufferToGpuImage2dY8(gpu_context, scratch_buffer2->vaddress, scratch_buffer2->fd, scratch_buffer2->size, scratch_width, scratch_height, scratch_buffer2->stride, &blurred2_mem_cl, &err_code);
    
    //cl::Memory y_plane;
    //cl::Memory uv_plane;
    //MapIonBufferToGpuImage2dYuv(gpu_context, scratch_buffer->vaddress, scratch_buffer->fd, scratch_buffer->size, scratch_width, scratch_height, scratch_buffer->stride, &scratch2_mem_cl, &y_plane, &uv_plane, &err_code);

    


    cl::Sampler sampler(
            gpu_context,
            CL_FALSE,
            CL_ADDRESS_CLAMP_TO_EDGE, /*CL_ADDRESS_CLAMP_TO_EDGE,*/
            CL_FILTER_LINEAR, /* CL_FILTER_NEAREST, */
            &err_code
    );

   


    MispTestParams params;

    params.src_offset_x     = 0;
    params.src_offset_y     = 0;
    params.src_width        = src_width;
    params.src_height       = src_height;
    params.src_line_stride  = src_line_stride;
    params.src_plane_stride = src_plane_stride;
    params.src_rggb         = src_rggb;
    params.dst_width        = dst_width;
    params.dst_height       = dst_height;
    params.dst_line_stride  = dst_line_stride;
    params.dst_plane_stride = dst_plane_stride;

    params.gain_r = 1.0; //ped_gain;
    params.gain_g = 1.0; //ped_gain;
    params.gain_b = 1.0; //ped_gain;
    params.gamma  = 1.5;

    //hack for 1920x1080 MIPI10 conversion issue, so we request a wider image from camera, but trim it to 1920
    if (params.src_width == 1936)
    {
        params.src_width = 1920;
    }


    //10 and 12 bit RAW
    cl::Kernel & downscale_blur_kernel = kernel4;
    cl::Kernel & normalize_kernel      = kernel5;
    //cl::Kernel & normalize_kernel      = raw10_12_to_mono8_norm_kernel;
    int pix_per_item                   = 2;

    if (src_bit_width == 8){
        downscale_blur_kernel          = raw8_downscale_blur_to_raw8_kernel;
        normalize_kernel               = raw8_to_raw8_norm_kernel;
        pix_per_item                   = 2;
    }

    cl_box_size_qcom box_size;
    //box_size.x = downscale_factor;
    //box_size.y = downscale_factor;
    box_size.x = 16;
    box_size.y = 16;

    

    //uint32_t scratch_width  = dst_width;
    //uint32_t scratch_height = dst_height;

    int argc = 0;
    int kernel_idx = 0;

    //first downscale using box average
    try{
        downscale_blur_kernel.setArg(argc++,src_mem_cl);
        downscale_blur_kernel.setArg(argc++,sampler);
        downscale_blur_kernel.setArg(argc++,scratch_mem_cl);

        downscale_blur_kernel.setArg(argc++,params.src_offset_x);
        downscale_blur_kernel.setArg(argc++,params.src_offset_y);
        downscale_blur_kernel.setArg(argc++,params.src_width);
        downscale_blur_kernel.setArg(argc++,params.src_height);
        downscale_blur_kernel.setArg(argc++,params.src_rggb);
        downscale_blur_kernel.setArg(argc++,scratch_width);
        downscale_blur_kernel.setArg(argc++,scratch_height);
        downscale_blur_kernel.setArg(argc++,scratch_buffer->stride);  //scratch_width
        downscale_blur_kernel.setArg(argc++,scratch_buffer->slice);  //scratch_height
        downscale_blur_kernel.setArg(argc++,box_size);

        argc = 0;
        kernel_idx++;
        blur_kernel.setArg(argc++,scratch2_mem_cl);
        blur_kernel.setArg(argc++,sampler);
        blur_kernel.setArg(argc++,blurred_mem_cl);
        blur_kernel.setArg(argc++,weight_mem_cl);
        blur_kernel.setArg(argc++,scratch_buffer2->stride);

        argc = 0;
        kernel_idx++;
        normalize_kernel.setArg(argc++,src_mem_cl);
        normalize_kernel.setArg(argc++,sampler);
        normalize_kernel.setArg(argc++,dst_mem_cl);

        normalize_kernel.setArg(argc++,params.gain_r);
        normalize_kernel.setArg(argc++,params.gain_g);
        normalize_kernel.setArg(argc++,params.gain_b);
        normalize_kernel.setArg(argc++,params.gamma);

        normalize_kernel.setArg(argc++,params.src_offset_x);
        normalize_kernel.setArg(argc++,params.src_offset_y);
        normalize_kernel.setArg(argc++,params.src_width);
        normalize_kernel.setArg(argc++,params.src_height);
        normalize_kernel.setArg(argc++,params.src_rggb);
        normalize_kernel.setArg(argc++,params.dst_width);
        normalize_kernel.setArg(argc++,params.dst_height);
        normalize_kernel.setArg(argc++,params.dst_line_stride);
        normalize_kernel.setArg(argc++,params.dst_plane_stride);
        //normalize_kernel.setArg(argc++,scratch2_mem_cl);
        normalize_kernel.setArg(argc++,blurred2_mem_cl);


        cl::Event syncEvent;
        //int64_t kernel_start = GetTimeUs();
        //queue.enqueueNDRangeKernel(kernel4, cl::NullRange, cl::NDRange(scratch_width/2,scratch_height/2), cl::NDRange(4,4), NULL, &syncEvent);
        queue.enqueueNDRangeKernel(kernel4, cl::NullRange, cl::NDRange(scratch_width/pix_per_item,(scratch_height)/pix_per_item), cl::NDRange(4,4), NULL);
        queue.enqueueNDRangeKernel(blur_kernel, cl::NullRange, cl::NDRange(scratch_width,scratch_height), cl::NDRange(32,32), NULL);
        //queue.enqueueNDRangeKernel(blur_kernel, cl::NullRange, cl::NDRange(scratch_width,scratch_height), cl::NDRange(32,32), NULL, &syncEvent);
        queue.enqueueNDRangeKernel(normalize_kernel, cl::NullRange, cl::NDRange(params.dst_width/pix_per_item,(params.dst_height)/pix_per_item), cl::NDRange(64,16), NULL, &syncEvent);
        
        cl_int errCode = syncEvent.wait();
        if (errCode)
        {
          std::cout << "Enqueue kernel error " << errCode << std::endl;
        }

        //int64_t kernel_stop  = GetTimeUs();
        //std::cout << "downscale + filter + main kernel time = " << (kernel_stop-kernel_start) << "us" << std::endl;
    }

    catch (const cl::Error &err) {
        std::cerr
            << "OpenCL error (kernel " << kernel_idx <<", arg " << argc <<"):"
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        //return 1;
    }

    queue.finish();
    

    


    
   
/*
    //blur kernel
    try{
        int argc = 0;
        blur_kernel.setArg(argc++,scratch2_mem_cl);
        blur_kernel.setArg(argc++,sampler);
        blur_kernel.setArg(argc++,blurred_mem_cl);
        blur_kernel.setArg(argc++,weight_mem_cl);
        blur_kernel.setArg(argc++,scratch_buffer2->stride);

        cl::Event syncEvent;
        int64_t kernel_start = GetTimeUs();
        queue.enqueueNDRangeKernel(blur_kernel, cl::NullRange, cl::NDRange(scratch_width,scratch_height), cl::NDRange(32,32), NULL, &syncEvent);
        cl_int errCode = syncEvent.wait();
        if (errCode)
        {
          std::cout << "Enqueue kernel error " << errCode << std::endl;
        }

        int64_t kernel_stop  = GetTimeUs();

        std::cout << "blur kernel time = " << (kernel_stop-kernel_start) << "us" << std::endl;
    }

    catch (const cl::Error &err) {
        std::cerr
            << "OpenCL error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        //return 1;
    }

    queue.finish();
*/
    

    



    
/*
    try{
        argc = 0;
        kernel_idx++;
        normalize_kernel.setArg(argc++,src_mem_cl);
        normalize_kernel.setArg(argc++,sampler);
        normalize_kernel.setArg(argc++,dst_mem_cl);

        normalize_kernel.setArg(argc++,params.gain_r);
        normalize_kernel.setArg(argc++,params.gain_g);
        normalize_kernel.setArg(argc++,params.gain_b);
        normalize_kernel.setArg(argc++,params.gamma);

        normalize_kernel.setArg(argc++,params.src_offset_x);
        normalize_kernel.setArg(argc++,params.src_offset_y);
        normalize_kernel.setArg(argc++,params.src_width);
        normalize_kernel.setArg(argc++,params.src_height);
        normalize_kernel.setArg(argc++,params.src_rggb);
        normalize_kernel.setArg(argc++,params.dst_width);
        normalize_kernel.setArg(argc++,params.dst_height);
        normalize_kernel.setArg(argc++,params.dst_line_stride);
        normalize_kernel.setArg(argc++,params.dst_plane_stride);
        //normalize_kernel.setArg(argc++,scratch2_mem_cl);
        normalize_kernel.setArg(argc++,blurred2_mem_cl);

        cl::Event syncEvent;
        int64_t kernel_start = GetTimeUs();
        //queue.enqueueNDRangeKernel(kernel5, cl::NullRange, cl::NDRange(params.dst_width/2,params.dst_height/2), cl::NDRange(16,16), NULL, &syncEvent);
        queue.enqueueNDRangeKernel(normalize_kernel, cl::NullRange, cl::NDRange(params.dst_width/pix_per_item,(params.dst_height)/pix_per_item), cl::NDRange(64,16), NULL, &syncEvent);
        cl_int errCode = syncEvent.wait();
        if (errCode)
        {
          std::cout << "Enqueue kernel error " << errCode << std::endl;
        }

        int64_t kernel_stop  = GetTimeUs();

        std::cout << "test1 (2) kernel time = " << (kernel_stop-kernel_start) << "us" << std::endl;
    }

    catch (const cl::Error &err) {
        std::cerr
            << "OpenCL error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        //return 1;
    }

    queue.finish();
    */

    clReleaseMemObject(blurred2_mem_cl());
    
    clReleaseMemObject(scratch2_mem_cl());
    clReleaseMemObject(blurred_mem_cl());
    clReleaseMemObject(weight_mem_cl());
    
    clReleaseMemObject(scratch_mem_cl());

    clReleaseMemObject(src_mem_cl());
    clReleaseMemObject(dst_mem_cl());
    clReleaseSampler(sampler());
    
    
    return 0;
}

int Misp::ConvertMonoMipi10ToRaw8(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_rggb,
                             void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride)
{
    cl_int     err_code;
    cl::Memory src_mem_cl;
    cl::Memory dst_mem_cl;

    //map the input MIPI10 image to GPU as regular buffer
    MapIonBufferToGpu(gpu_context, src_host_ptr, src_fd, src_size, &src_mem_cl, &err_code);

    //map the output RAW8 buffer to GPU as regular buffer
    MapIonBufferToGpu(gpu_context, dst_host_ptr, dst_fd, dst_size, &dst_mem_cl, &err_code);

    try{
        int argc = 0;
        kernel3.setArg(argc++,src_mem_cl);
        kernel3.setArg(argc++,dst_mem_cl);
        kernel3.setArg(argc++,src_width);
        kernel3.setArg(argc++,src_height);
        kernel3.setArg(argc++,src_line_stride);
        kernel3.setArg(argc++,dst_line_stride);

        cl::Event syncEvent;
        //int64_t kernel_start = GetTimeUs();
        //queue.enqueueNDRangeKernel(kernel3, cl::NullRange, cl::NDRange(src_width/16,src_height), cl::NDRange(32,32), NULL, &syncEvent);
        queue.enqueueNDRangeKernel(kernel3, cl::NullRange, cl::NDRange(src_width/16,src_height), cl::NDRange(64,16), NULL, &syncEvent);
        cl_int errCode = syncEvent.wait();
        if (errCode)
        {
          std::cout << "Enqueue kernel error " << errCode << std::endl;
        }

        //int64_t kernel_stop  = GetTimeUs();
        //std::cout << "kernel time = " << (kernel_stop-kernel_start) << "us" << std::endl;
    }

    catch (const cl::Error &err) {
        std::cerr
            << "OpenCL error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        //return 1;
    }

    queue.finish();

    clReleaseMemObject(src_mem_cl());
    clReleaseMemObject(dst_mem_cl());

    return 0;
}

int Misp::ConvertMonoMipi12ToRaw8(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_rggb,
                             void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride)
{
    cl_int     err_code;
    cl::Memory src_mem_cl;
    cl::Memory dst_mem_cl;

    //map the input MIPI12 image to GPU as regular buffer
    MapIonBufferToGpu(gpu_context, src_host_ptr, src_fd, src_size, &src_mem_cl, &err_code);

    //map the output RAW8 buffer to GPU as regular buffer
    MapIonBufferToGpu(gpu_context, dst_host_ptr, dst_fd, dst_size, &dst_mem_cl, &err_code);

    try{
        int argc = 0;
        kernel2.setArg(argc++,src_mem_cl);
        kernel2.setArg(argc++,dst_mem_cl);
        kernel2.setArg(argc++,src_width);
        kernel2.setArg(argc++,src_height);
        kernel2.setArg(argc++,src_line_stride);
        kernel2.setArg(argc++,dst_line_stride);

        cl::Event syncEvent;
        //int64_t kernel_start = GetTimeUs();
        //queue.enqueueNDRangeKernel(kernel2, cl::NullRange, cl::NDRange(src_width/16,src_height), cl::NDRange(32,32), NULL, &syncEvent);
        queue.enqueueNDRangeKernel(kernel2, cl::NullRange, cl::NDRange(src_width/2,src_height), cl::NDRange(64,16), NULL, &syncEvent);
        cl_int errCode = syncEvent.wait();
        if (errCode)
        {
          std::cout << "Enqueue kernel error " << errCode << std::endl;
        }

        //int64_t kernel_stop  = GetTimeUs();
        //std::cout << "kernel time = " << (kernel_stop-kernel_start) << "us" << std::endl;
    }

    catch (const cl::Error &err) {
        std::cerr
            << "OpenCL error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        //return 1;
    }

    queue.finish();

    clReleaseMemObject(src_mem_cl());
    clReleaseMemObject(dst_mem_cl());

    return 0;
}


int Misp::AR0144MipiToRaw8LSC(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_bpp,
                                void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride)
{
    cl_int     err_code;
    cl::Memory src_mem_cl;
    cl::Memory dst_mem_cl;
    cl::Memory dst2_mem_cl;

    //map the input MIPI12 image to GPU as regular buffer
    MapIonBufferToGpu(gpu_context, src_host_ptr, src_fd, src_size, &src_mem_cl, &err_code);

    //map the output RAW8 buffer to GPU as regular buffer
    MapIonBufferToGpu(gpu_context, dst_host_ptr, dst_fd, dst_size, &dst_mem_cl, &err_code);

    MapIonBufferToGpu(gpu_context, scratch_full_buffer->vaddress, scratch_full_buffer->fd, scratch_full_buffer->size, &dst2_mem_cl, &err_code);

    
    //8 bit
    cl::Kernel & k             = ar0144_8bit_lsc_kernel;
    uint32_t pix_per_item      = 1;
    cl::NDRange workgroup_size = cl::NDRange(32,32);

    if (src_bpp == 10){
        k              = ar0144_10bit_lsc_kernel;
        pix_per_item   = 4;
        workgroup_size = cl::NDRange(64,16);
    }
    else if (src_bpp == 12){
        k              = ar0144_12bit_lsc_kernel;
        pix_per_item   = 2;
        workgroup_size = cl::NDRange(8,64);
    }

    try{
        int argc = 0;
        k.setArg(argc++,src_mem_cl);
        k.setArg(argc++,dst_mem_cl);
        k.setArg(argc++,dst2_mem_cl);
        k.setArg(argc++,src_width);
        k.setArg(argc++,src_height);
        k.setArg(argc++,src_line_stride);
        k.setArg(argc++,dst_line_stride);

        cl::Event syncEvent;
        //int64_t kernel_start = GetTimeUs();
        queue.enqueueNDRangeKernel(k, cl::NullRange, cl::NDRange(src_width/pix_per_item,src_height), workgroup_size, NULL, &syncEvent);  //process two pixels at a time
        cl_int errCode = syncEvent.wait();
        if (errCode)
        {
          std::cout << "Enqueue kernel error " << errCode << std::endl;
        }

        //int64_t kernel_stop  = GetTimeUs();
        //std::cout << "AR0144Mipi12ToRaw8LSC kernel time = " << (kernel_stop-kernel_start) << "us" << std::endl;
    }

    catch (const cl::Error &err) {
        std::cerr
            << "OpenCL error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        //return 1;
    }

    queue.finish();

    clReleaseMemObject(src_mem_cl());
    clReleaseMemObject(dst_mem_cl());
    clReleaseMemObject(dst2_mem_cl());

    return 0;
}


int Misp::ConvertMonoMipi12ToYuv(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_rggb,
                             void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride)
{
    cl_int     err_code;
    cl::Memory src_mem_cl;
    cl::Memory dst_mem_cl;

    //map the input MIPI12 image to GPU
    MapIonBufferToGpuImage2dBayer12(gpu_context, src_host_ptr, src_fd, src_size, src_width, src_height, src_line_stride, &src_mem_cl, &err_code);
    
    //map the output YUV buffer as regular buffer (not YUV multi-plane opencl type)
    MapIonBufferToGpu(gpu_context, dst_host_ptr, dst_fd, dst_size, &dst_mem_cl, &err_code);

    cl::Sampler sampler(
            gpu_context,
            CL_FALSE,
            CL_ADDRESS_CLAMP_TO_EDGE, /*CL_ADDRESS_CLAMP_TO_EDGE,*/
            CL_FILTER_LINEAR, /* CL_FILTER_NEAREST, */
            &err_code
    );

    //const float ped      = 16.0/256.0;  //IMX412 has pedestal value of 64 for 10bit output, so 16.0 for 8 bit
    //const float ped_gain = 1.0 / (1.0 - ped);

    MispTestParams params;

    params.src_offset_x     = 0;
    params.src_offset_y     = 0;
    params.src_width        = src_width;
    params.src_height       = src_height;

    //hack for 1920x1080 MIPI10 conversion issue, so we request a wider image from camera, but trim it to 1920
    if (params.src_width == 1936)
    {
        params.src_width = 1920;
    }

    params.src_line_stride  = src_line_stride;
    params.src_plane_stride = src_plane_stride;
    params.src_rggb         = src_rggb;
    params.dst_width        = dst_width;
    params.dst_height       = dst_height;
    params.dst_line_stride  = dst_line_stride;
    params.dst_plane_stride = dst_plane_stride;
    params.gain_r = 1.00; //ped_gain;
    params.gain_g = 1.0; //ped_gain;
    params.gain_b = 1.0; //ped_gain;
    params.gamma  = 1.0;

    try
    {
        //printf("starting to set args");
        int argc = 0;
        kernel.setArg(argc++,src_mem_cl);  //printf("loaded arg 0\n");
        kernel.setArg(argc++,sampler);     //printf("loaded arg 1\n");
        kernel.setArg(argc++,dst_mem_cl);  //printf("loaded arg 2\n");


        kernel.setArg(argc++,params.gain_r);
        kernel.setArg(argc++,params.gain_g);
        kernel.setArg(argc++,params.gain_b);
        kernel.setArg(argc++,params.gamma);
        kernel.setArg(argc++,params.src_offset_x);
        kernel.setArg(argc++,params.src_offset_y);
        kernel.setArg(argc++,params.src_width);
        kernel.setArg(argc++,params.src_height);
        kernel.setArg(argc++,params.src_rggb);
        kernel.setArg(argc++,params.dst_width);
        kernel.setArg(argc++,params.dst_height);
        kernel.setArg(argc++,params.dst_line_stride);
        kernel.setArg(argc++,params.dst_plane_stride);




        cl::Event syncEvent;
        //int64_t kernel_start = GetTimeUs();
        queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(64,16), NULL, &syncEvent);
        cl_int errCode = syncEvent.wait();
        if (errCode)
        {
          std::cout << "Enqueue kernel error " << errCode << std::endl;
        }

        //int64_t kernel_stop  = GetTimeUs();
        //std::cout << "kernel time = " << (kernel_stop-kernel_start) << "us" << std::endl;
    }

    catch (const cl::Error &err) {
        std::cerr
            << "OpenCL error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        //return 1;
    }

    queue.finish();

    clReleaseSampler(sampler());
    clReleaseMemObject(src_mem_cl());
    clReleaseMemObject(dst_mem_cl());

    return 0;
}

int Misp::DrawPip(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride,
                 void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride)
{
    int64_t kernel_start = GetTimeUs();

    cl_int     err_code;
    cl::Memory src_mem_cl;
    cl::Memory dst_mem_cl;

    //map the input RAW8 image to GPU
    MapIonBufferToGpuImage2dY8(gpu_context, src_host_ptr, src_fd, src_size, src_width, src_height, src_line_stride, &src_mem_cl, &err_code);

    //map the output YUV buffer as regular buffer (not YUV multi-plane opencl type)
    MapIonBufferToGpu(gpu_context, dst_host_ptr, dst_fd, dst_size, &dst_mem_cl, &err_code);

    cl::Sampler sampler(
            gpu_context,
            CL_FALSE,
            CL_ADDRESS_CLAMP_TO_EDGE, /*CL_ADDRESS_CLAMP_TO_EDGE,*/
            CL_FILTER_LINEAR,
            &err_code
    );


    try
    {
        /*
        //printf("starting to set args");
        int argc = 0;
        kernel.setArg(argc++,src_mem_cl);  //printf("loaded arg 0\n");
        kernel.setArg(argc++,sampler);     //printf("loaded arg 1\n");
        kernel.setArg(argc++,dst_mem_cl);  //printf("loaded arg 2\n");


        kernel.setArg(argc++,params.src_offset_x);
        kernel.setArg(argc++,params.src_offset_y);
        kernel.setArg(argc++,params.src_width);
        kernel.setArg(argc++,params.src_height);

        kernel.setArg(argc++,params.dst_width);
        kernel.setArg(argc++,params.dst_height);
        kernel.setArg(argc++,params.dst_line_stride);
        kernel.setArg(argc++,params.dst_plane_stride);
*/
        cl::Event syncEvent;
        
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(1,src_height), cl::NDRange(1,16), NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(16,16), NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NullRange, NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(64,16), NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(32,32), NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(64,16), NULL, &syncEvent);
        cl_int errCode = syncEvent.wait();
        if (errCode)
        {
          std::cout << "Enqueue kernel error " << errCode << std::endl;
        }
    }


    catch (const cl::Error &err) {
        std::cerr
            << "OpenCL error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        //return 1;
    }

    queue.finish();

    clReleaseSampler(sampler());
    clReleaseMemObject(src_mem_cl());
    clReleaseMemObject(dst_mem_cl());

    int64_t kernel_stop  = GetTimeUs();
    if (0){
        std::cout << "kernel time = " << (kernel_stop-kernel_start) << "us" << std::endl;
    }
    

    return 0;
}


int Misp::ConvertMipi10ToYuv(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_rggb,
                             void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride)
{
    int64_t kernel_start = GetTimeUs();
    //std::cout<<"+ConvertMipi10ToYuv"<<std::endl;
    cl_int     err_code;
    cl::Memory src_mem_cl;
    cl::Memory dst_mem_cl;
    //cl_int     output_line_stride  = dst_line_stride;  //pixels
    //cl_int     output_plane_stride = dst_plane_stride; //lines
    

    //map the input MIPI10 image to GPU
    MapIonBufferToGpuImage2dBayer10(gpu_context, src_host_ptr, src_fd, src_size, src_width, src_height, src_line_stride, &src_mem_cl, &err_code);
    
    //map the output YUV buffer as regular buffer (not YUV multi-plane opencl type)
    MapIonBufferToGpu(gpu_context, dst_host_ptr, dst_fd, dst_size, &dst_mem_cl, &err_code);


    cl::Sampler sampler(
            gpu_context,
            CL_FALSE,
            CL_ADDRESS_CLAMP_TO_EDGE, /*CL_ADDRESS_CLAMP_TO_EDGE,*/
            CL_FILTER_LINEAR, /* CL_FILTER_NEAREST */
            &err_code
    );

    //MispTestParams params;
    static int cntr=0;
    cntr++;

    //hack for 1920x1080 MIPI10 conversion issue, so we request a wider image from camera, but trim it to 1920
    if (src_width == 1936)
    {
        src_width = 1920;
    }

    float out_zoom    = roi_zoom;
    if (out_zoom < 1.0005)
        out_zoom = 1.0;
        
    float src_roi_width  = src_width  / out_zoom;
    float src_roi_height = src_height / out_zoom;

    if (out_zoom == 1.0){
        src_roi_width  = src_width;
        src_roi_height = src_height;
    }

    //crop input to maintain output aspect ratio
    bool maintain_output_aspect_ratio = 1;
    if (maintain_output_aspect_ratio) {
        float h_ratio   = src_roi_width  / dst_width;
        float v_ratio   = src_roi_height / dst_height;
        float min_ratio = h_ratio < v_ratio ? h_ratio : v_ratio;
    
        src_roi_width   = min_ratio * dst_width;
        src_roi_height  = min_ratio * dst_height;
    }

    if (src_roi_width  > src_width)  src_roi_width  = src_width; //sanity check
    if (src_roi_height > src_height) src_roi_height = src_height;

    float src_roi_offset_x   = src_width  / 2 - src_roi_width  / 2;
    float src_roi_offset_y   = src_height / 2 - src_roi_height / 2;

    if (src_roi_offset_x < 0) src_roi_offset_x = 0; //sanity check
    if (src_roi_offset_y < 0) src_roi_offset_y = 0;

    //M_PRINT("zoom %.4f, offsets x,y %f %f, src roi x,y %f %f\n", out_zoom, src_roi_offset_x, src_roi_offset_y, src_roi_width, src_roi_height);

    //params.src_line_stride  = src_line_stride;
    //params.src_plane_stride = src_plane_stride;

    try
    {
        //printf("starting to set args");
        int argc = 0;
        kernel.setArg(argc++,src_mem_cl);  //printf("loaded arg 0\n");
        kernel.setArg(argc++,sampler);     //printf("loaded arg 1\n");
        kernel.setArg(argc++,dst_mem_cl);  //printf("loaded arg 2\n");


        kernel.setArg(argc++,awb_gains[0]);
        kernel.setArg(argc++,awb_gains[1]);
        kernel.setArg(argc++,awb_gains[2]);
        kernel.setArg(argc++,gamma_correction);
        kernel.setArg(argc++,src_roi_offset_x);
        kernel.setArg(argc++,src_roi_offset_y);
        kernel.setArg(argc++,src_roi_width);
        kernel.setArg(argc++,src_roi_height);
        kernel.setArg(argc++,src_rggb);
        kernel.setArg(argc++,dst_width);
        kernel.setArg(argc++,dst_height);
        kernel.setArg(argc++,dst_line_stride);
        kernel.setArg(argc++,dst_plane_stride);

        cl::Event syncEvent;
        
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(1,src_height), cl::NDRange(1,16), NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(16,16), NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NullRange, NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(64,16), NULL, &syncEvent);
        //queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(32,32), NULL, &syncEvent);
        queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(64,16), NULL, &syncEvent);
        cl_int errCode = syncEvent.wait();
        if (errCode)
        {
          std::cout << "Enqueue kernel error " << errCode << std::endl;
        }
    }

    catch (const cl::Error &err) {
        std::cerr
            << "OpenCL error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        //return 1;
    }

    queue.finish();

    clReleaseSampler(sampler());
    clReleaseMemObject(src_mem_cl());
    clReleaseMemObject(dst_mem_cl());

    int64_t kernel_stop  = GetTimeUs();
    if (0){
        std::cout << "kernel time = " << (kernel_stop-kernel_start) << "us" << std::endl;
    }
    

    return 0;
}



int Misp::ConvertMipi10ToYuvRot(void * src_host_ptr, int32_t src_fd, uint32_t src_size, uint32_t src_width, uint32_t src_height, uint32_t src_line_stride, uint32_t src_plane_stride, uint32_t src_rggb,
                             void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride, float H[9])
{
    //std::cout<<"+ConvertMipi10ToYuv"<<std::endl;
    cl_int     err_code;
    cl::Memory src_mem_cl;
    cl::Memory dst_mem_cl;
    //cl_int     output_line_stride  = dst_line_stride;  //pixels
    //cl_int     output_plane_stride = dst_plane_stride; //lines
    

    //map the input MIPI10 image to GPU
    MapIonBufferToGpuImage2dBayer10(gpu_context, src_host_ptr, src_fd, src_size, src_width, src_height, src_line_stride, &src_mem_cl, &err_code);
    
    //map the output YUV buffer as regular buffer (not YUV multi-plane opencl type)
    MapIonBufferToGpu(gpu_context, dst_host_ptr, dst_fd, dst_size, &dst_mem_cl, &err_code);


    cl::Sampler sampler(
            gpu_context,
            CL_FALSE,
            CL_ADDRESS_CLAMP, /*CL_ADDRESS_CLAMP_TO_EDGE,*/
            CL_FILTER_LINEAR,
            &err_code
    );

/*
    cl::Sampler sampler(
            gpu_context,
            CL_FALSE,
            CL_ADDRESS_NONE,
            CL_FILTER_NEAREST,
            &err_code
    );
*/

    //const float ped      = 16.0/256.0;  //IMX412 has pedestal value of 64 for 10bit output, so 16.0 for 8 bit
    //const float ped_gain = 1.0 / (1.0 - ped);

    MispTestParams params;
    static int cntr=0;
    cntr++;

    //image rotation for testing
    //params.R00 = cosf(2.0*M_PI * cntr / (2*240.0));
    //params.R01 = -sinf(2.0*M_PI * cntr / (2*240.0));
    //params.R10 = -params.R01;
    //params.R11 = params.R00;
    

    params.R00 = 1.0;
    params.R01 = 0.0;
    params.R10 = 0.0;
    params.R11 = 1.0;
    /*
    params.src_offset_x     = src_width/2;
    params.src_offset_y     = src_height/2;
    params.src_width        = src_width/2;
    params.src_height       = src_height/2;
    */

    params.src_offset_x     = 0;
    params.src_offset_y     = 0;
    params.src_width        = src_width;
    params.src_height       = src_height;

    //hack for 1920x1080 MIPI10 conversion issue, so we request a wider image from camera, but trim it to 1920
    if (params.src_width == 1936)
    {
        params.src_width = 1920;
    }

    params.src_line_stride  = src_line_stride;
    params.src_plane_stride = src_plane_stride;
    params.src_rggb         = src_rggb;

    params.dst_width        = dst_width;
    params.dst_height       = dst_height;
    params.dst_line_stride  = dst_line_stride;
    params.dst_plane_stride = dst_plane_stride;
    params.gain_r = 1.025; //ped_gain;
    params.gain_g = 1.0; //ped_gain;
    params.gain_b = 2.5; //ped_gain;
    params.gamma  = 2.0;



    EisParams eis_params;
    memcpy(&eis_params.H, &H[0], 9*sizeof(float));

    cl::Kernel & k = transform_kernel;


    try
    {
        //printf("starting to set args");
        int argc = 0;
        k.setArg(argc++,src_mem_cl);  //printf("loaded arg 0\n");
        k.setArg(argc++,sampler);     //printf("loaded arg 1\n");
        k.setArg(argc++,dst_mem_cl);  //printf("loaded arg 2\n");


        k.setArg(argc++,params.gain_r);
        k.setArg(argc++,params.gain_g);
        k.setArg(argc++,params.gain_b);
        k.setArg(argc++,params.gamma);
        k.setArg(argc++,params.src_offset_x);
        k.setArg(argc++,params.src_offset_y);
        k.setArg(argc++,params.src_width);
        k.setArg(argc++,params.src_height);
        k.setArg(argc++,params.src_rggb);
        k.setArg(argc++,params.dst_width);
        k.setArg(argc++,params.dst_height);
        k.setArg(argc++,params.dst_line_stride);
        k.setArg(argc++,params.dst_plane_stride);

        for (int i=0; i<9; i++){
            k.setArg(argc++,eis_params.H[i]);
        }
        


        cl::Event syncEvent;
        //int64_t kernel_start = GetTimeUs();

        queue.enqueueNDRangeKernel(k, cl::NullRange, cl::NDRange(dst_width/2,dst_height/2), cl::NDRange(32,32), NULL, &syncEvent);
        cl_int errCode = syncEvent.wait();
        if (errCode)
        {
          std::cout << "Enqueue kernel error " << errCode << std::endl;
        }

        //int64_t kernel_stop  = GetTimeUs();

        //std::cout << "kernel time = " << (kernel_stop-kernel_start) << "us" << std::endl;
    }

    catch (const cl::Error &err) {
        std::cerr
            << "OpenCL error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        //return 1;
    }

    queue.finish();

    clReleaseSampler(sampler());
    clReleaseMemObject(src_mem_cl());
    clReleaseMemObject(dst_mem_cl());

    return 0;
}



uint8_t * Misp::GetTempBuffer(uint32_t size)
{
    temp_buffer.resize(size);
    return &(temp_buffer[0]);
}



int Misp::MapIonBufferToGpu(cl::Context & cl_ctx, 
                      void * host_ptr, int32_t ion_fd, uint32_t size, 
                      cl::Memory * cl_mem_out_ptr,
                      cl_int * err_code)
{
    cl_mem_ion_host_ptr  cl_mem_ptr;
    memset(&cl_mem_ptr, 0, sizeof(cl_mem_ion_host_ptr));
    cl_mem_ptr.ext_host_ptr.allocation_type   = CL_MEM_ION_HOST_PTR_QCOM;
    cl_mem_ptr.ext_host_ptr.host_cache_policy = CL_MEM_HOST_UNCACHED_QCOM; //CL_MEM_HOST_WRITEBACK_QCOM; //CL_MEM_HOST_UNCACHED_QCOM;
    cl_mem_ptr.ion_filedesc                   = ion_fd;
    cl_mem_ptr.ion_hostptr                    = host_ptr;

    *cl_mem_out_ptr = cl::Buffer(cl_ctx,
                               CL_MEM_USE_HOST_PTR | CL_MEM_EXT_HOST_PTR_QCOM,
                               size , &cl_mem_ptr, err_code);

    return 0;
}

int Misp::MapIonBufferToGpuImage2dY8(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code)
{
    cl_mem_ion_host_ptr  cl_mem_ptr;
    memset(&cl_mem_ptr, 0, sizeof(cl_mem_ion_host_ptr));
    cl_mem_ptr.ext_host_ptr.allocation_type   = CL_MEM_ION_HOST_PTR_QCOM;
    cl_mem_ptr.ext_host_ptr.host_cache_policy = CL_MEM_HOST_UNCACHED_QCOM; //CL_MEM_HOST_WRITEBACK_QCOM; //CL_MEM_HOST_UNCACHED_QCOM;
    cl_mem_ptr.ion_filedesc                   = ion_fd;
    cl_mem_ptr.ion_hostptr                    = host_ptr;

    cl::ImageFormat src_format(CL_R, CL_UNORM_INT8);
    //cl::ImageFormat src_format(CL_QCOM_NV12, CL_UNORM_INT8); //also works
    //cl::ImageFormat src_format(CL_INTENSITY, CL_UNORM_INT8);  //does not work

    *cl_mem_out_ptr = cl::Image2D(cl_ctx,
                                  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_HOST_PTR_QCOM,
                                  src_format,
                                  width, height, stride,
                                  &cl_mem_ptr, err_code);

    return 0;
}

/*
int Misp::MapIonBufferToGpuImage2dBayer8(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code)
{
    cl_mem_ion_host_ptr  cl_mem_ptr;
    memset(&cl_mem_ptr, 0, sizeof(cl_mem_ion_host_ptr));
    cl_mem_ptr.ext_host_ptr.allocation_type   = CL_MEM_ION_HOST_PTR_QCOM;
    cl_mem_ptr.ext_host_ptr.host_cache_policy = CL_MEM_HOST_UNCACHED_QCOM; //CL_MEM_HOST_WRITEBACK_QCOM; //CL_MEM_HOST_UNCACHED_QCOM;
    cl_mem_ptr.ion_filedesc                   = ion_fd;
    cl_mem_ptr.ion_hostptr                    = host_ptr;

    cl::ImageFormat src_format(CL_QCOM_BAYER, CL_QCOM_UNORM_INT8);  //does not work

    *cl_mem_out_ptr = cl::Image2D(cl_ctx,
                                  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_HOST_PTR_QCOM,
                                  src_format,
                                  width, height, stride,
                                  &cl_mem_ptr, err_code);

    return 0;
}
*/

int Misp::MapIonBufferToGpuImage2dY16(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code)
{
    cl_mem_ion_host_ptr  cl_mem_ptr;
    memset(&cl_mem_ptr, 0, sizeof(cl_mem_ion_host_ptr));
    cl_mem_ptr.ext_host_ptr.allocation_type   = CL_MEM_ION_HOST_PTR_QCOM;
    cl_mem_ptr.ext_host_ptr.host_cache_policy = CL_MEM_HOST_UNCACHED_QCOM; //CL_MEM_HOST_WRITEBACK_QCOM; //CL_MEM_HOST_UNCACHED_QCOM;
    cl_mem_ptr.ion_filedesc                   = ion_fd;
    cl_mem_ptr.ion_hostptr                    = host_ptr;

    cl::ImageFormat src_format(CL_R, CL_UNORM_INT16);

    *cl_mem_out_ptr = cl::Image2D(cl_ctx,
                                  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_HOST_PTR_QCOM,
                                  src_format,
                                  width, height, stride,
                                  &cl_mem_ptr, err_code);

    return 0;
}

int Misp::MapIonBufferToGpuImage2dBayer10(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code)
{
    cl_mem_ion_host_ptr  cl_mem_ptr;
    memset(&cl_mem_ptr, 0, sizeof(cl_mem_ion_host_ptr));
    cl_mem_ptr.ext_host_ptr.allocation_type   = CL_MEM_ION_HOST_PTR_QCOM;
    cl_mem_ptr.ext_host_ptr.host_cache_policy = CL_MEM_HOST_UNCACHED_QCOM; //CL_MEM_HOST_WRITEBACK_QCOM; //CL_MEM_HOST_UNCACHED_QCOM;
    cl_mem_ptr.ion_filedesc                   = ion_fd;
    cl_mem_ptr.ion_hostptr                    = host_ptr;

    cl::ImageFormat src_format(CL_QCOM_BAYER, CL_QCOM_UNORM_MIPI10);

/*
    size_t img_row_pitch = 0;
    cl_image_format src_format2;
    src_format2.image_channel_order     = CL_QCOM_BAYER;
    src_format2.image_channel_data_type = CL_QCOM_UNORM_MIPI10;
    cl_int err = clGetDeviceImageInfoQCOM(gpu_device(), width, height, &src_format2,
                                          CL_IMAGE_ROW_PITCH, sizeof(img_row_pitch), &img_row_pitch, NULL);
    printf("bayer10 input w=%d, h=%d, s=%d, image pitch = %lu, err %d\n",width, height, stride, img_row_pitch,err);
*/
    *cl_mem_out_ptr = cl::Image2D(cl_ctx,
                                  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_HOST_PTR_QCOM,
                                  src_format,
                                  width, height, stride,
                                  &cl_mem_ptr, err_code);

    return 0;
}

int Misp::MapIonBufferToGpuImage2dBayer12(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code)
{
    cl_mem_ion_host_ptr  cl_mem_ptr;
    memset(&cl_mem_ptr, 0, sizeof(cl_mem_ion_host_ptr));
    cl_mem_ptr.ext_host_ptr.allocation_type   = CL_MEM_ION_HOST_PTR_QCOM;
    cl_mem_ptr.ext_host_ptr.host_cache_policy = CL_MEM_HOST_UNCACHED_QCOM; //CL_MEM_HOST_WRITEBACK_QCOM; //CL_MEM_HOST_UNCACHED_QCOM;
    cl_mem_ptr.ion_filedesc                   = ion_fd;
    cl_mem_ptr.ion_hostptr                    = host_ptr;

    cl::ImageFormat src_format(CL_QCOM_BAYER, CL_QCOM_UNORM_MIPI12);

    *cl_mem_out_ptr = cl::Image2D(cl_ctx,
                                  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_HOST_PTR_QCOM,
                                  src_format,
                                  width, height, stride,
                                  &cl_mem_ptr, err_code);

    return 0;
}

int Misp::MapIonBufferToGpuImage2dRgba8(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr,
                                            cl_int * err_code)
{
    cl_mem_ion_host_ptr  cl_mem_ptr;
    memset(&cl_mem_ptr, 0, sizeof(cl_mem_ion_host_ptr));
    cl_mem_ptr.ext_host_ptr.allocation_type   = CL_MEM_ION_HOST_PTR_QCOM;
    cl_mem_ptr.ext_host_ptr.host_cache_policy = CL_MEM_HOST_UNCACHED_QCOM; //CL_MEM_HOST_WRITEBACK_QCOM; //CL_MEM_HOST_UNCACHED_QCOM;
    cl_mem_ptr.ion_filedesc                   = ion_fd;
    cl_mem_ptr.ion_hostptr                    = host_ptr;

    cl::ImageFormat src_format(CL_RGBA, CL_UNORM_INT8);

/*
    size_t img_row_pitch = 0;
    cl_image_format src_format2;
    src_format2.image_channel_order     = CL_RGBA;
    src_format2.image_channel_data_type = CL_UNORM_INT8;
    cl_int err = clGetDeviceImageInfoQCOM(gpu_device(), width, height, &src_format2,
                                          CL_IMAGE_ROW_PITCH, sizeof(img_row_pitch), &img_row_pitch, NULL);
    //printf("rgba input w=%d, h=%d, s=%d, image pitch = %lu, err %d\n",width, height, stride, img_row_pitch,err);
*/

    *cl_mem_out_ptr = cl::Image2D(cl_ctx,
                                  CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_HOST_PTR_QCOM,
                                  src_format,
                                  width, height, stride,
                                  &cl_mem_ptr, err_code);

    return 0;
}

int Misp::CreateGaussianKernelImage(cl::Context & cl_ctx, 
                                    cl::Memory * cl_mem_out_ptr,
                                    cl_int * err_code)
{
    const int filter_size   = 9;
    const int filter_center = 4;

    //convert the filter to half precision float
    cl_half gaussian_filter_9x9_half[81];
    for (int ii=0; ii<81; ii++){
        gaussian_filter_9x9_half[ii] = float_to_half(gaussian_filter_9x9[ii]);
    }


    cl_image_format weight_image_format;
    weight_image_format.image_channel_order     = CL_R;
    weight_image_format.image_channel_data_type = CL_HALF_FLOAT;

    cl_weight_image_desc_qcom weight_image_desc;
    std::memset(&weight_image_desc, 0, sizeof(weight_image_desc));
    weight_image_desc.image_desc.image_type        = CL_MEM_OBJECT_WEIGHT_IMAGE_QCOM;
    weight_image_desc.image_desc.image_width       = filter_size;
    weight_image_desc.image_desc.image_height      = filter_size;
    weight_image_desc.image_desc.image_array_size  = 1; //num_phases
    weight_image_desc.image_desc.image_row_pitch   = 0; //must set to zero
    weight_image_desc.image_desc.image_slice_pitch = 0; //must set to zero

    weight_image_desc.weight_desc.center_coord_x   = filter_center;
    weight_image_desc.weight_desc.center_coord_y   = filter_center;
    weight_image_desc.weight_desc.flags            = 0;  // specify separable filter. Default (flags=0) is 2D convolution filter  //CL_WEIGHT_IMAGE_SEPARABLE_QCOM

    cl_mem weight_image = clCreateImage(
            cl_ctx(),
            CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
            &weight_image_format,
            reinterpret_cast<cl_image_desc *>(&weight_image_desc),
            static_cast<void *>(gaussian_filter_9x9_half),
            err_code
    );

    if (*err_code != CL_SUCCESS) {
        std::cerr << "Error " << *err_code << " with clCreateImage for weight image." << "\n";
        std::exit(*err_code);
    }

    *cl_mem_out_ptr = weight_image;
    return 0;
}

int Misp::MapIonBufferToGpuImage2dYuv(cl::Context & cl_ctx, 
                                            void * host_ptr, int32_t ion_fd, uint32_t size,
                                            int width, int height, int stride, 
                                            cl::Memory * cl_mem_out_ptr, cl::Memory * y_plane, cl::Memory * uv_plane,
                                            cl_int * err_code)
{
    cl_mem_ion_host_ptr  cl_mem_ptr;
    memset(&cl_mem_ptr, 0, sizeof(cl_mem_ion_host_ptr));
    
    cl_mem_ptr.ext_host_ptr.allocation_type   = CL_MEM_ION_HOST_PTR_QCOM;
    cl_mem_ptr.ext_host_ptr.host_cache_policy = CL_MEM_HOST_UNCACHED_QCOM; //CL_MEM_HOST_WRITEBACK_QCOM; //CL_MEM_HOST_UNCACHED_QCOM;
    cl_mem_ptr.ion_filedesc                   = ion_fd;
    cl_mem_ptr.ion_hostptr                    = host_ptr;

    cl::ImageFormat image_format(CL_QCOM_NV12, CL_UNORM_INT8);
    
    *cl_mem_out_ptr = cl::Image2D(cl_ctx,
                                  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_HOST_PTR_QCOM,
                                  image_format,
                                  width, height, stride,
                                  &cl_mem_ptr, err_code);
    
    
    cl_image_format dst_y_plane_format;
    dst_y_plane_format.image_channel_order     = CL_QCOM_NV12_Y;
    dst_y_plane_format.image_channel_data_type = CL_UNORM_INT8;

    cl_image_desc dst_y_plane_desc;
    std::memset(&dst_y_plane_desc, 0, sizeof(dst_y_plane_desc));
    dst_y_plane_desc.image_type   = CL_MEM_OBJECT_IMAGE2D;
    dst_y_plane_desc.image_width  = width;
    dst_y_plane_desc.image_height = height;
    dst_y_plane_desc.mem_object   = (*cl_mem_out_ptr)();

    cl_mem dst_y_plane = clCreateImage(
            cl_ctx(),
            CL_MEM_READ_WRITE,
            &dst_y_plane_format,
            &dst_y_plane_desc,
            NULL,
            err_code
    );
    if (*err_code != CL_SUCCESS)
    {
        std::cerr << "Error " << *err_code << " with clCreateImage for dst image y plane." << "\n";
        std::exit(*err_code);
    }
    
    
    cl_image_format dst_uv_plane_format;
    dst_uv_plane_format.image_channel_order     = CL_QCOM_NV12_UV;
    dst_uv_plane_format.image_channel_data_type = CL_UNORM_INT8;

    cl_image_desc dst_uv_plane_desc;
    std::memset(&dst_uv_plane_desc, 0, sizeof(dst_uv_plane_desc));
    dst_uv_plane_desc.image_type   = CL_MEM_OBJECT_IMAGE2D;
    // The image dimensions for the uv-plane derived image must be the same as the parent image, even though the
    // actual dimensions of the uv-plane differ by a factor of 2 in each dimension.
    dst_uv_plane_desc.image_width  = width;
    dst_uv_plane_desc.image_height = height;
    dst_uv_plane_desc.mem_object   = (*cl_mem_out_ptr)();

    cl_mem dst_uv_plane = clCreateImage(
            cl_ctx(),
            CL_MEM_READ_WRITE,
            &dst_uv_plane_format,
            &dst_uv_plane_desc,
            NULL,
            err_code
    );
    if (*err_code != CL_SUCCESS)
    {
        std::cerr << "Error " << *err_code << " with clCreateImage for dst image uv plane." << "\n";
        std::exit(*err_code);
    }
    
    *y_plane = dst_y_plane;
    *uv_plane = dst_uv_plane;

    return 0;
}


int Misp::LoadKernelFromString(const std::string & kernel_string, std::string kernel_name, cl::Kernel & kernel, std::string compiler_opts)
{
    //std::cout << "Loading kernel:  " << kernel_name << " from string" <<std::endl;

    //see if the program has already been built
    ProgramMap::iterator it = cl_programs.find(kernel_name);

    if (it == cl_programs.end()) {
        //std::cout << "Building program  " <<std::endl;
        cl::Program new_program(gpu_context, cl::Program::Sources(1, std::make_pair(kernel_string.c_str(), kernel_string.size()) ));

        try {
            new_program.build(devices, compiler_opts.c_str()); //"-cl-fast-relaxed-math"
        } catch (const cl::Error&) {
            std::cerr
            << "OpenCL compilation error" << std::endl
            << new_program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(gpu_device)
            << std::endl;
            return -1;
        }

        cl::Kernel new_kernel(new_program, kernel_name.c_str());
        kernel = new_kernel;

        //store the compiled program for re-use
        cl_programs.insert({kernel_name,new_program});
    }
    else {
        //std::cout << "Using cached program  " <<std::endl;
        cl::Kernel new_kernel(it->second, kernel_name.c_str());
        kernel = new_kernel;
    }

    //std::cout << "Succesfully loaded kernel:  " << kernel_name <<std::endl;

    return 0;
}

int Misp::LoadKernelFromFile(std::string opencl_kernel_file, std::string kernel_name, cl::Kernel & kernel, std::string compiler_opts)
{
    std::cout << "Loading kernel:  " << kernel_name <<std::endl;
    std::string gpu_code_str;
    if (LoadFileToString(opencl_kernel_file, gpu_code_str))
    {
        std::cerr << "Could not read gpu source file " << std::string(opencl_kernel_file) << std::endl;
        return -1;
    }

    cl::Program new_program(gpu_context, cl::Program::Sources(1, std::make_pair(gpu_code_str.c_str(), gpu_code_str.size()) ));

    try {
        new_program.build(devices, compiler_opts.c_str()); //"-cl-fast-relaxed-math"
    } catch (const cl::Error&) {
        std::cerr
        << "OpenCL compilation error" << std::endl
        << new_program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(gpu_device)
        << std::endl;
        return -1;
    }

    cl::Kernel new_kernel(new_program, kernel_name.c_str());
    kernel = new_kernel;

    std::cout << "Succesfully loaded kernel:  " << kernel_name <<std::endl;

    return 0;
}

int Misp::LoadFileToString(std::string filename, std::string& s)
{
  size_t size;
  char*  str;
  std::ifstream f(filename, std::ifstream::in | std::ifstream::binary);
  if(f.is_open())
  {
    size_t fileSize;
    f.seekg(0, std::fstream::end);
    size = fileSize = (size_t)f.tellg();
    f.seekg(0, std::fstream::beg);
    str = new char[size+1];
    if(!str)
    {
      f.close();
      return 0;
    }

    f.read(str, fileSize);
    f.close();
    str[size] = '\0';
    s = std::string(str, str+size);
    delete[] str;
    return 0;
  }
  std::cout<<"Error: failed to open file:"<<filename<<std::endl;
  return -1;
}

int64_t Misp::GetTimeUs()
{
    struct timespec ts;
    if(clock_gettime(CLOCK_MONOTONIC, &ts)){
        std::cerr << "ERROR calling clock_gettime"<<std::endl;
        return -1;
    }
    return (int64_t)ts.tv_sec*1000000 + (int64_t)ts.tv_nsec/1000;
}


int Misp::SetAwbManualGains(float r, float g, float b)
{
    if (r<0.0 || g < 0.0 || b < 0.0){
        std::cout<<"MISP error setting AWB gains: "<<r<<","<<g<<","<<b<<","<<std::endl;
        return -1;
    }

    if (r==0.0 && g == 0.0 && b == 0.0){
        awb_mode = "auto";
        //awb_gains[0] = 1.0;
        //awb_gains[1] = 1.0;
        //awb_gains[2] = 1.0;
        awb_counter = 0;
        return 0;
    }

    awb_mode = "manual";
    awb_gains[0] = r;
    awb_gains[1] = g;
    awb_gains[2] = b;

    return 0;
}


int Misp::SetGammaCorrection(float gamma_corr)
{
    if (gamma_corr< 0.0){
        std::cout<<"MISP error setting gamma correction: "<<gamma_corr<<std::endl;
        return -1;
    }

    gamma_correction = gamma_corr;

    return 0;
}

int Misp::SetPIP(void * img, int width, int height, int stride, float scale)
{
    memcpy(scratch_full_buffer->vaddress,img,stride*height);

    pip_src_width  = width;
    pip_src_height = height;
    pip_src_stride = stride;

    return 0;
}


int Misp::ComputeAWB(void * dst_host_ptr, int32_t dst_fd, uint32_t dst_size, uint32_t dst_width, uint32_t dst_height, uint32_t dst_line_stride, uint32_t dst_plane_stride)
{
    awb_counter++;

    if (awb_counter < 15 || awb_counter > 45){
        return 0;
    }
    int64_t tstart = GetTimeUs();
    uint32_t y_acc = 0;
    uint32_t u_acc = 0;
    uint32_t v_acc = 0;

    float Yav = 0;
    float Uav = 0;
    float Vav = 0;

    //compute average Y
    uint8_t * y_ptr = (uint8_t *)dst_host_ptr;
    for (unsigned int i=0; i<dst_height; i++){
        uint8_t * src = y_ptr + dst_line_stride * i;
        for (unsigned int jj=0; jj<dst_width; jj++){
            y_acc += *src++;
        }
    }

    //compute average U and V
    uint8_t * uv_ptr = (uint8_t *)dst_host_ptr + dst_plane_stride*dst_line_stride;
    for (unsigned int i=0; i<dst_height/2; i++){
        uint8_t * src = uv_ptr + dst_line_stride * i;
        for (unsigned int jj=0; jj<dst_width; jj+=2){
            u_acc += *src++;
            v_acc += *src++;
        }
    }

    Yav = (double)(y_acc) / (dst_width*dst_height) * 4.0;
    Uav = ( ((double)(u_acc) / (dst_width*dst_height/4) ) - 128 ) * 4.0;
    Vav = ( ((double)(v_acc) / (dst_width*dst_height/4) ) - 128 ) * 4.0;

    float du = -Uav;
    float dv = -Vav;
    float a = -0.168736 / 1024.0;
    //float b = -0.331264 / 1024.0;
    float c = 0.5 / 1024.0;
    float d = 0.5 / 1024.0;
    //float e = -0.418688 / 1024.0;
    float f = -0.081312 / 1024.0;
    float dR = (dv - f*du/c) / (d-a*f/c);
    float dB = (du-a*dR)/c;

    float Rav = (Yav + 1.402 * Vav) * 1024.0;
    //float Gav = (Yav - 0.114 * 1.772 / 0.587 * Uav - 0.299 * 1.402 / 0.587 * Vav) * 1024.0;
    float Bav = (Yav + 1.772 * Uav) * 1024.0;

    float gR = (Rav+dR)/Rav;
    float gG = 1.0;
    float gB = (Bav+dB)/Bav;

/*
    float min_val = gR;
    if (gG < min_val) min_val = gG;
    if (gB < min_val) min_val = gB;

    if (min_val < 1.0){
        gR /= min_val;
        gG /= min_val;
        gB /= min_val;
    }
*/

    awb_gains[0] = awb_gains[0] * gR;
    awb_gains[1] = awb_gains[1] * gG;
    awb_gains[2] = awb_gains[2] * gB;

    float min_val = awb_gains[0];
    if (awb_gains[1] < min_val) min_val = awb_gains[1];
    if (awb_gains[2] < min_val) min_val = awb_gains[2];

    awb_gains[0] /= min_val;
    awb_gains[1] /= min_val;
    awb_gains[2] /= min_val;

    

    //const float max_gain = 2.5;
    //if (awb_gains[0] > max_gain) awb_gains[0] = max_gain;
    //if (awb_gains[1] > max_gain) awb_gains[1] = max_gain;
    //if (awb_gains[2] > max_gain) awb_gains[2] = max_gain;

    int64_t tstop  = GetTimeUs();
    printf("MISP AWB: average YUV : %f, %f, %f... gains : %f %f %f, t=%dus\n",Yav,Uav,Vav,awb_gains[0],awb_gains[1],awb_gains[2],(int32_t)(tstop-tstart));
    //printf("average YUV : %f, %f, %f... gains : %f %f %f, t=%dus\n",Yav,Uav,Vav,gR, gG, gB,(int32_t)(tstop-tstart));

    return 0;
}


std::vector<std::string> split_string(std::string input)
{
    std::vector<std::string> result;
    const char * str = input.c_str();
    const char c     = ' ';

    do
    {
        const char *begin = str;

        while(*str != c && *str)
            str++;

        result.push_back(std::string(begin, str));
    } while (0 != *str++);

    return result;
}

int Misp::HandleControlCommand(std::string cmd)
{
    if (!(cmd.find("misp") != std::string::npos)){
        return -1;
    }

    std::vector<std::string> ss = split_string(cmd);
    /*
    for (auto s : ss){
        M_PRINT("%s\n",s.c_str());
    }*/

    if (ss.size() >= 2 && ss[0] == "set_misp_zoom"){
        float zoom_val = std::stof(ss[1]);
        if (zoom_val<0.0 || zoom_val > 100.0)
        {
            M_ERROR("set_misp_zoom cmd sanity check failed. zoom value = %f\n",zoom_val);
            return -1;
        }
        M_PRINT("setting misp zoom to %f\n", zoom_val);
        SetRoiZoom(zoom_val);

        if (ss.size() >= 3)
        {
            float alpha = std::stof(ss[2]);
            if (alpha<0.0 || alpha > 1.0)
            {
                M_ERROR("set_misp_zoom cmd sanity check failed. alpha value = %f\n",alpha);
                return -1;
            }
            roi_zoom_alpha = alpha;
        }
        else {
            roi_zoom_alpha = 0.9;
        }
    }

    //M_PRINT("Handled misp command: camera: %s, cmd: %s\n",camera_name.c_str(), cmd.c_str());
    return 0;
}
