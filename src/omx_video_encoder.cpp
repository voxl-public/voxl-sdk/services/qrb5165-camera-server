/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#include <OMX_Component.h>
#include <OMX_IndexExt.h>
#include <cstring>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/resource.h>
#include <sys/syscall.h>
#include <errno.h>
#include <system/graphics.h>
// #include <media/hardware/HardwareAPI.h>
#include <OMX_QCOMExtns.h>
#include <modal_journal.h>
#include <OMX_VideoExt.h>
#include <voxl_cutils.h>
#include <modal_pipe.h>

#include "omx_video_encoder.h"
#include "buffer_manager.h"
#include "common_defs.h"
#include "voxl_camera_server.h"

//#include <camera/CameraMetadata.h>     //ALOGE (to make gralloc_priv.h happy)
//#include <libhardware/gralloc_priv.h>  //private_handle_t

// stream gets more I frames per second
#define N_I_FRAMES_STREAM 2
#define NUM_OUTPUT_BUFFERS 16
#define OMX_SHARED_BUFFERS 1

#ifdef APQ8096
    const char* OMX_LIB_NAME = "/usr/lib/libOmxCore.so";
    const OMX_COLOR_FORMATTYPE   OMX_COLOR_FMT = OMX_QCOM_COLOR_FormatYVU420SemiPlanar;
#elif QRB5165
    const char* OMX_LIB_NAME = "/usr/lib/libmm-omxcore.so";
    const OMX_COLOR_FORMATTYPE   OMX_COLOR_FMT = OMX_COLOR_FormatYUV420SemiPlanar;
#endif

///<@todo Make these functions
#define Log2(number, power) {OMX_U32 temp = number; power = 0; while ((0 == (temp & 0x1)) && power < 16) {temp >>=0x1; power++;}}
#define FractionToQ16(q,num,den) { OMX_U32 power; Log2(den,power); q = num << (16 - power); }

static const int32_t OMXSpecVersion = 0x00000101;

// Helper MACRO to reset the size, version of any OMX structure
#define OMX_RESET_STRUCT_SIZE_VERSION(_structPointer_, _structName_)    \
    (_structPointer_)->nSize = sizeof(_structName_);                    \
    (_structPointer_)->nVersion.nVersion = OMXSpecVersion

// Helper MACRO to reset any OMX structure to its default valid state
#define OMX_RESET_STRUCT(_structPointer_, _structName_)     \
    memset((_structPointer_), 0x0, sizeof(_structName_));   \
    (_structPointer_)->nSize = sizeof(_structName_);        \
    (_structPointer_)->nVersion.nVersion = OMXSpecVersion


// Main thread functions for providing input buffer to the OMX input port and processing encoded buffer on the OMX output port
void* ThreadProcessOMXInputPort(void* data);
void* ThreadProcessOMXOutputPort(void* data);

// Function called by the OMX component for event handling
OMX_ERRORTYPE OMXEventHandler(OMX_IN OMX_HANDLETYPE hComponent,
                              OMX_IN OMX_PTR        pAppData,
                              OMX_IN OMX_EVENTTYPE  eEvent,
                              OMX_IN OMX_U32        nData1,
                              OMX_IN OMX_U32        nData2,
                              OMX_IN OMX_PTR        pEventData);

// Function called by the OMX component to indicate the input buffer we passed to it has been consumed
OMX_ERRORTYPE OMXEmptyBufferHandler(OMX_IN OMX_HANDLETYPE        hComponent,
                                    OMX_IN OMX_PTR               pAppData,
                                    OMX_IN OMX_BUFFERHEADERTYPE* pBuffer);

// Function called by the OMX component to hand over the encoded output buffer
OMX_ERRORTYPE OMXFillHandler(OMX_OUT OMX_HANDLETYPE        hComponent,
                             OMX_OUT OMX_PTR               pAppData,
                             OMX_OUT OMX_BUFFERHEADERTYPE* pBuffer);

// Investigate build-time linking to omx instead of this mess
typedef OMX_ERRORTYPE (*OMXGetHandleFunc)(OMX_OUT OMX_HANDLETYPE* handle,
                                          OMX_IN OMX_STRING componentName,
                                          OMX_IN OMX_PTR appData,
                                          OMX_IN OMX_CALLBACKTYPE* callBacks);
typedef OMX_ERRORTYPE (*OMXFreeHandleFunc)(OMX_IN OMX_HANDLETYPE hComp);

static OMX_ERRORTYPE (*OMXInit)(void);
static OMX_ERRORTYPE (*OMXDeinit)(void);
static OMXGetHandleFunc OMXGetHandle;
static OMXFreeHandleFunc OMXFreeHandle;

static void __attribute__((constructor)) setupOMXFuncs()
{

    void *OmxCoreHandle = dlopen(OMX_LIB_NAME, RTLD_NOW);

    OMXInit =   (OMX_ERRORTYPE (*)(void))dlsym(OmxCoreHandle, "OMX_Init");
    OMXDeinit = (OMX_ERRORTYPE (*)(void))dlsym(OmxCoreHandle, "OMX_Deinit");
    OMXGetHandle =     (OMXGetHandleFunc)dlsym(OmxCoreHandle, "OMX_GetHandle");
    OMXFreeHandle =   (OMXFreeHandleFunc)dlsym(OmxCoreHandle, "OMX_FreeHandle");
}


// -----------------------------------------------------------------------------------------------------------------------------
// Constructor
// -----------------------------------------------------------------------------------------------------------------------------
VideoEncoder::VideoEncoder(VideoEncoderConfig* PVideoEncoderConfig)
{
    pthread_mutex_init(&out_mutex, NULL);

    pthread_condattr_t attr;
    pthread_condattr_init(&attr);
    pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);
    pthread_cond_init(&out_cond, &attr);
    pthread_condattr_destroy(&attr);

    m_VideoEncoderConfig = *PVideoEncoderConfig;
    m_outputPipe        = m_VideoEncoderConfig.outputPipe;
    m_inputBufferSize   = 0;
    m_inputBufferCount  = 0;
    m_outputBufferSize  = 0;
    m_outputBufferCount = 0;

    m_nextInputBufferIndex  = 0;
    m_nextOutputBufferIndex = 0;

    if(OMXInit()){
        M_ERROR("OMX Init failed!\n");
        throw -EINVAL;
    }

    if(SetConfig()){
        M_ERROR("OMX Set config failed!\n");
        throw -EINVAL;
    }

    if(OMX_SendCommand(m_OMXHandle, OMX_CommandStateSet, (OMX_U32)OMX_StateExecuting, NULL)){
        M_ERROR("OMX Set state executing failed!\n");
        throw -EINVAL;
    }

    // We do this so that the OMX component has output buffers to fill the encoded frame data. The output buffers are
    // recycled back to the OMX component once they are returned to us and after we write the frame content to the
    // video file
    for(uint32_t i = 0;  i < m_outputBufferCount; i++){
        if(OMX_FillThisBuffer(m_OMXHandle, m_ppOutputBuffers[i])){
            M_ERROR("OMX Fill buffer: %d failed!\n", i);
            throw -EINVAL;
        }
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Destructor
// -----------------------------------------------------------------------------------------------------------------------------
VideoEncoder::~VideoEncoder()
{
   return;

}


// -----------------------------------------------------------------------------------------------------------------------------
// This configures the OMX component i.e. the video encoder's input, output ports and all its parameters and gets it into a
// ready to use state. After this function we can start sending input buffers to the video encoder and it will start sending
// back the encoded frames
// -----------------------------------------------------------------------------------------------------------------------------
OMX_ERRORTYPE VideoEncoder::SetConfig(void)
{
    OMX_ERRORTYPE ret;
    char* pComponentName;
    OMX_CALLBACKTYPE                 callbacks = {OMXEventHandler, OMXEmptyBufferHandler, OMXFillHandler};
    OMX_COLOR_FORMATTYPE             omxFormat = OMX_COLOR_FormatMax;
    OMX_VIDEO_PARAM_PORTFORMATTYPE   videoPortFmt;

    m_pHALInputBuffers = m_VideoEncoderConfig.inputBuffers;
    OMX_RESET_STRUCT(&videoPortFmt, OMX_VIDEO_PARAM_PORTFORMATTYPE);

    if(m_VideoEncoderConfig.venc_config.mode == VENC_H265){
        pComponentName = (char *)"OMX.qcom.video.encoder.hevc";
    }else{
        pComponentName = (char *)"OMX.qcom.video.encoder.avc";
    }

    ret = OMXGetHandle(&m_OMXHandle, pComponentName, this, &callbacks);
    if(ret){
        M_ERROR("OMX Get handle failed!\n");
        _print_omx_error(ret);
        return OMX_ErrorUndefined;
    }

    if(m_VideoEncoderConfig.format != HAL_PIXEL_FORMAT_YCbCr_420_888){
        M_ERROR("OMX Unknown video recording format!\n");
        return OMX_ErrorBadParameter;
    }

    omxFormat = OMX_COLOR_FMT;
    bool isFormatSupported = false;
    // Check if OMX component supports the input frame format
    OMX_S32 index = 0;

    M_DEBUG("Available color formats for OMX:\n");

    while (!OMX_GetParameter(m_OMXHandle, OMX_IndexParamVideoPortFormat, (OMX_PTR)&videoPortFmt)){
        videoPortFmt.nPortIndex = PortIndexIn;
        videoPortFmt.nIndex     = index;
        if (videoPortFmt.eColorFormat == omxFormat) isFormatSupported = true;
        M_DEBUG("\t%s (0x%x)\n", colorFormatStr(videoPortFmt.eColorFormat), videoPortFmt.eColorFormat);
        index++;
    }

    if (!isFormatSupported){
        M_ERROR("OMX unsupported video input format: %s\n", colorFormatStr(omxFormat));
        return OMX_ErrorBadParameter;
    }

    OMX_U32 bps = m_VideoEncoderConfig.venc_config.mbps*1000000;

    ////////////////////////////////////////////////////////////////////////////
    // OMX_VIDEO_PARAM_BITRATETYPE && OMX_VIDEO_CONFIG_BITRATETYPE
    // both need to be set
    ////////////////////////////////////////////////////////////////////////////
    OMX_VIDEO_PARAM_BITRATETYPE paramBitRate;
    OMX_RESET_STRUCT_SIZE_VERSION(&paramBitRate, OMX_VIDEO_PARAM_BITRATETYPE);
    paramBitRate.nPortIndex = PortIndexOut;

    ret = OMX_GetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_IndexParamVideoBitrate, (OMX_PTR)&paramBitRate);
    if(ret){
        M_ERROR("OMX_GetParameter of OMX_IndexParamVideoBitrate failed\n");
        _print_omx_error(ret);
        return OMX_ErrorUndefined;
    }

    // constant Q mode
    if(m_VideoEncoderConfig.venc_config.br_ctrl == VENC_CONTROL_CQP){
        paramBitRate.eControlRate   = OMX_Video_ControlRateDisable;
        paramBitRate.nTargetBitrate = 0;
    }
    // constant bitrate mode
    else if(m_VideoEncoderConfig.venc_config.br_ctrl == VENC_CONTROL_CBR){
        //for CBR, the correct param is OMX_Video_ControlRateVariable (OMX_Video_ControlRateConstant does not work)
        paramBitRate.eControlRate   = OMX_Video_ControlRateVariable;
        paramBitRate.nTargetBitrate = bps;
    }
    else{
        M_ERROR("unknown control rate %d\n", m_VideoEncoderConfig.venc_config.br_ctrl);
        return OMX_ErrorUndefined;
    }
    ret = OMX_SetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_IndexParamVideoBitrate, (OMX_PTR)&paramBitRate);
    if(ret){
        M_ERROR("OMX_SetParameter of OMX_IndexParamVideoBitrate failed for out index!\n");
        _print_omx_error(ret);
        return OMX_ErrorUndefined;
    }

    OMX_VIDEO_CONFIG_BITRATETYPE configBitRate;
    OMX_RESET_STRUCT_SIZE_VERSION(&configBitRate, OMX_VIDEO_CONFIG_BITRATETYPE);
    configBitRate.nPortIndex = PortIndexOut;
    configBitRate.nEncodeBitrate = bps;
    ret = OMX_SetConfig(m_OMXHandle, (OMX_INDEXTYPE)(OMX_IndexConfigVideoBitrate), (OMX_PTR)&configBitRate);
    if(ret){
        M_ERROR("OMX_SetParameter of OMX_IndexConfigVideoBitrate failed!\n");
        _print_omx_error(ret);
        return OMX_ErrorUndefined;
    }

    ////////////////////////////////////////////////////////////////////////////
    // set h264/h265 params
    ////////////////////////////////////////////////////////////////////////////
    if(m_VideoEncoderConfig.venc_config.mode == VENC_H264){

        if (bps < 1500000){
            m_fpsHackScale = 1500000.0 / bps;
            M_WARN("OMX SetTargetBitrate: H264 CBR requires bps >= 1.5Mbit (%d bps provided). Using FPS hack. scale = %f\n",bps, m_fpsHackScale);
        }

        OMX_VIDEO_AVCPROFILETYPE h264_profile = OMX_VIDEO_AVCProfileHigh; //OMX_VIDEO_AVCProfileMain // OMX_VIDEO_AVCProfileBaseline;
        OMX_VIDEO_AVCLEVELTYPE   h264_level   = OMX_VIDEO_AVCLevel1;      //CBR only works with level1, but this is not true level1, as h264 level is 128×96@30

        OMX_VIDEO_PARAM_PROFILELEVELTYPE profile;
        OMX_RESET_STRUCT(&profile, OMX_VIDEO_PARAM_PROFILELEVELTYPE);
        profile.nPortIndex = PortIndexOut;

        //not sure if this is strictly required, but gstreamer sets this
        ret = OMX_GetParameter(m_OMXHandle, OMX_IndexParamVideoProfileLevelCurrent, (OMX_PTR)&profile);
        if(ret){
            M_ERROR("OMX Get parameter of OMX_IndexParamVideoProfileLevelCurrent failed\n");
            _print_omx_error(ret);
            return OMX_ErrorUndefined;
        }

        profile.eProfile = h264_profile;
        profile.eLevel   = h264_level;

        ret = OMX_SetParameter(m_OMXHandle, OMX_IndexParamVideoProfileLevelCurrent, (OMX_PTR)&profile);
        if(ret){
            M_ERROR("OMX_SetParameter of OMX_IndexParamVideoProfileLevelCurrent failed!\n");
            _print_omx_error(ret);
            return OMX_ErrorUndefined;
        }

        ////////////////////////////////////////////////////////////////////////
        // set OMX_VIDEO_PARAM_AVCTYPE
        // start with platform defaults and work from there, don't erase
        ////////////////////////////////////////////////////////////////////////
        OMX_VIDEO_PARAM_AVCTYPE avc;
        OMX_RESET_STRUCT(&avc, OMX_VIDEO_PARAM_AVCTYPE);
        avc.nPortIndex = PortIndexOut;
        ret = OMX_GetParameter(m_OMXHandle, OMX_IndexParamVideoAvc, (OMX_PTR)&avc);
        if(ret){
            M_ERROR("OMX Get parameter of OMX_IndexParamVideoAvc failed\n");
            _print_omx_error(ret);
            return OMX_ErrorUndefined;
        }

        avc.eProfile = h264_profile;
        avc.eLevel   = h264_level;

        // defaults from qcom open source omx/v4l2 android implementation
        avc.nPFrames              = m_VideoEncoderConfig.venc_config.nPframes;
        avc.nBFrames              = 0;
        avc.bUseHadamard          = OMX_TRUE;  //Enable/disable Hadamard transform
        avc.nRefFrames            = 1;
        avc.nRefIdx10ActiveMinus1 = 1;
        avc.nRefIdx11ActiveMinus1 = 0;
        avc.bEnableUEP            = OMX_FALSE;  //Enable/disable unequal error protection. This is only valid of data partitioning is enabled
        avc.bEnableFMO            = OMX_FALSE;  //Enable/disable flexible macroblock ordering
        avc.bEnableASO            = OMX_FALSE;  //Enable/disable arbitrary slice ordering
        avc.bEnableRS             = OMX_FALSE;  //Enable/disable sending of redundant slices
        avc.nAllowedPictureTypes  = OMX_VIDEO_PictureTypeI | OMX_VIDEO_PictureTypeP;  //Specifies the picture types allowed in the bitstream
        avc.bFrameMBsOnly         = OMX_FALSE;   //specifies that every coded picture of the coded video sequence is a coded frame containing only frame macroblocks
        avc.bMBAFF                = OMX_FALSE;  //Enable/disable switching between frame and field macroblocks within a picture
        avc.bEntropyCodingCABAC   = OMX_FALSE; //only available in Main or High profile
        if (avc.eProfile == OMX_VIDEO_AVCProfileMain || avc.eProfile == OMX_VIDEO_AVCProfileHigh){
            avc.bEntropyCodingCABAC  = OMX_TRUE;
        }
        avc.bWeightedPPrediction  = OMX_TRUE;
        avc.nWeightedBipredicitonMode = 0;
        avc.bconstIpred           = OMX_FALSE;  //Enable/disable intra prediction
        avc.bDirect8x8Inference   = OMX_TRUE; //Specifies the method used in the derivation process for luma motion vectors for B_Skip, B_Direct_16x16 and B_Direct_8x8 as specified in subclause 8.4.1.2 of the AVC spec
        avc.bDirectSpatialTemporal = OMX_FALSE;  //Flag indicating spatial or temporal direct mode used in B slice coding (related to bDirect8x8Inference)
        avc.nCabacInitIdc         = 0;  //Index used to init CABAC contexts
        avc.eLoopFilterMode       = OMX_VIDEO_AVCLoopFilterEnable;  //Enable/disable loop filter
        // avc.nSliceHeaderSpacing       = 1024; // Number of macroblocks between slice header, put zero if not used //don't know where this came from
        avc.nSliceHeaderSpacing   = 0; // This needs to be 0 for OpenHD, otherwise OHD will have issues decoding. 
                                           // From OMX spec sheet: nSliceHeaderSpacing is the number of macroblocks in a slice. This value is set to 0x0 when not used.

        // other defaults from somewhere, probably thundercomm example
        // avc.nBFrames                  = 0;
        // avc.bUseHadamard              = OMX_TRUE;
        // avc.nRefFrames                = 2;
        // avc.nRefIdx10ActiveMinus1     = 0;
        // avc.nRefIdx11ActiveMinus1     = 0;
        // avc.bEnableUEP                = OMX_FALSE;
        // avc.bEnableFMO                = OMX_FALSE;
        // avc.bEnableASO                = OMX_FALSE;
        // avc.bEnableRS                 = OMX_FALSE;
        // avc.nAllowedPictureTypes      =
        // avc.bFrameMBsOnly             = OMX_TRUE;
        // avc.bMBAFF                    = OMX_FALSE;
        // avc.bWeightedPPrediction      = OMX_TRUE;
        // avc.bconstIpred               = OMX_TRUE;
        // avc.bDirect8x8Inference       = OMX_TRUE;
        // avc.bDirectSpatialTemporal    = OMX_TRUE;
        // avc.eLoopFilterMode           = OMX_VIDEO_AVCLoopFilterEnable;
        // avc.bEntropyCodingCABAC       = OMX_TRUE;
        // avc.nCabacInitIdc             = 1;

        ret = OMX_SetParameter(m_OMXHandle, OMX_IndexParamVideoAvc, (OMX_PTR)&avc);
        if(ret){
            M_ERROR("OMX_SetParameter of OMX_IndexParamVideoAvc failed!\n");
            _print_omx_error(ret);
            return OMX_ErrorUndefined;
        }
    }

    // Configure for H265
    else if(m_VideoEncoderConfig.venc_config.mode == VENC_H265)
    {
        if (bps < 3000000){
            m_fpsHackScale = 3000000.0 / bps;
            M_WARN("OMX SetTargetBitrate: H265 CBR requires bps >= 3.0Mbit (%d bps provided). Using FPS hack. scale = %f\n",bps, m_fpsHackScale);
        }

        OMX_VIDEO_PARAM_PROFILELEVELTYPE profile;
        OMX_RESET_STRUCT(&profile, OMX_VIDEO_PARAM_PROFILELEVELTYPE);
        profile.nPortIndex = PortIndexOut;

        ret = OMX_GetParameter(m_OMXHandle, OMX_IndexParamVideoProfileLevelCurrent, (OMX_PTR)&profile);
        if(ret){
            M_ERROR("OMX Get parameter of OMX_IndexParamVideoProfileLevelCurrent failed\n");
            _print_omx_error(ret);
            return OMX_ErrorUndefined;
        }

        profile.eProfile = OMX_VIDEO_HEVCProfileMain;
        profile.eLevel   = OMX_VIDEO_HEVCHighTierLevel5;

        ret = OMX_SetParameter(m_OMXHandle, OMX_IndexParamVideoProfileLevelCurrent, (OMX_PTR)&profile);
        if(ret){
            M_ERROR("OMX_SetParameter of OMX_IndexParamVideoProfileLevelCurrent failed!\n");
            _print_omx_error(ret);
            return OMX_ErrorUndefined;
        }


        ////////////////////////////////////////////////////////////////////////
        // set OMX_VIDEO_PARAM_HEVCTYPE
        ////////////////////////////////////////////////////////////////////////
        OMX_VIDEO_PARAM_HEVCTYPE hevc;
        OMX_RESET_STRUCT(&hevc, OMX_VIDEO_PARAM_HEVCTYPE);
        hevc.nPortIndex = PortIndexOut;
        ret = OMX_GetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_IndexParamVideoHevc, (OMX_PTR)&hevc);
        if(ret){
            M_ERROR("OMX_GetParameter of OMX_IndexParamVideoHevc failed!\n");
            _print_omx_error(ret);
            return OMX_ErrorUndefined;
        }

        //nKeyFrameInterval;        // distance between consecutive I-frames (including one
                                      // of the I frames). 0 means interval is unspecified and
                                      // can be freely chosen by the codec. 1 means a stream of
                                      // only I frames.
        hevc.nKeyFrameInterval = m_VideoEncoderConfig.venc_config.nPframes+1;
        hevc.eProfile          = OMX_VIDEO_HEVCProfileMain;
        hevc.eLevel            = OMX_VIDEO_HEVCHighTierLevel5;

        // TODO more params here? It seens HEVC does not have other params exposed..

        ret = OMX_SetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_IndexParamVideoHevc, (OMX_PTR)&hevc);
        if(ret){
            M_ERROR("OMX_SetParameter of OMX_IndexParamVideoHevc failed!\n");
            return OMX_ErrorUndefined;
        }

    }
    else{
        M_ERROR("Unsupported coding type!\n");
        return OMX_ErrorBadParameter;
    }


    ////////////////////////////////////////////////////////////////////////////
    // OMX_CONFIG_FRAMERATETYPE
    ////////////////////////////////////////////////////////////////////////////
    OMX_CONFIG_FRAMERATETYPE framerate;
    OMX_RESET_STRUCT_SIZE_VERSION(&framerate, OMX_CONFIG_FRAMERATETYPE);
    framerate.nPortIndex = PortIndexOut;
    // FractionToQ16(framerate.xEncodeFramerate, (int)(m_VideoEncoderConfig.frameRate * 2), 2);
    framerate.xEncodeFramerate = m_VideoEncoderConfig.frameRate << 16;
    ret = OMX_SetConfig(m_OMXHandle, OMX_IndexConfigVideoFramerate, (OMX_PTR)&framerate);
    if(ret){
        M_ERROR("OMX_SetConfig of OMX_IndexConfigVideoFramerate failed!\n");
        _print_omx_error(ret);
        return OMX_ErrorUndefined;
    }


    ////////////////////////////////////////////////////////////////////////
    // set QOMX_EXTNINDEX_VIDEO_INITIALQP
    // bitrate still seems to fluctuate a lot on startup, but does it
    // slightly less when starting with the max compression
    ////////////////////////////////////////////////////////////////////////
    QOMX_EXTNINDEX_VIDEO_INITIALQP initqp;
    OMX_RESET_STRUCT_SIZE_VERSION(&initqp, QOMX_EXTNINDEX_VIDEO_INITIALQP);
    initqp.nPortIndex = PortIndexOut;
    int qstart;
    if(m_VideoEncoderConfig.venc_config.br_ctrl==VENC_CONTROL_CQP){
        qstart = m_VideoEncoderConfig.venc_config.Qfixed;
    }else{
        qstart = (m_VideoEncoderConfig.venc_config.Qmin + m_VideoEncoderConfig.venc_config.Qmax)/2;
    }
    initqp.nQpI = qstart;
    initqp.nQpP = qstart;
    initqp.nQpB = qstart;
    initqp.bEnableInitQp = 0x7; // apply to all I, P, and B
    ret = OMX_SetParameter(m_OMXHandle, (OMX_INDEXTYPE)QOMX_IndexParamVideoInitialQp,(OMX_PTR)&initqp);
    if(ret){
        M_ERROR("%s Failed to set Initial QP parameter\n", __func__);
        _print_omx_error(ret);
        return ret;
    }



#ifndef APQ8096
    ////////////////////////////////////////////////////////////////////////
    // set OMX_QCOM_VIDEO_PARAM_IPB_QPRANGETYPE
    // standard OMX_VIDEO_PARAM_QUANTIZATIONTYPE doesn't work
    // this is the main quantization range that's respected!
    ////////////////////////////////////////////////////////////////////////
    OMX_QCOM_VIDEO_PARAM_IPB_QPRANGETYPE qp_range;
    OMX_RESET_STRUCT_SIZE_VERSION(&qp_range, OMX_QCOM_VIDEO_PARAM_IPB_QPRANGETYPE);
    qp_range.nPortIndex = PortIndexOut;
    int qmin, qmax;
    if(m_VideoEncoderConfig.venc_config.br_ctrl==VENC_CONTROL_CQP){
        qmin = m_VideoEncoderConfig.venc_config.Qfixed;
        qmax = m_VideoEncoderConfig.venc_config.Qfixed;
    }else{
        qmin = m_VideoEncoderConfig.venc_config.Qmin;
        qmax = m_VideoEncoderConfig.venc_config.Qmax;
    }
    qp_range.minIQP = qmin;
    qp_range.maxIQP = qmax;
    qp_range.minPQP = qmin;
    qp_range.maxPQP = qmax;
    qp_range.minBQP = qmin;
    qp_range.maxBQP = qmax;
    ret = OMX_SetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_QcomIndexParamVideoIPBQPRange, (OMX_PTR)&qp_range);
    if(ret){
        M_ERROR("%s Failed to set Q Range parameter\n", __func__);
        _print_omx_error(ret);
        return ret;
    }
#endif



    ////////////////////////////////////////////////////////////////////////////
    // QOMX_EXTNINDEX_VIDEO_LOW_LATENCY_MODE
    // https://android.googlesource.com/platform/hardware/qcom/sm7250/media/+/0aef9b5a7fda17e3fac441b565cd1fb4e37df0ff/mm-video-v4l2/vidc/venc/src/omx_video_extensions.hpp
    ////////////////////////////////////////////////////////////////////////////
#ifndef APQ8096
    //WARNING: enabling low latency mode causes CBR mode not to work properly
    if (m_VideoEncoderConfig.venc_config.br_ctrl==VENC_CONTROL_CQP){
        QOMX_EXTNINDEX_VIDEO_LOW_LATENCY_MODE lowLatency;
        OMX_RESET_STRUCT_SIZE_VERSION(&lowLatency, QOMX_EXTNINDEX_VIDEO_LOW_LATENCY_MODE);
        lowLatency.bEnableLowLatencyMode = OMX_TRUE;
        ret = OMX_SetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_QTIIndexParamLowLatencyMode, (OMX_PTR)&lowLatency);
        if(ret){
            M_ERROR("%s Failed to set Low Latency parameter\n", __func__);
            _print_omx_error(ret);
            return ret;
        }
    }
#endif


#ifndef APQ8096
/*
    ////////////////////////////////////////////////////////////////////////////
    // Configure Bitrate Savings (CAC)
    // https://android.googlesource.com/platform/hardware/qcom/sm7250/media/+/0aef9b5a7fda17e3fac441b565cd1fb4e37df0ff/mm-video-v4l2/vidc/venc/src/omx_video_extensions.hpp
    // encoder bitrate savings control (CAC) - content adaptive coding feature
    // it is bitmask.
    //  0x0: disable CAC
    //  0x1: 8 bit: enable.  10 bit: disable
    //  0x2: 8 bit: disable. 10 bit: enable
    //  0x3: 8 bit: enable.  10 bit: enable
    ////////////////////////////////////////////////////////////////////////////
    //if not set, it will default to 3
    OMX_U32 adaptive_coding = 3; //if set to 0, CBR bit rate control does not work
    ret = OMX_SetConfig(m_OMXHandle, (OMX_INDEXTYPE)OMX_QTIIndexConfigContentAdaptiveCoding, (OMX_PTR)&adaptive_coding);
    if(ret){
        M_ERROR("%s Failed to set OMX adaptive coding\n", __func__);
        _print_omx_error(ret);
        return OMX_ErrorUndefined;
    }
*/
    QOMX_ENABLETYPE enInputQueue;
    OMX_RESET_STRUCT_SIZE_VERSION(&enInputQueue, QOMX_ENABLETYPE);
    enInputQueue.bEnable = OMX_TRUE;
    ret = OMX_SetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_QcomIndexParamVencControlInputQueue, (OMX_PTR)&enInputQueue);
    if(ret){
        M_ERROR("%s Failed to set enable input queue parameter\n", __func__);
        _print_omx_error(ret);
        return ret;
    }
#endif



    // Set/Get input port parameters
    // Must be called after the codec params, otherwise CBR does not work
    if (SetPortParams((OMX_U32)PortIndexIn,
                      (OMX_U32)(m_VideoEncoderConfig.inputBuffers->totalBuffers),
                      (OMX_U32*)&m_inputBufferSize,
                      (OMX_U32*)&m_inputBufferCount,
                      omxFormat))
    {
        M_ERROR("OMX SetPortParams of PortIndexIn failed!\n");
        return OMX_ErrorUndefined;
    }

    // Set/Get output port parameters
    if (SetPortParams((OMX_U32)PortIndexOut,
                      (OMX_U32)NUM_OUTPUT_BUFFERS,
                      (OMX_U32*)&m_outputBufferSize,
                      (OMX_U32*)&m_outputBufferCount,
                      omxFormat))
    {
        M_ERROR("OMX SetPortParams of PortIndexOut failed!\n");
        return OMX_ErrorUndefined;
    }


    ////////////////////////////////////////////////////////////////////////////
    // Enable the port!
    ////////////////////////////////////////////////////////////////////////////
    ret = OMX_SendCommand(m_OMXHandle, OMX_CommandPortEnable, PortIndexIn, NULL);
    if(ret){
        M_ERROR("OMX failed to send enable command to in port\n");
        _print_omx_error(ret);
        return ret;
    }

    // Encoder has to be switched to Idle state before allocating buffers
    if (OMX_SendCommand(m_OMXHandle, OMX_CommandStateSet, (OMX_U32)OMX_StateIdle, NULL))
    {
        M_ERROR("------voxl-camera-server ERROR: OMX_SendCommand OMX_StateIdle failed\n");\
        return OMX_ErrorUndefined;
    }

    // Allocate input / output port buffers
    m_ppInputBuffers  = (OMX_BUFFERHEADERTYPE **)malloc(sizeof(OMX_BUFFERHEADERTYPE *) * m_inputBufferCount);
    m_ppOutputBuffers = (OMX_BUFFERHEADERTYPE **)malloc(sizeof(OMX_BUFFERHEADERTYPE *) * m_outputBufferCount);

    if ((m_ppInputBuffers == NULL) || (m_ppOutputBuffers == NULL))
    {
        M_ERROR("OMX Allocate OMX_BUFFERHEADERTYPE ** failed\n");
        return OMX_ErrorUndefined;
    }

    for (uint32_t i = 0; i < m_inputBufferCount; i++)
    {
        if(!m_VideoEncoderConfig.inputBuffers->bufferBlocks[i].vaddress)
        {
            M_WARN("Encoder expecting(%d) more buffers than module allocated(%d)\n", m_inputBufferCount, i);
            return OMX_ErrorUndefined;
        }

#if QRB5165 && OMX_SHARED_BUFFERS
        /*
        //this is only needed if passing shared memory pointers in non metadata mode
        OMX_QCOM_PLATFORM_PRIVATE_PMEM_INFO bparam;
        bparam.pmem_fd = ((private_handle_t*)(m_VideoEncoderConfig.inputBuffers->buffers[i]))->fd;
        bparam.offset  = ((private_handle_t*)(m_VideoEncoderConfig.inputBuffers->buffers[i]))->offset;

        //call OXM_UseBuffer and provide the shared memory fd as app data
        if (int ret = OMX_UseBuffer (m_OMXHandle, &m_ppInputBuffers[i], PortIndexIn, &bparam, m_inputBufferSize,
                (OMX_U8*)m_VideoEncoderConfig.inputBuffers->bufferBlocks[i].vaddress))
        */

        //for metadata mode, we call OMX_AllocateBuffer, which will actually allocate metadata array only
        //we will save the Gralloc info into these metadata buffers
        if (int ret = OMX_AllocateBuffer (m_OMXHandle, &m_ppInputBuffers[i], PortIndexIn, NULL, m_inputBufferSize))
        {
            M_ERROR("OMX_UseBuffer on input buffer: %d failed\n", i);
            OMXEventHandler(NULL, NULL, OMX_EventError, ret, 1, NULL);
            return OMX_ErrorUndefined;
        }

        //OMX has given us an array of metabuffers in m_ppInputBuffers[]->pBuffer
        //this metabuffer needs to be filled with the Gralloc information
        VideoGrallocMetadata * meta = (VideoGrallocMetadata*)m_ppInputBuffers[i]->pBuffer;
        meta->buffer_type           = kMetadataBufferTypeGrallocSource;
        meta->meta_handle           = m_VideoEncoderConfig.inputBuffers->bufferBlocks[i].bo;

        //save the meta pointer so we can later use that to associate the OMX buffers in callbacks with actual data buffers
        m_VideoEncoderConfig.inputBuffers->bufferBlocks[i].meta_address = m_ppInputBuffers[i]->pBuffer;
#else
        // The OMX component i.e. the video encoder allocates the block, gets the memory from hal
        if (int ret = OMX_UseBuffer (m_OMXHandle, &m_ppInputBuffers[i], PortIndexIn, this, m_inputBufferSize,
                    (OMX_U8*)m_VideoEncoderConfig.inputBuffers->bufferBlocks[i].vaddress))
            {
                M_ERROR("OMX_UseBuffer on input buffer: %d failed\n", i);
                OMXEventHandler(NULL, NULL, OMX_EventError, ret, 1, NULL);
                return OMX_ErrorUndefined;
            }
#endif
    }

    for (uint32_t i = 0; i < m_outputBufferCount; i++)
    {
        // The OMX component i.e. the video encoder allocates the memory residing behind these buffers
        if (OMX_AllocateBuffer (m_OMXHandle, &m_ppOutputBuffers[i], PortIndexOut, this, m_outputBufferSize))
        {
            M_ERROR("OMX_AllocateBuffer on output buffer: %d failed\n", i);
            return OMX_ErrorUndefined;
        }
    }


    return OMX_ErrorNone;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function sets the input or output port parameters and gets the input or output port buffer sizes and count to allocate
// -----------------------------------------------------------------------------------------------------------------------------
OMX_ERRORTYPE VideoEncoder::SetPortParams(OMX_U32  portIndex,               ///< In or Out port
                                          OMX_U32  bufferCountMin,          ///< Minimum number of buffers
                                          OMX_U32* pBufferSize,             ///< Returned buffer size
                                          OMX_U32* pBufferCount,            ///< Returned number of buffers
                                          OMX_COLOR_FORMATTYPE inputFormat) ///< Image format on the input port
{
    OMX_ERRORTYPE ret;
    OMX_PARAM_PORTDEFINITIONTYPE sPortDef;
    OMX_RESET_STRUCT(&sPortDef, OMX_PARAM_PORTDEFINITIONTYPE);
    if ((pBufferSize == NULL) || (pBufferCount == NULL)){
        M_ERROR("OMX Buffer error : NULL pointer\n");
        return OMX_ErrorBadParameter;
    }

    // get the default parameters for this index (in or out)
    sPortDef.nPortIndex = portIndex;
    ret = OMX_GetParameter(m_OMXHandle, OMX_IndexParamPortDefinition, (OMX_PTR)&sPortDef);
    if(ret){
        M_ERROR("OMX_GetParameter OMX_IndexParamPortDefinition failed!\n");
        return ret;
    }

    // change port definition settings that are for both in and out
    sPortDef.format.video.xFramerate   = ((uint32_t)(m_VideoEncoderConfig.frameRate*m_fpsHackScale)) << 16;
    sPortDef.format.video.nFrameWidth  = m_VideoEncoderConfig.width;
    sPortDef.format.video.nFrameHeight = m_VideoEncoderConfig.height;
    // sPortDef.format.video.nStride      = m_VideoEncoderConfig.width;
 


    // set input/output specific settings
    if(portIndex == PortIndexIn){

           // cheap hack to get stride from GBM data
#ifdef QRB5165
    sPortDef.format.video.nStride = m_VideoEncoderConfig.inputBuffers->bufferBlocks[0].stride;
#else
    sPortDef.format.video.nStride = m_VideoEncoderConfig.width;
#endif
    sPortDef.bEnabled = OMX_TRUE;
    sPortDef.format.video.nSliceHeight = 1024; // TODO play with this

        sPortDef.format.video.eColorFormat = inputFormat;
        sPortDef.bBuffersContiguous = OMX_TRUE; // ???
        sPortDef.format.video.eCompressionFormat =  OMX_VIDEO_CodingUnused;

#if QRB5165 && OMX_SHARED_BUFFERS
        /*
        //this is only needed if passing shared memory pointers in non metadata mode
        //this actually sets m_use_input_pmem = true in omx_video_base.cpp
        OMXComponentCapabilityFlagsType flags;
        OMX_RESET_STRUCT(&flags, OMXComponentCapabilityFlagsType);

        //get parameter call actually sets m_use_input_pmem = true !!
        OMX_GetParameter(m_OMXHandle, OMX_COMPONENT_CAPABILITY_TYPE_INDEX, (OMX_PTR)&flags);
        */

        //enable metadata mode so that OMX will use our Gralloc buffers
        StoreMetaDataInBuffersParams buffer_mode;
        OMX_RESET_STRUCT(&buffer_mode,StoreMetaDataInBuffersParams);

        buffer_mode.nPortIndex     = portIndex;
        buffer_mode.bStoreMetaData = OMX_TRUE;

        ret = OMX_SetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_QcomIndexParamVideoMetaBufferMode, (OMX_PTR)&buffer_mode);
        if(ret){
            M_ERROR("OMX_SetParameter OMX_QcomIndexParamVideoMetaBufferMode failed!\n");
            return ret;
        }
#endif

    }
    else{
        sPortDef.format.video.eColorFormat =  OMX_COLOR_FormatUnused;
        if(m_VideoEncoderConfig.venc_config.br_ctrl == VENC_CONTROL_CQP){
            sPortDef.format.video.nBitrate = 0;
        }else{
            //WARNING: for CBR, if you set non-zero bitrate here, CBR does not work
            sPortDef.format.video.nBitrate = 0; //m_VideoEncoderConfig.venc_config.mbps * 1000000;
        }
        sPortDef.bEnabled = OMX_TRUE;
        if(m_VideoEncoderConfig.venc_config.mode == VENC_H265){
            sPortDef.format.video.eCompressionFormat =  OMX_VIDEO_CodingHEVC;
        }else{
            sPortDef.format.video.eCompressionFormat =  OMX_VIDEO_CodingAVC;
        }
    }

    // now set our updated port definition
    ret = OMX_SetParameter(m_OMXHandle, OMX_IndexParamPortDefinition, (OMX_PTR)&sPortDef);
    if(ret){
        M_ERROR("OMX_SetParameter OMX_IndexParamPortDefinition failed!\n");
        return ret;
    }


    // now read them back and check the buffer count
    ret = OMX_GetParameter(m_OMXHandle, OMX_IndexParamPortDefinition, (OMX_PTR)&sPortDef);
    if (ret != OMX_ErrorNone) {
        M_ERROR("Error: GET OMX_IndexParamPortDefinition (second get)\n");
        return ret;
    }

    if (bufferCountMin < sPortDef.nBufferCountMin){
        bufferCountMin = sPortDef.nBufferCountMin;
    }

    sPortDef.nBufferCountActual = bufferCountMin;
    // sPortDef.nBufferCountMin    = bufferCountMin;
    M_DEBUG("Buffer Count Expected: %d\n", sPortDef.nBufferCountActual);

    // now write it back with the updated buffer count
    if (OMX_SetParameter(m_OMXHandle, OMX_IndexParamPortDefinition, (OMX_PTR)&sPortDef)){
        M_ERROR("OMX_SetParameter OMX_IndexParamPortDefinition failed!\n");
        return OMX_ErrorUndefined;
    }

    if (OMX_GetParameter(m_OMXHandle, OMX_IndexParamPortDefinition, (OMX_PTR)&sPortDef)){
        M_ERROR("------voxl-camera-server ERROR: OMX_GetParameter OMX_IndexParamPortDefinition failed!\n");
        return OMX_ErrorUndefined;
    }
    M_DEBUG("Buffer Count Actual: %d\n", sPortDef.nBufferCountActual);

    if(bufferCountMin != sPortDef.nBufferCountActual){
        M_ERROR("Failed to get correct number of buffers from OMX module, expected: %d got: %d\n", bufferCountMin, sPortDef.nBufferCountActual);
        return OMX_ErrorUndefined;
    }

    M_DEBUG("Port Def %d:\n\tCount Min: %d\n\tCount Actual: %d\n\tSize: 0x%x\n\tBuffers Contiguous: %s\n\tBuffer Alignment: %d\n",
        portIndex,
        sPortDef.nBufferCountMin,
        sPortDef.nBufferCountActual,
        sPortDef.nBufferSize,
        sPortDef.bBuffersContiguous ? "Yes" : "No",
        sPortDef.nBufferAlignment
        );

    *pBufferCount = sPortDef.nBufferCountActual;
    *pBufferSize  = sPortDef.nBufferSize;

    return OMX_ErrorNone;
}

int VideoEncoder::ItemsInQueue()
{
    return out_metaQueue.size();
}

// -----------------------------------------------------------------------------------------------------------------------------
// The client calls this interface function to pass in a YUV image frame to be encoded
// -----------------------------------------------------------------------------------------------------------------------------
void VideoEncoder::ProcessFrameToEncode(camera_image_metadata_t meta, BufferBlock* buffer)
{
    pthread_mutex_lock(&out_mutex);
    // Queue up work for thread "ThreadProcessOMXOutputPort"
    out_metaQueue.push_back(meta);
    //pthread_cond_signal(&out_cond);
    pthread_mutex_unlock(&out_mutex);

    OMX_BUFFERHEADERTYPE* OMXBuffer = NULL;

    //find the corresponding OMX buffer that matches our actual data buffer
    //in non-shared memory mode, the OMX pBuffer will equal to the virtual address of our data buffer
    //in shared memory mode, the OMX pBuffer will equal to the address of OMX metadata associated with the data buffer
    void * match_address = buffer->vaddress;

#if QRB5165 && OMX_SHARED_BUFFERS
    match_address = buffer->meta_address;
#endif

    for(unsigned int i = 0; (OMXBuffer = m_ppInputBuffers[i])->pBuffer != match_address; i++) {
        //M_VERBOSE("Encoder Buffer Miss\n");
        if(i == m_pHALInputBuffers->totalBuffers - 1){
            M_ERROR("Encoder did not find omx-ready buffer for buffer: 0x%lx, skipping encoding\n", buffer->vaddress);
            return;
        }
    }
    M_VERBOSE("Encoder Buffer Hit\n");

    // QRB Testing
    // 4096x2160
    // OMXBuffer->nFilledLen = buffer->width * (buffer->height + 267) * 3 / 2;
    // 2048x1536
    // OMXBuffer->nFilledLen = buffer->width * buffer->height * 3 / 2;
    // 1024x768
    // OMXBuffer->nFilledLen = buffer->width * (buffer->height + 171) * 3 / 2;

    #ifdef QRB5165
        OMXBuffer->nFilledLen = buffer->size;
    #else
        int offset = 0;
        switch (buffer->height) {
            case 720 : offset = 16; break;
            case 1080: offset = 8;  break;
        }
        OMXBuffer->nFilledLen = buffer->width * (buffer->height + offset) * 3 / 2;
    // bufferMakeYUVContiguous(buffer);
    #endif

    OMXBuffer->nTimeStamp = meta.timestamp_ns;

    if (OMX_EmptyThisBuffer(m_OMXHandle, OMXBuffer))
    {
        M_ERROR("OMX_EmptyThisBuffer failed for framebuffer: %d\n", meta.frame_id);
    }

    M_VERBOSE("OMX emptied a buffer\n");
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function performs any work necessary to start receiving encoding frames from the client
// -----------------------------------------------------------------------------------------------------------------------------
void VideoEncoder::Start()
{
    pthread_attr_t resultAttr;
    pthread_attr_init(&resultAttr);
    pthread_attr_setdetachstate(&resultAttr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&out_thread,
                   &resultAttr,
                   [](void* data){return ((VideoEncoder*)data)->ThreadProcessOMXOutputPort();},
                   this);

    pthread_attr_destroy(&resultAttr);
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function is called by the client to indicate that no more frames will be sent for encoding
// -----------------------------------------------------------------------------------------------------------------------------
void VideoEncoder::Stop()
{
    stop  = true;
    usleep(500000);
    stop_read = true;
    // The thread wont finish and the "join" call will not return till the last expected encoded frame is received from
    // the encoder OMX component
    pthread_cond_signal(&out_cond);

    pthread_join(out_thread, NULL);

    pthread_mutex_destroy(&out_mutex);
    pthread_cond_destroy(&out_cond);

    if(OMX_SendCommand(m_OMXHandle, OMX_CommandStateSet, (OMX_U32)OMX_StateIdle, NULL)){
        M_ERROR("OMX Set state idle failed!\n");
        throw -EINVAL;
    }

    //disable the port before associated buffers can be freed
    //WARNING: disabling the input port causes a segfault in non shared memory mode
    //however, without disabling the input port, OMX will not actually free the buffers...
#if QRB5165 && OMX_SHARED_BUFFERS
    if(OMX_SendCommand(m_OMXHandle, OMX_CommandPortDisable, PortIndexIn, NULL)){
        M_ERROR("OMX failed to send disable command to input port\n");
    }
#endif

    //disable the port before associated buffers can be freed
    if(OMX_SendCommand(m_OMXHandle, OMX_CommandPortDisable, PortIndexOut, NULL)){
        M_ERROR("OMX failed to send disable command to output port\n");
    }

    for(unsigned int i=0; i<m_inputBufferCount; i++){
        OMX_FreeBuffer(m_OMXHandle, PortIndexIn, m_ppInputBuffers[i]);
    }

    free(m_ppInputBuffers);

    for (unsigned int i=0; i<m_outputBufferCount; i++){
        OMX_FreeBuffer(m_OMXHandle, PortIndexOut, m_ppOutputBuffers[i]);
    }

    free(m_ppOutputBuffers);

    //OMX_FreeHandle(m_OMXHandle);
    OMXDeinit();

    // if (m_OMXHandle != NULL)
    // {
    //     OMXFreeHandle(m_OMXHandle);
    //     m_OMXHandle = NULL;
    // }

}



// -----------------------------------------------------------------------------------------------------------------------------
// Function called by the OMX component for event handling
// -----------------------------------------------------------------------------------------------------------------------------
OMX_ERRORTYPE OMXEventHandler(OMX_IN OMX_HANDLETYPE hComponent,     ///< OMX component handle
                              OMX_IN OMX_PTR        pAppData,       ///< Any private app data
                              OMX_IN OMX_EVENTTYPE  eEvent,         ///< Event identifier
                              OMX_IN OMX_U32        nData1,         ///< Data 1
                              OMX_IN OMX_U32        nData2,         ///< Data 2
                              OMX_IN OMX_PTR        pEventData)     ///< Event data
{
    //VideoEncoder * enc = (VideoEncoder*)pAppData;

    switch (eEvent) {
        case OMX_EventCmdComplete:
            M_DEBUG("OMX_EventCmdComplete\n");
            if (nData1 == OMX_CommandStateSet){
                if (nData2 == OMX_StateExecuting){
                    M_DEBUG("Encoder moved to Executing State\n");
                } else if (nData2 == OMX_StateIdle){
                    M_DEBUG("Encoder moved to Idle State\n");
                } else if (nData2 == OMX_StateLoaded){
                    M_DEBUG("Encoder moved to Loaded State\n");
                }
            }
            break;
        case OMX_EventError:
            M_DEBUG("OMX_EventError: ");
           _print_omx_error((OMX_ERRORTYPE)nData1);
            break;
        case OMX_EventMark:
            M_DEBUG("OMX Event: OMX_EventMark\n");
            break;
        case OMX_EventPortSettingsChanged:
            M_DEBUG("OMX Event: OMX_EventPortSettingsChanged\n");
            break;
        case OMX_EventBufferFlag:
            M_DEBUG("OMX Event: OMX_EventBufferFlag\n");
            break;
        case OMX_EventResourcesAcquired:
            M_DEBUG("OMX Event: OMX_EventResourcesAcquired\n");
            break;
        case OMX_EventComponentResumed:
            M_DEBUG("OMX Event: OMX_EventComponentResumed\n");
            break;
        case OMX_EventDynamicResourcesAvailable:
            M_DEBUG("OMX Event: OMX_EventDynamicResourcesAvailable\n");
            break;
        case OMX_EventPortFormatDetected:
            M_DEBUG("OMX Event: OMX_EventPortFormatDetected\n");
            break;
        case OMX_EventKhronosExtensions:
            M_DEBUG("OMX Event: OMX_EventKhronosExtensions\n");
            break;
        case OMX_EventVendorStartUnused:
            M_DEBUG("OMX Event: OMX_EventVendorStartUnused\n");
            break;
        case OMX_EventMax:
            M_DEBUG("OMX Event: OMX_EventMax\n");
            break;
        default:
            M_DEBUG("OMX Event: Unknown\n");
            break;

    }
    return OMX_ErrorNone;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Function called by the OMX component indicating it has completed consuming our YUV frame for encoding
// -----------------------------------------------------------------------------------------------------------------------------
OMX_ERRORTYPE OMXEmptyBufferHandler(OMX_IN OMX_HANDLETYPE        hComponent,    ///< OMX component handle
                                    OMX_IN OMX_PTR               pAppData,      ///< Any private app data
                                    OMX_IN OMX_BUFFERHEADERTYPE* pBuffer)       ///< Buffer that has been emptied
{

    VideoEncoder*  pVideoEncoder = (VideoEncoder*)pAppData;

#if QRB5165 && OMX_SHARED_BUFFERS
    //pBuffer->pBuffer is equal to the metadata address
    void * vaddress = NULL;

    uint32_t total_buffers = pVideoEncoder->m_pHALInputBuffers->totalBuffers;

    for(unsigned int i = 0; i<total_buffers; i++)
    {
        if (pVideoEncoder->m_pHALInputBuffers->bufferBlocks[i].meta_address == pBuffer->pBuffer)
        {
            vaddress = pVideoEncoder->m_pHALInputBuffers->bufferBlocks[i].vaddress;
            bufferPushAddress(*pVideoEncoder->m_pHALInputBuffers, vaddress);
            return OMX_ErrorNone;
        }
    }

    M_ERROR("Recieved invalid buffer in %s\n", __FUNCTION__);
    return OMX_ErrorBadParameter;

#else
    //in non shared memory mode, we already have the vaddress of the buffer, so we can return it to the buffer pool
    bufferPushAddress(*pVideoEncoder->m_pHALInputBuffers, pBuffer->pBuffer);
    return OMX_ErrorNone;
#endif
}

// -----------------------------------------------------------------------------------------------------------------------------
// Function called by the OMX component to give us the encoded frame. Since this is a callback we dont do much work but simply
// prepare work that will be done by the worker threads
// -----------------------------------------------------------------------------------------------------------------------------
OMX_ERRORTYPE OMXFillHandler(OMX_OUT OMX_HANDLETYPE        hComponent,  ///< OMX component handle
                             OMX_OUT OMX_PTR               pAppData,    ///< Any private app data
                             OMX_OUT OMX_BUFFERHEADERTYPE* pBuffer)     ///< Buffer that has been filled by OMX component
{
    VideoEncoder*  pVideoEncoder = (VideoEncoder*)pAppData;
    if(pVideoEncoder->stop) return OMX_ErrorNone;
    pthread_mutex_lock(&pVideoEncoder->out_mutex);
    // Queue up work for thread "ThreadProcessOMXOutputPort"
    pVideoEncoder->out_msgQueue.push_back(pBuffer);
    pthread_cond_signal(&pVideoEncoder->out_cond);
    pthread_mutex_unlock(&pVideoEncoder->out_mutex);

    return OMX_ErrorNone;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This thread function processes the encoded buffers available on the OMX component's output port
// -----------------------------------------------------------------------------------------------------------------------------
void* VideoEncoder::ThreadProcessOMXOutputPort()
{
    pthread_setname_np(pthread_self(), "omx_out");

    int64_t frameNumber = -1;

    // The condition of the while loop is such that this thread will not terminate till it receives the last expected encoded
    // frame from the OMX component
    while (!stop_read)
    {
        pthread_mutex_lock(&out_mutex);

        if (out_msgQueue.empty())
        {
            pthread_cond_wait(&out_cond, &out_mutex);
            pthread_mutex_unlock(&out_mutex);
            continue;
        }

        if(out_metaQueue.empty()){
            M_WARN("Trying to process omx output with missing metadata\n");
            //pthread_cond_wait(&out_cond, &out_mutex);
            pthread_mutex_unlock(&out_mutex);
            continue;
        }

        if(m_outputPipe==NULL){
            M_WARN("Trying to process omx output without initialized pipe\n");
            pthread_cond_wait(&out_cond, &out_mutex);
            pthread_mutex_unlock(&out_mutex);
            continue;
        }


        // Coming here means we have a encoded frame to process
        OMX_BUFFERHEADERTYPE* pOMXBuffer = out_msgQueue.front();
        out_msgQueue.pop_front();


        camera_image_metadata_t meta     = out_metaQueue.front();
        // h264 metadata packet, don't associate it with a frame
        if(m_VideoEncoderConfig.venc_config.mode == VENC_H265){
            if(pOMXBuffer->pBuffer[4] != 0x40){
                out_metaQueue.pop_front();
            } else {
                meta.frame_id = -1;
            }
        } else {
            if(pOMXBuffer->pBuffer[4] != 0x67){
                out_metaQueue.pop_front();
            } else {
                meta.frame_id = -1;
            } 
        }

        pthread_mutex_unlock(&out_mutex);
        frameNumber = meta.frame_id;

        meta.size_bytes = pOMXBuffer->nFilledLen;
        if(m_VideoEncoderConfig.venc_config.mode == VENC_H265){
            meta.format = IMAGE_FORMAT_H265;
        }else{
            meta.format = IMAGE_FORMAT_H264;
        }

        pipe_server_write_camera_frame(*m_outputPipe, meta, pOMXBuffer->pBuffer);
        M_VERBOSE("Sent encoded frame: %d\n", frameNumber);

        // Since we processed the OMX buffer we can immediately recycle it by
        // sending it to the output port of the OMX component
        // reset filled len
        pOMXBuffer->nFilledLen=0;
        if (OMX_FillThisBuffer(m_OMXHandle, pOMXBuffer))
        {
            M_ERROR("OMX_FillThisBuffer resulted in error for frame %d\n", frameNumber);
        }

        // set the desired CBR bitrate (workaround for CBR not being set correctly during init)
        if(m_VideoEncoderConfig.venc_config.br_ctrl == VENC_CONTROL_CBR){
            if (m_dynamicBitrateUpdateCount == 0){
                OMX_U32 bps = m_VideoEncoderConfig.venc_config.mbps*1000000;
                m_dynamicBitrateUpdateCount++;
                SetTargetBitrate(bps);
            }
        }
    }

    M_DEBUG("------ Last frame encoded: %d\n", frameNumber);

    return NULL;
}

int VideoEncoder::SetIntraPeriod(uint32_t intra_period)
{
    QOMX_VIDEO_INTRAPERIODTYPE intraPeriod;
    OMX_RESET_STRUCT_SIZE_VERSION(&intraPeriod, QOMX_VIDEO_INTRAPERIODTYPE);

    intraPeriod.nPortIndex = PortIndexOut;
    intraPeriod.nIDRPeriod = 1;  //1 means every I frame will be an IDR frame
    intraPeriod.nPFrames   = intra_period;
    intraPeriod.nBFrames   = 0;

    OMX_ERRORTYPE ret            = OMX_SetConfig(m_OMXHandle, (OMX_INDEXTYPE)(QOMX_IndexConfigVideoIntraperiod), (OMX_PTR)&intraPeriod);
    if(ret){
        M_ERROR("OMX_SetParameter of QOMX_IndexConfigVideoIntraperiod failed!\n");
        _print_omx_error(ret);
        return -1;
    }

    return 0;
}

int VideoEncoder::SetTargetBitrate(uint32_t bps)
{
    //do not allow dynamic bitrate update until the encoder starts running, otherwise CBR gets messed up
    if (m_dynamicBitrateUpdateCount == 0){
        M_WARN("Attempting to update encoder bitrate while encoder is idle!\n");
        return -1;
    }

    //H265 has a minimum 3Mbit requirement, otherwise results in 10Mbit
    //this limit has been disable because we are testing a workaround that increases configured encoder fps to allow lower bitrates
    /*
    if(m_VideoEncoderConfig.venc_config.mode == VENC_H264){
        if (bps < 1500000){
            M_WARN("OMX SetTargetBitrate: H264 CBR requires bps > 1.5Mbit (%d bps provided). Updating to 1.5Mbit\n",bps);
            bps = 1500000;
        }
    }
    else if(m_VideoEncoderConfig.venc_config.mode == VENC_H265){
        if (bps < 3000000){
            M_WARN("OMX SetTargetBitrate: H265 CBR requires bps > 3Mbit (%d bps provided). Updating to 3Mbit\n",bps);
            bps = 3000000;
        }
    }
    */

    //simple sanity check
    if ( (bps < 100000) || (bps > 150000000) ){
        M_ERROR("Attempting to set an invalid encoder bit rate : %d bps\n",bps);
        return -1;
    }

    OMX_VIDEO_CONFIG_BITRATETYPE configBitRate;
    OMX_RESET_STRUCT_SIZE_VERSION(&configBitRate, OMX_VIDEO_CONFIG_BITRATETYPE);

    configBitRate.nPortIndex     = PortIndexOut;
    configBitRate.nEncodeBitrate = (OMX_U32)(bps * m_fpsHackScale);
    OMX_ERRORTYPE ret            = OMX_SetConfig(m_OMXHandle, (OMX_INDEXTYPE)(OMX_IndexConfigVideoBitrate), (OMX_PTR)&configBitRate);
    if(ret){
        M_ERROR("OMX_SetParameter of OMX_IndexConfigVideoBitrate failed!\n");
        _print_omx_error(ret);
        return -1;
    }

    m_dynamicBitrateUpdateCount++;

    return 0;
}


/*
Params that are not working

    //does not work, results in OMX-VENC: ERROR: unsupported index 2130706489
    OMX_QCOM_VIDEO_PARAM_VUI_TIMING_INFO vui_timing_info;
    OMX_RESET_STRUCT_SIZE_VERSION(&vui_timing_info, OMX_QCOM_VIDEO_PARAM_VUI_TIMING_INFO);
    vui_timing_info.bEnable = OMX_TRUE;
    ret = OMX_SetConfig(m_OMXHandle, (OMX_INDEXTYPE)(OMX_QcomIndexParamH264VUITimingInfo), (OMX_PTR)&vui_timing_info);
    if(ret){
        M_ERROR("OMX_SetParameter of OMX_QcomIndexParamH264VUITimingInfo failed!\n");
        _print_omx_error(ret);
        return OMX_ErrorUndefined;
    }


    //WARNING: this results in an error : E OMX-VENC: venc_dev: ERROR: Invalid Port Index for OMX_IndexParamVideoBitrate
    paramBitRate.nPortIndex = PortIndexIn;
     ret = OMX_SetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_IndexParamVideoBitrate, (OMX_PTR)&paramBitRate);
    if(ret){
        M_ERROR("OMX_SetParameter of OMX_IndexParamVideoBitrate failed for in index!\n");
        _print_omx_error(ret);
        return OMX_ErrorUndefined;
    }

    ////////////////////////////////////////////////////////////////////////////
    // OMX_QCOM_VIDEO_CONFIG_PERF_LEVEL
    // https://android.googlesource.com/platform/hardware/qcom/sm7250/media/+/0aef9b5a7fda17e3fac441b565cd1fb4e37df0ff/mm-video-v4l2/vidc/venc/src/omx_video_extensions.hpp
    ////////////////////////////////////////////////////////////////////////////
    //WARNING: it seems this is not working, results in error : E OMX-VENC: ERROR: unsupported index 2130706493
    OMX_QCOM_VIDEO_CONFIG_PERF_LEVEL perf;
    OMX_RESET_STRUCT_SIZE_VERSION(&perf, OMX_QCOM_VIDEO_CONFIG_PERF_LEVEL);
    perf.ePerfLevel = OMX_QCOM_PerfLevelTurbo;
    //perf.ePerfLevel = OMX_QCOM_PerfLevelNominal;
    ret = OMX_SetConfig(m_OMXHandle, (OMX_INDEXTYPE)(OMX_QcomIndexConfigPerfLevel), (OMX_PTR)&perf);
    if(ret){
        M_ERROR("%s Failed to set OMX Turbo Config\n", __func__);
        _print_omx_error(ret);
        return OMX_ErrorUndefined;
    }


    //priority config does not seem to be doing anything
    OMX_PARAM_U32TYPE priority_config;
    OMX_RESET_STRUCT_SIZE_VERSION(&priority_config, OMX_PARAM_U32TYPE);
    int32_t priority = 0;
    priority_config.nU32 = priority;
    if (OMX_SetConfig(m_OMXHandle, (OMX_INDEXTYPE)OMX_IndexConfigPriority, (OMX_PTR)&priority_config))
    {
        M_ERROR("OMX_SetConfig of OMX_IndexConfigPriority failed!\n");
        return OMX_ErrorUndefined;
    }



// more things to try:
    // OMX_QTIIndexParamColorSpaceConversion
    // OMX_QTIIndexConfigContentAdaptiveCoding


    ////////////////////////////////////////////////////////////////////////////
    // OMX_VIDEO_PARAM_QUANTIZATIONTYPE
    // standard OMX quantization struct, doesn't seem to work
    // returns OMX_ErrorUnsupportedIndex
    ////////////////////////////////////////////////////////////////////////////
    OMX_VIDEO_PARAM_QUANTIZATIONTYPE quant;
    OMX_RESET_STRUCT_SIZE_VERSION(&quant, OMX_VIDEO_PARAM_QUANTIZATIONTYPE);
    initqp.nPortIndex = PortIndexOut;
    quant.nQpI = 35;
    quant.nQpP = 35;
    quant.nQpB = 35;
    ret = OMX_SetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_IndexParamQuantizationTable, (OMX_PTR)&quant);
    if(ret){
        M_ERROR("%s Failed to set Quantization Parameter\n", __func__);
        _print_omx_error(ret);
        return ret;
    }


    ////////////////////////////////////////////////////////////////////////
    // set OMX_QCOM_VIDEO_PARAM_PEAK_BITRATE
    // not working, returns OMX_ErrorUnsupportedIndex
    ////////////////////////////////////////////////////////////////////////
    OMX_QCOM_VIDEO_PARAM_PEAK_BITRATE peak_br;
    OMX_RESET_STRUCT_SIZE_VERSION(&peak_br, OMX_QCOM_VIDEO_PARAM_PEAK_BITRATE);
    peak_br.nPeakBitrate = m_VideoEncoderConfig.targetBitRate;
    ret = OMX_SetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_QcomIndexParamPeakBitrate, (OMX_PTR)&peak_br);
    if(ret){
        M_ERROR("%s Failed to set Peak Bitrate parameter\n", __func__);
        _print_omx_error(ret);
        return ret;
    }


        // Set Color aspect parameters
    // android::DescribeColorAspectsParams colorParams;
    // OMX_RESET_STRUCT(&colorParams, android::DescribeColorAspectsParams);
    // colorParams.nPortIndex = PortIndexIn;

    // if (OMX_GetConfig(m_OMXHandle, (OMX_INDEXTYPE)OMX_QTIIndexConfigDescribeColorAspects, (OMX_PTR)&colorParams))
    // {
    //     M_ERROR("OMX_GetConfig of OMX_QTIIndexConfigDescribeColorAspects failed!\n");
    //     return OMX_ErrorUndefined;
    // }
    // colorParams.sAspects.mPrimaries    = android::ColorAspects::PrimariesBT709_5;
    // colorParams.sAspects.mTransfer     = android::ColorAspects::TransferSMPTE170M;
    // colorParams.sAspects.mMatrixCoeffs = android::ColorAspects::MatrixBT709_5;

    // OMX_RESET_STRUCT_SIZE_VERSION(&colorParams, android::DescribeColorAspectsParams);

    // if (OMX_SetConfig(m_OMXHandle, (OMX_INDEXTYPE)OMX_QTIIndexConfigDescribeColorAspects, (OMX_PTR)&colorParams))
    // {
    //     M_ERROR("OMX_SetConfig of OMX_QTIIndexConfigDescribeColorAspects failed!\n");
    //     return OMX_ErrorUndefined;
    // }


*/
